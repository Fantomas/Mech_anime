#pragma once
#include <assert.h>
#include <set>

#include "Constants.h"
#include "ID_Genearator.h"
#include "Sceleton.h"


using namespace mech_a;
using namespace mech_a::sceleton;
using namespace mech_a::origin;


namespace mech_a {
	namespace sceleton {

		/**
		*  Wrapper by Value of associative container
		*/
		template<class T_Key, class T_Value, class T_Container = std::unordered_map<T_Key, T_Value>>
		struct MECHA_API ValuePtrContainer
		{
			typedef typename T_Container::iterator Iterator;

			ValuePtrContainer()
			{ }

			ValuePtrContainer(Iterator it)
				: m_It(it)
			{ }

			const T_Value& operator*() const { return m_It->second; }
			const T_Value* operator->() const { return &m_It->second; }
			T_Value& operator*() { return m_It->second; }
			T_Value* operator->() { return &m_It->second; }
			operator T_Value* () { return &m_It->second; }
			operator T_Value* () const { return &m_It->second; }

			T_Key GetKey() const { return m_It->first; }
			T_Value* ToPointer() { return &m_It->second }

			Iterator m_It;
		};


		typedef ValuePtrContainer<ULONG, SceletonData::TypeGlobalCS>      GlobalCSMapPtr;
		typedef ValuePtrContainer<ULONG, SceletonData::TypeLocalCS>       LocalCSMapPtr;
		typedef ValuePtrContainer<ULONG, SceletonData::ConnectionData>    ConnectionDataMapPtr;
		typedef ValuePtrContainer<ULONG, ElementData>                     ElementDataMapPtr;

		typedef ValuePtrContainer<ULONG, SceletonData::MotionData>                                   MotionsMapPtr;
		typedef ValuePtrContainer<MA_REAL, SceletonData::StateData, std::map<MA_REAL, StateData>>    StatesMapPtr;
		typedef ValuePtrContainer<ULONG, SceletonData::StateData>                                    StatesUnMapPtr;


		class MECHA_API SceletonBuilder;
		class MECHA_API Element;
		class MECHA_API IPartSceleton;


		/**
		*  IPartPropertyNotifier
		*/
		struct MECHA_API IPartPropertyNotifier
		{
			virtual ~IPartPropertyNotifier() { }

			virtual void OnPropertyChanged(IPartSceleton* pChangeObject, const std::wstring& propName) = 0;
		};


		/**
		*  IPartSceleton
		*/
		class MECHA_API IPartSceleton
		{
		protected:

			IPartSceleton()
			{ }

			std::unordered_set<IPartPropertyNotifier*>  m_pPartPropertyNotifiers;

			void OnPropertyChanged(const std::wstring& propName)
			{
				for (IPartPropertyNotifier* pNotifier : m_pPartPropertyNotifiers)
					pNotifier->OnPropertyChanged(this, propName);
			}

		public:

			virtual ~IPartSceleton() { }

			virtual ULONG ID() const = 0;
			virtual const std::wstring& Name() const = 0;
			virtual void SetName(const std::wstring& name) = 0;
			virtual const SceletonData::TypeGlobalCS& GetWCS() const = 0;
			virtual IPartSceleton* GetParentPart() const = 0;
			virtual std::unordered_set<IPartSceleton*> GetChildParts() const = 0;

			void AddPropertyNotifier(IPartPropertyNotifier* pPropNotifier)
			{
				m_pPartPropertyNotifiers.emplace(pPropNotifier);
			}

			const std::unordered_set<IPartPropertyNotifier*>& GetPropertyNotifiers() const
			{
				return m_pPartPropertyNotifiers;
			}

			void EraseNotifier(IPartPropertyNotifier* pPropNotifier)
			{
				m_pPartPropertyNotifiers.erase(pPropNotifier);
			}

			void ClearNotifiers()
			{
				m_pPartPropertyNotifiers.clear();
			}

		private:

			friend class SceletonBuilder;
		};


		/**
		*  Connection
		*/
		class MECHA_API Connection : public IPartSceleton
		{
			Connection(ConnectionDataMapPtr::Iterator itConnectionData, 
				Element* pElementA, Element* pElementB, 
				GlobalCSMapPtr::Iterator itWCS, 
				LocalCSMapPtr::Iterator itLCS
				)
				: IPartSceleton()
				, m_pConnectionData(itConnectionData)
				, m_pElementA(pElementA)
				, m_pElementB(pElementB)
				, m_pWCS(itWCS)
				, m_pLCS(itLCS)
			{ }

		public:

			virtual ULONG ID() const override
			{
				return m_pConnectionData->m_ID;
			}

			virtual const std::wstring& Name() const override
			{
				return m_pConnectionData->m_Name;
			}

			virtual void SetName(const std::wstring& name) override
			{
				m_pConnectionData->m_Name = name;
				OnPropertyChanged(L"Name");
			}

			virtual const SceletonData::TypeGlobalCS& GetWCS() const override
			{
				return *m_pWCS;
			}

			virtual IPartSceleton* GetParentPart() const override;

			virtual std::unordered_set<IPartSceleton*> GetChildParts() const override;

			/** Current lcs in basic lcs */
			const SceletonData::TypeLocalCS& GetLCS() const
			{
				return *m_pLCS;
			}

			/** LCS in relationship connection to the parent element */
			const SceletonData::TypeGlobalCS& GetBasicLCS() const
			{
				return m_pConnectionData->m_BasicLCS;
			}

			/** return LCS * BasicLCS  */
			SceletonData::TypeGlobalCS GetGlobalLCS() const;

			/** WCS parent include basic LCS*/
			SceletonData::TypeGlobalCS GetBasicWCS() const;

			Element* GetElementA() const
			{
				return m_pElementA;
			}

			Element* GetElementB() const
			{
				return m_pElementB;
			}

			void UpdateWCSFromElementA();

			SceletonData::TypeGlobalCS CalculateParentWCS() const;

			/** Get Extents of freedom */
			ExtentsFreedom GetExtentsOfFreedom() const
			{
				return m_pConnectionData->m_ExtentsFreedom;
			}

			/** Set Extents of freedom */
			void SetExtentsOfFreedom(ExtentsFreedom extFrd)
			{
				m_pConnectionData->m_ExtentsFreedom = extFrd;
				OnPropertyChanged(L"ExtentsFreedom");
			}

			/** Set new wcs and update full tree of the sceleton */
			void Retransform(const SceletonData::TypeGlobalCS& fromWCS);

			/** Set new wcs and update only the child tree of the sceleton  */
			void RetransformChilds(const SceletonData::TypeGlobalCS& fromWCS);

			/** Set new lcs and update the child tree in to up or down direction */
			void RetransformLocal(const SceletonData::TypeLocalCS& newLCS, bool down = true);

			/** Set new basic lcs and update the child tree in to down direction */
			void RetransformBasicLocal(const SceletonData::TypeGlobalCS& newBasicLCS);

			/** For only autonomus connections */
			void UpdateWCSFromLCS_AndUpdate();

			void SetCoordinatesToStream(OUT std::stringstream& stream) const
			{
				m_pWCS->ToStream(stream);
				m_pLCS->ToStream(stream);
			}

			void LoadCoordinatesFromStream(IN std::stringstream& stream)
			{
				SceletonData::TypeGlobalCS wcs(stream);
				SceletonData::TypeLocalCS lcs(stream);
		
				setWCS(wcs);
				setLCS(lcs);
			}

		private:

			void notifyWCS();

			void setWCS(const SceletonData::TypeGlobalCS& wcs);

			void setBasicLCS(const SceletonData::TypeGlobalCS& lcs)
			{
				m_pConnectionData->m_BasicLCS = lcs;
				OnPropertyChanged(L"BasicLCS");
			}

			void updateWCS_onlyConnect(const SceletonData::TypeGlobalCS& wcs);

			void setLCS(const SceletonData::TypeLocalCS& lcs)
			{
				*m_pLCS = lcs;
				OnPropertyChanged(L"LCS");
			}

			void setLCS_WCS(const SceletonData::TypeGlobalCS& gLcs)
			{
				SceletonData::TypeLocalCS lcs;
				FromCS_ToCS(gLcs, lcs);
				setLCS(lcs);
			}

			void setElementA(Element* pElementA);
			void setElementB(Element* pElementB);

			// fields
			ConnectionDataMapPtr        m_pConnectionData;
			Element*                    m_pElementA;
			Element*                    m_pElementB;
			GlobalCSMapPtr              m_pWCS;
			LocalCSMapPtr               m_pLCS;

			// friends
			friend MECHA_API class SceletonBuilder;
			friend MECHA_API void LoadParts(IN std::unordered_map<ULONG, ElementData>& elements, IN std::unordered_map<ULONG, SceletonData::ConnectionData>& connections,
				IN StateData& state, OUT std::unordered_map<ULONG, std::unique_ptr<IPartSceleton>>& parts);
			friend MECHA_API void UpdateSceletonDownWCS(Connection* pUpperConnect, const SceletonData::TypeGlobalCS& updateWCS);
			friend MECHA_API void UpdateSceletonDownLCS(Connection* pUpperConnect, const SceletonData::TypeLocalCS& updateLCS);
			friend MECHA_API void UpdateSceleton(Connection* pBeginConnect, const SceletonData::TypeGlobalCS& updateWCS);
			friend MECHA_API void UpdateSceletonUpLCS(Connection* pUpperConnect, const SceletonData::TypeLocalCS& updateLCS);
			friend MECHA_API void UpdateSceletonFromConnection(Connection* pConnect, bool onlyUpDir);
			friend MECHA_API void UpdateSceletonDownBasicLCS(Connection* pUpperConnect, const SceletonData::TypeGlobalCS& updateBasicLCS);
			friend MECHA_API void UpdateLocalPositions(const LocalPositions& newPositions, const std::pair<ULONG, SceletonData::TypeGlobalCS>& wcsBaseConnection, IN std::unordered_map<ULONG, std::unique_ptr<IPartSceleton>>& connections);
		};


		/**
		*  Element
		*/
		class MECHA_API Element : public IPartSceleton
		{
			Element(ElementDataMapPtr::Iterator itElementData, Connection* pParentConnect)
				: IPartSceleton()
				, m_pElementData(itElementData)
				, m_pParentConnect(pParentConnect)
			{ }

		public:

			virtual ULONG ID() const override
			{
				return m_pElementData->m_ID;
			}

			virtual const std::wstring& Name() const override
			{
				return m_pElementData->m_Name;
			}

			virtual void SetName(const std::wstring& name) override
			{
				m_pElementData->m_Name = name;
				OnPropertyChanged(L"Name");
			}

			virtual const SceletonData::TypeGlobalCS& GetWCS() const override
			{
				return m_pParentConnect->GetWCS();
			}

			Connection* GetParentConnect() const
			{
				return m_pParentConnect;
			}

			const std::unordered_set<Connection*>& GetChildConnects() const
			{
				return m_psConnects;
			}

			MA_REAL Lenght() const
			{
				return m_pElementData->m_Lengh;
			}

			bool SetLenght(MA_REAL lenght)
			{
				if (lenght < 0 || Compare(lenght, 0))
					return false;

				m_pElementData->m_Lengh = lenght;
				OnPropertyChanged(L"Lenght");
				return true;
			}

			virtual IPartSceleton* GetParentPart() const override
			{
				return m_pParentConnect;
			}

			virtual std::unordered_set<IPartSceleton*> GetChildParts() const override
			{
				std::unordered_set<IPartSceleton*> resArr;

				for (Connection* pConn : m_psConnects)
					resArr.insert(pConn);

				return resArr;
			}

		private:

			void addChildConnect(Connection* pConnect);
			void removeChildConnect(Connection* pConnect);

			// fields
			ElementDataMapPtr                m_pElementData;
			Connection*                      m_pParentConnect;
			std::unordered_set<Connection*>  m_psConnects;

			// friends
			friend MECHA_API class SceletonBuilder;
			friend MECHA_API class Connection;
			friend MECHA_API void LoadParts(IN std::unordered_map<ULONG, ElementData>& elements, IN std::unordered_map<ULONG, SceletonData::ConnectionData>& connections,
				IN StateData& state, OUT std::unordered_map<ULONG, std::unique_ptr<IPartSceleton>>& parts);
		};
	}
}