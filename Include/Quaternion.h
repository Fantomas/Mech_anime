#pragma once
#include "Constants.h"
#include "Vector.h"
#include "Matrix.h"


namespace mech_a {

	namespace math {

		struct MECHA_API Quaternion
		{
			Quaternion() 
				: m_W(1), m_X(0), m_Y(0), m_Z(0)
			{ }

			Quaternion(MA_REAL W, MA_REAL X, MA_REAL Y, MA_REAL Z) 
				: m_W(W), m_X(X), m_Y(Y), m_Z(Z)
			{ }

			Quaternion(MA_REAL W, const Vector& vc) 
				: m_W(W), m_X(vc.X), m_Y(vc.Y), m_Z(vc.Z)
			{ }

			operator Vector() const { return Vector(m_X, m_Y, m_Z); }

			Quaternion operator * (const Quaternion& qt) const;

			MA_REAL GetValue() const;

			// сопряженный данному
			Quaternion GetConjugate() const;

			bool Normalize();

			void SetAxisAngl(MA_REAL angle, Vector axis);

			void GetAxisAngl(OUT MA_REAL& angle, OUT Vector& axis) const;

			// fields
			MA_REAL m_X, m_Y, m_Z, m_W;

			// stream work
			Quaternion(std::stringstream& stream)
				: m_W(1), m_X(0), m_Y(0), m_Z(0)
			{
				FromStream(stream);
			}

			void ToStream(std::stringstream& stream) const
			{
				stream.write((char*)this, sizeof(Quaternion));
			}

			void FromStream(std::stringstream& stream)
			{
				stream.read((char*)this, sizeof(Quaternion));
			}
		};
	}
}