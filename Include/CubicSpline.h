#pragma once
#include "Constants.h"


using namespace std;


namespace mech_a {

	namespace math {

		/**
		*  Cubical Spline of defect one
		*/
		struct MECHA_API CubicSpline
		{
			CubicSpline()
				: m_A(0), m_B(0), m_C(0), m_D(0), m_Xk(0)
			{ }

			MA_REAL Calculate(MA_REAL x) const;

			MA_REAL CalculateDerivative(MA_REAL x) const;

			// std::pair<MA_REAL, MA_REAL> MaxValue(MA_REAL x1, MA_REAL x2) const; // x, y

			std::pair<MA_REAL, MA_REAL> MaxDerivative(MA_REAL x1, MA_REAL x2) const; // x, y

			MA_REAL  m_A, m_B, m_C, m_D, m_Xk;

			// stream work
			CubicSpline(std::stringstream& stream)
				: m_A(0), m_B(0), m_C(0), m_D(0), m_Xk(0)
			{
				FromStream(stream);
			}

			void ToStream(std::stringstream& stream) const
			{
				stream.write((char*)this, sizeof(CubicSpline));
			}

			void FromStream(std::stringstream& stream)
			{
				stream.read((char*)this, sizeof(CubicSpline));
			}
		};


		/**
		*  Cubical Spline Function
		*/
		class MECHA_API CubicSplineFunction
		{
		public:

			CubicSplineFunction(const map<MA_REAL, MA_REAL>& values);

			MA_REAL Calculate(MA_REAL x) const;

		private:

			std::map<MA_REAL, CubicSpline>  m_Splines;
		};


		/**
		*  Algorithm of create Cubic Spline of defect one 
		*  < x, y >
		*/
		MECHA_API inline vector<CubicSpline> CubicSplineInterpolation(const map<MA_REAL, MA_REAL>& values);
		MECHA_API inline map<MA_REAL, CubicSpline> CubicSplineInterpolation2(const map<MA_REAL, MA_REAL>& values);

	}
}

