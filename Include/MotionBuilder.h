#pragma once
#include <memory>
#include <unordered_map>

#include "Constants.h"
#include "ID_Genearator.h"
#include "Sceleton.h"
#include "SceletonMethods.h"
#include "SceletonSupport.h"


using namespace mech_a;
using namespace mech_a::sceleton;
using namespace mech_a::origin;


namespace mech_a {
	namespace sceleton {


		struct MECHA_API IStateBuilder
		{
			virtual ~IStateBuilder() { }

			virtual SceletonData& GetSceletonData() = 0;

			virtual void OnStatePartChanged(IPartSceleton* pChangeObject, const std::wstring& propName) = 0;
		};


		struct MECHA_API PartStateNotifier : public IPartPropertyNotifier
		{
			PartStateNotifier(IStateBuilder* pStateBuilder)
				: m_pStateBuilder(pStateBuilder)
			{
				assert(pStateBuilder);
			}

			virtual void OnPropertyChanged(IPartSceleton* pChangeObject, const std::wstring& propName) override
			{
				assert(m_pStateBuilder);
				m_pStateBuilder->OnStatePartChanged(pChangeObject, propName);
			}
			
		protected:

			PartStateNotifier(const PartStateNotifier& el) { }
			PartStateNotifier& operator = (const PartStateNotifier& el) { return *this; }

			IStateBuilder*  m_pStateBuilder;
		};


		class MECHA_API IState
		{
		public:

			IState() { }

			virtual ~IState() { }

			virtual StateData* GetStateDataRef() const = 0;

			ULONG ID() const;

			const std::wstring& Name() const;

			void SetName(const std::wstring& name);

			std::unordered_set<IPartSceleton*> GetParts() const;

			std::unordered_map<UINT, Connection*> GetConnections() const;

			std::unordered_map<UINT, IPartSceleton*> GetPartsMap() const;

			std::unordered_map<ULONG, std::unique_ptr<IPartSceleton>>& Parts() { return m_Parts; }

			IPartSceleton* GetPartByName(const std::wstring& name) const;

			LocalPositions GetLocalPositions() const;

			GlobalPositions GetGlobalPositions() const;

			void ToStream(OUT std::stringstream& stream);
			
			void FromStream(IN std::stringstream& stream);

		protected:

			IState(const IState& el) { }
			IState& operator = (const IState& el) { return *this; }

			// fields
			std::unordered_map<ULONG, std::unique_ptr<IPartSceleton>>  m_Parts;
			std::unique_ptr<PartStateNotifier>  m_PartsNotifier;
		};


		class MECHA_API Motion;


		template<class StateDataPtr>
		class MECHA_API State : public IState
		{
		public:

			State(StateDataPtr stateData, IStateBuilder* pMotion);

			State(StateDataPtr stateData, SceletonData& scelData);

			virtual StateData* GetStateDataRef() const override { return (StateData*)m_StateData; }

			StateDataPtr GetStateDataPtr() const { return m_StateData; }

		private:

			State(const State& el) { }
			State& operator = (const State& el) { return *this; }

			// fields
			StateDataPtr  m_StateData;

			// friends
			friend class Motion;
		};


		typedef State<StatesMapPtr>    StateMotion;
		typedef State<StatesUnMapPtr>  StateSpecial;
		typedef State<StateData*>      StateBasic;


		class MECHA_API MotionPlayer
		{
		public:

			MotionPlayer(const StateData& startState, MA_REAL startTimeStamp, Motion* pMotion);

			StateBasic* GetState() const { return m_State.get(); }

			const StateData& GetStateData() const { return m_StateData; }

			MA_REAL GetTimeStamp() const { return m_CurrentTimeStamp; }

			MA_REAL SetTimeStamp(MA_REAL timeStamp);

		private:

			MotionPlayer(const MotionPlayer& el) { }
			MotionPlayer& operator = (const MotionPlayer& el) { return *this; }

			void UpdatePlayer();
			void recalcTimePosition();
			void setStateData(const StateData& stateData);

			// fields
			Motion*                      m_pMotion;
			MA_REAL                      m_CurrentTimeStamp;
			StateData                    m_StateData;
			StateData                    m_DefautStateData;
			std::unique_ptr<StateBasic>  m_State;

			// friends
			friend class Motion;
		};


		class MECHA_API MotionBuilder;


		class MECHA_API Motion : public IStateBuilder
		{
		public:

			Motion(MotionsMapPtr motionData, MotionBuilder* pMotionBuilder);

			virtual SceletonData& GetSceletonData() override;

			virtual void OnStatePartChanged(IPartSceleton* pChangeObject, const std::wstring& propName) override
			{
				recalcInterpolationData();
				m_MotionPlayer->UpdatePlayer();
			}

			ULONG ID() const;

			ULONG ID_Relatively_Part() const;

			bool SetID_Relatively_Part(ULONG idRelPart);

			const std::wstring& Name() const;

			void SetName(const std::wstring& name);

			std::unordered_set<StateMotion*> GetStates() const;

			std::unordered_map<UINT, StateMotion*> GetStatesIDMap() const;

			std::unordered_map<MA_REAL, StateMotion*> GetStatesMap() const;

			StateMotion* CreateState(ULONG idBaseState, const std::wstring& name, MA_REAL timeStamp);

			void RemoveState(ULONG idState);

			MotionPlayer* GetMotionPlayer() const { return m_MotionPlayer.get();  }

			MA_REAL GetStateTimeStamp(UINT idState);

		private:

			Motion(const Motion& el) { }
			Motion& operator = (const Motion& el) { return *this; }

			void recalcInterpolationData();

			// fields
			MotionBuilder*                  m_pMotionBuilder;
			MotionsMapPtr                   m_MotionData;
			std::unique_ptr<MotionPlayer>   m_MotionPlayer;
			std::unordered_map<ULONG, std::unique_ptr<StateMotion>>  m_States;

			friend class MotionPlayer;
		};


		class MECHA_API MotionBuilder : public IStateBuilder
		{
		public:

			MotionBuilder(const SceletonData& sceletonData);

			virtual SceletonData& GetSceletonData() override { return m_SceletonData; }

			virtual void OnStatePartChanged(IPartSceleton* pChangeObject, const std::wstring& propName) override
			{ }

			StateBasic* GetStateBasic() const { return m_StateBasic.get(); }

			std::unordered_map<ULONG, IState*> GetStates() const;

			std::unordered_map<ULONG, StateSpecial*> GetSpecialStates() const;

			std::unordered_map<ULONG, Motion*> GetMotions() const;

			StateSpecial* CreateSpecialState(ULONG idBaseState, const std::wstring& name);

			void RemoveSpecialState(ULONG idState);

			Motion* CreateMotion(const std::wstring& name, ULONG idRelativeConnect, ULONG idBaseState = 1, const std::wstring& baseStateName = L"First state");

			Motion* CreateMotionAsInterpolation(const std::wstring& name, ULONG idInterpMotion, ULONG idBaseState, float coeffInterp, ULONG idRelativeConnect);

			void RemoveMotion(ULONG idMotion);

		private:

			MotionBuilder(const MotionBuilder& el) { }
			MotionBuilder& operator = (const MotionBuilder& el) { return *this; }

			void recalcID_Generators();

			// fields
			SceletonData   m_SceletonData;
			std::unique_ptr<StateBasic>  m_StateBasic;
			std::unordered_map<ULONG, std::unique_ptr<Motion>>        m_Motions;
			std::unordered_map<ULONG, std::unique_ptr<StateSpecial>>  m_SpecialStates;

			ID_Genearator  m_StateIdGenerator;
			ID_Genearator  m_MotionIdGenerator;

			// friends
			friend class Motion;
		};

	}
}