#pragma once
#include "Constants.h"
#include "ID_Genearator.h"
#include "Sceleton.h"
#include "SceletonMethods.h"
#include "SceletonSupport.h"


using namespace mech_a;
using namespace mech_a::sceleton;
using namespace mech_a::origin;


namespace mech_a {
	namespace sceleton {


		/**
		*  SceletonBuilder listener.
		*/
		struct MECHA_API ISceletonBuilderListener
		{
			virtual ~ISceletonBuilderListener() { }

			virtual void OnCreatePart(IPartSceleton* pCreatePart) = 0;
			virtual void OnRemovePart(IPartSceleton* pRemovePart) = 0;
		};

		/**
		*  SceletonBuilder impl.
		*/
		class MECHA_API SceletonBuilder
		{
		public:

			SceletonBuilder(const SceletonData& sceletonData, ISceletonBuilderListener* pListener = NULL);

			bool CreateConnection(const SC& sc, const std::wstring& name, OUT Connection** ppNewConnect, Element* pToElement = NULL);

			bool CreateElement(Connection* pConnectA, Connection* pConnectB, const std::wstring& name, OUT Element** ppNewElement);

			bool CreateElement(Connection* pConnectA, MA_REAL lenght, const std::wstring& name, OUT Element** ppNewElement);

			bool RemoveSceletonPart(IPartSceleton* pRemovePart);

			void SetListener(ISceletonBuilderListener* pListener);

			std::unordered_set<IPartSceleton*> GetParts() const;

			std::unordered_map<UINT, IPartSceleton*> GetPartsMap() const;

			const SceletonData& GetData() const { return m_SceletonData; }

		private:

			SceletonBuilder(const SceletonBuilder& el) { }
			SceletonBuilder& operator = (const SceletonBuilder& el) { return *this; }

			bool removeElement(Element* pElement);
			bool removeConnection(Connection* pConnection);
			void removeMotionData(ULONG eraseID);

			// fields
			std::unordered_map<ULONG, std::unique_ptr<IPartSceleton>>  m_Parts;
			SceletonData               m_SceletonData;
			ID_Genearator              m_ID_Generator;
			ISceletonBuilderListener*  m_pListener;
		};

	}
}