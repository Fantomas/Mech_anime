#pragma once
#include <windows.h>

#include "Constants.h"
#include "Euler_angles.h"
#include "Vector.h"
#include "Matrix.h"
#include "Quaternion.h"



namespace mech_a {

	namespace math {


		// Reals
		MECHA_API bool Compare(MA_REAL vl1, MA_REAL vl2, MA_REAL tol = DEF_TOL);

		MECHA_API MA_REAL Min(MA_REAL vl1, MA_REAL vl2);

		MECHA_API MA_REAL Max(MA_REAL vl1, MA_REAL vl2);

		MECHA_API MA_REAL Round(MA_REAL val, MA_REAL fraction = 1.0);

		// Vector vs Vector
		MECHA_API bool Compare(const Vector& vc1, const Vector& vc2, MA_REAL tol = DEF_TOL);

		MECHA_API MA_REAL Dot(const Vector& vc1, const Vector& vc2);

		MECHA_API Vector Cross(const Vector& vc1, const Vector& vc2);

		MECHA_API bool IsNParallel(const Vector& vc1, const Vector& vc2, MA_REAL tol = DEF_TOL);

		MECHA_API bool IsParallel(const Vector& vc1, const Vector& vc2, MA_REAL tol = DEF_TOL);
		
		MECHA_API bool IsNCodir(const Vector& vc1, const Vector& vc2, MA_REAL tol = DEF_TOL);

		MECHA_API bool IsCodir(const Vector& vc1, const Vector& vc2, MA_REAL tol = DEF_TOL);

		MECHA_API bool IsNNormals(const Vector& vc1, const Vector& vc2, MA_REAL tol = DEF_TOL);

		MECHA_API MA_REAL AngleBetween(Vector vc1, Vector vc2, MA_REAL tol = DEF_TOL);

		MECHA_API MA_REAL AngleBetweenNormals(const Vector& vc1, const Vector& vc2, MA_REAL tol = DEF_TOL);

		MECHA_API MA_REAL DistanceBetween(const Vector& vc1, const Vector& vc2);

		MECHA_API MA_REAL ProjectionVectorTo(const Vector& vc1, const Vector& vc2);

		// ���� ����� ����� ��������� �� ��������
		// ������������ �� this �� vc - �� ������� �������  - ������������ � norm
		// -PI <= res <= PI
		MECHA_API MA_REAL AngleToProjection(Vector vc1, Vector vc2, Vector norm);

		MECHA_API Vector Min(const Vector& vc1, const Vector& vc2);

		MECHA_API Vector Max(const Vector& vc1, const Vector& vc2);

		// Vector vs Matrix
		MECHA_API bool Compare(const Matrix& mtr1, const Matrix& mtr2, MA_REAL tol = DEF_TOL);

		MECHA_API Vector operator * (const Vector& vc, const Matrix& mt);

		MECHA_API Vector& operator *= (Vector& vc, const Matrix& mt);

		MECHA_API Vector TransformNormal(const Vector& vc, const Matrix& mt);

		MECHA_API Matrix LCSToWCS(const Vector& pos, const Vector& xAxis, const Vector& yAxis, const Vector& zAxis);

		MECHA_API Matrix WCSToLCS(const Vector& pos, const Vector& xAxis, const Vector& yAxis, const Vector& zAxis);

		MECHA_API MA_REAL AngSetToRound(MA_REAL ang);

		// Matrix
		MECHA_API Matrix MatrixTranslation(Vector vc);

		MECHA_API Matrix MatrixRotationX(MA_REAL angle);

		MECHA_API Matrix MatrixRotationY(MA_REAL angle);

		MECHA_API Matrix MatrixRotationZ(MA_REAL angle);

		MECHA_API Matrix MatrixRotationAxis(const Vector& v, MA_REAL ang);

		MECHA_API Matrix TransformInPoint(const Matrix& mtr1, const Matrix& mtr2);

		MECHA_API Matrix MatrixRotationYawPitchRoll(const EulerAngles& eulerAngles);

		MECHA_API Matrix MatrixRotationYawPitchRollInverse(const EulerAngles& eulerAngles);

		MECHA_API void YawPitchRollFromMatrix(const Matrix& mtr, OUT EulerAngles& eulerAngles);

		MECHA_API Matrix InverseDirectionCosMatrix(const Matrix& mtr);

		MECHA_API Matrix MtrA_In_MtrB(const Matrix& mtrA, const Matrix& mtrB);


		// Quaternion
		MECHA_API MA_REAL QuaternionDot(const Quaternion& q1, const Quaternion& q2);

		MECHA_API Quaternion QuaternionSlerp(const Quaternion& q1, const Quaternion& q2, MA_REAL t);

		MECHA_API bool MatrixToQuaternion(const Matrix& mt, OUT Quaternion& res);

		MECHA_API Matrix QuaternionToMatrix(const Quaternion& qt);

		MECHA_API void RotateByQt(Quaternion qt, IN OUT Vector& vc);

		MECHA_API void RotateByQtNorm(const Quaternion& qt, IN OUT Vector& vc);
	}
}