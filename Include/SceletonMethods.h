#pragma once
#include "Definitions.h"
#include "Sceleton.h"
#include "SystemCoordinate.h"
#include "SceletonSupport.h"


using namespace mech_a;
using namespace mech_a::math;
using namespace mech_a::origin;


namespace mech_a {
	namespace sceleton {

		class MECHA_API IPartSceleton;

		MECHA_API void InterpolationState(const SceletonData::MotionData& motion, MA_REAL timeStamp, std::unordered_map<ULONG, SceletonData::ConnectionData>& connections
			, OUT std::unordered_map<ULONG, SceletonData::TypeLocalCS>& stateRes);

		MECHA_API SC LinearInterpolation(const SC& fromSC, const SC& toSC, MA_REAL pos);
		MECHA_API QuaternionCS LinearInterpolation(const QuaternionCS& fromSC, const QuaternionCS& toSC, MA_REAL pos);

		MECHA_API void GetLocalTransform(const SC& fromSC, const SC& toSC, OUT SC& localSC);
		MECHA_API void GetLocalTransform(const SC& fromSC, const SC& toSC, OUT EulerAnglesCS& localSC);
		MECHA_API void GetLocalTransform(const SC& fromSC, const SC& toSC, OUT QuaternionCS& localSC);
		MECHA_API void GetLocalTransform(const QuaternionCS& fromSC, const QuaternionCS& toSC, OUT QuaternionCS& localSC);

		MECHA_API void ReTransform(const SC& wcs, const SC& lcs, OUT SC& resWcs);
		MECHA_API void ReTransform(const SC& wcs, const EulerAnglesCS& lcs, OUT SC& resWcs);
		MECHA_API void ReTransform(const SC& wcs, const QuaternionCS& lcs, OUT SC& resWcs);
		MECHA_API void ReTransform(const QuaternionCS& wcs, const QuaternionCS& lcs, OUT QuaternionCS& resWcs);

		MECHA_API void ReTransformRevers(const SC& wcs, const EulerAnglesCS& lcs, OUT SC& resWcs);
		MECHA_API void ReTransformRevers(const SC& wcs, const QuaternionCS& lcs, OUT SC& resWcs);
		MECHA_API void ReTransformRevers(const QuaternionCS& wcs, const QuaternionCS& lcs, OUT QuaternionCS& resWcs);

		MECHA_API void UpdateSceletonFromConnection(Connection* pConnect, bool onlyUpDir = false);
		MECHA_API void UpdateSceletonDownWCS(Connection* pUpperConnect, const SceletonData::TypeGlobalCS& updateWCS);
		MECHA_API void UpdateSceletonDownBasicLCS(Connection* pUpperConnect, const SceletonData::TypeGlobalCS& updateBasicLCS);
		MECHA_API void UpdateSceletonDownLCS(Connection* pUpperConnect, const SceletonData::TypeLocalCS& updateLCS);
		MECHA_API void UpdateSceleton(Connection* pBeginConnect, const SceletonData::TypeGlobalCS& updateWCS);
		MECHA_API void UpdateSceletonUpLCS(Connection* pUpperConnect, const SceletonData::TypeLocalCS& updateLCS);

		MECHA_API void LoadParts(IN std::unordered_map<ULONG, ElementData>& elements, IN std::unordered_map<ULONG, SceletonData::ConnectionData>& connections
			, IN StateData& state, OUT std::unordered_map<ULONG, std::unique_ptr<IPartSceleton>>& parts);

		MECHA_API void RemoveInterpolationData(ULONG idConn, InterpolationData<SceletonData::TypeLocalCS, SceletonData::sInterpolationType>& interpolationData);

		MECHA_API void UpdateLocalPositions(const LocalPositions& newPositions, const std::pair<ULONG, SceletonData::TypeGlobalCS>& wcsBaseConnection, IN std::unordered_map<ULONG, std::unique_ptr<IPartSceleton>>& connections);

		// --------------------------- Sceleton processor --------------------------------------------------------------------------------------------------------------

		MECHA_API std::unordered_map<ULONG, SceletonData::TypeLocalCS::RotatorType> CalculateLimitKinematicCharaters(const SceletonData& sceletonData, const std::unordered_map<ULONG, MA_REAL>& motionTimeRelationCoeffs);

		MECHA_API CubicSplineFunction DifferenceInHeightOfConnections(const SceletonData::MotionData& motionData, ULONG idConnA, ULONG idConnB);

		MECHA_API LocalPositions IntermediateState(const LocalPositions& stateA, const LocalPositions& stateB, MA_REAL relationAtoB);
		
		MECHA_API SceletonData::TypeLocalCS::RotatorType DiffRotatorStates(const SceletonData::TypeLocalCS::RotatorType& fromState, const SceletonData::TypeLocalCS::RotatorType& toState);
		MECHA_API LocalPositions DiffStates(const LocalPositions& fromState, const LocalPositions& toState);
	}
}