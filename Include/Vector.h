#pragma once
#include "Constants.h"


namespace mech_a {

	namespace math {

		struct MECHA_API Vector
		{
			Vector() 
				: X(0), Y(0), Z(0), W(1) 
			{ }
			
			Vector(MA_REAL _x, MA_REAL _y, MA_REAL _z, MA_REAL _w = 1) 
				: X(_x), Y(_y), Z(_z), W(_w)
			{ }

			// operators
			Vector operator + (const Vector& vc) const;
			Vector operator - (const Vector& vc) const;
			Vector operator * (MA_REAL vl) const;
			Vector operator / (MA_REAL vl) const;

			Vector& operator += (const Vector& vc);
			Vector& operator -= (const Vector& vc);
			Vector& operator *= (MA_REAL vl);
			Vector& operator /= (MA_REAL vl);

			// methods
			Vector  Revers() const;	
			MA_REAL    Length() const;
			MA_REAL    LenghtSquare() const;
			bool    Normalize(MA_REAL tol = DEF_TOL);
			Vector  NormalWith(MA_REAL tol = DEF_TOL) const;
			bool    IsNormal(MA_REAL tol = DEF_TOL) const;

			Vector OrtX() const;
			Vector OrtY() const;
			Vector OrtZ() const;

			// statics
			static const Vector& NaN();
			static const Vector& Null();
			static const Vector& OX();
			static const Vector& OY();
			static const Vector& OZ();

			// fields
			MA_REAL X, Y, Z, W;

			// stream work
			Vector(std::stringstream& stream)
				: X(0), Y(0), Z(0), W(1)
			{
				FromStream(stream);
			}

			void ToStream(std::stringstream& stream) const
			{
				stream.write((char*)&X, sizeof(MA_REAL));
				stream.write((char*)&Y, sizeof(MA_REAL));
				stream.write((char*)&Z, sizeof(MA_REAL));
				stream.write((char*)&W, sizeof(MA_REAL));
			}

			void FromStream(std::stringstream& stream)
			{
				stream.read((char*)&X, sizeof(MA_REAL));
				stream.read((char*)&Y, sizeof(MA_REAL));
				stream.read((char*)&Z, sizeof(MA_REAL));
				stream.read((char*)&W, sizeof(MA_REAL));
			}
		};


		MECHA_API Vector operator * (const MA_REAL& vl, const Vector vc);
	}
}