#pragma once
#include "Constants.h"
#include "MathFunctions.h"


using namespace mech_a;
using namespace mech_a::math;


namespace mech_a {

	namespace origin {


		/**
		*  Kinematic state implementation
		*/
		class MECHA_API KinematicState
		{
		public:

			KinematicState();

			KinematicState(const Vector& dirVelocity, MA_REAL velocity, Vector dirAngularVelocity, MA_REAL angularVelocity);

			void Set(const Vector& dirVelocity, MA_REAL velocity);

			void SetAngularChars(const Vector& dirAngularVelocity, MA_REAL angularVelocity);

			void Set(const Vector& dirVelocity, MA_REAL velocity, const Vector& dirAngularVelocity, MA_REAL angularVelocity);


			const Vector&  DirVelocity() const;

			MA_REAL           Velocity() const;

			const Vector&  DirAngularVelocity() const;

			MA_REAL           AngularVelocity() const;


			void  SetDirVelocity(const Vector& dir);

			void  SetVelocity(MA_REAL vl);

			void  SetDirAngularVelocity(const Vector& dir);

			void  AngularVelocity(MA_REAL vl);

		private:

			// fields
			Vector  m_dirVelocity;
			MA_REAL m_velocity;

			Vector  m_dirAngularVelocity;
			MA_REAL m_angularVelocity;
		};
	}
}