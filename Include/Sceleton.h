#pragma once

#pragma warning(disable : 4251)
#pragma warning(disable : 4267)
#pragma warning(disable : 4100)

#include <string>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <list>

#include "Constants.h"
#include "Definitions.h"
#include "MathFunctions.h"
#include "CubicSpline.h"
#include "Euler_angles.h"
#include "SystemCoordinate.h"


using namespace mech_a;
using namespace mech_a::math;


namespace mech_a {
	namespace sceleton {

		/**
		*  Basic element of sceleton
		*/
		struct MECHA_API BasicData
		{
			BasicData()
				: m_ID(0)
			{ }

			BasicData(ULONG id, const std::wstring& name)
				: m_ID(id), m_Name(name)
			{ }

			virtual ~BasicData() { }

			ULONG         m_ID;
			std::wstring  m_Name;

			/** Work with stream */
			BasicData(IN std::stringstream& stream)
				: m_ID(0)
			{
				FromStream(stream);
			}

			virtual void ToStream(OUT std::stringstream& stream) const
			{
				stream.write((char*)&m_ID, sizeof(m_ID));
				WriteWstringToStream(m_Name, stream);
			}

			virtual void FromStream(IN std::stringstream& stream)
			{
				stream.read((char*)&m_ID, sizeof(m_ID));
				ReadWstringFromStream(m_Name, stream);
			}
		};


		/**
		*  ElementData of sceleton
		*/
		struct MECHA_API ElementData : public BasicData
		{
			ElementData()
				: m_ParentConnectId(0)
				, m_Lengh(0)
			{ }

			ElementData(ULONG id, const std::wstring& name = L"")
				: BasicData(id, name)
				, m_ParentConnectId(0)
				, m_Lengh(0)
			{ }

			ULONG                      m_ParentConnectId;
			std::unordered_set<ULONG>  m_ConnectIDs;
			MA_REAL                    m_Lengh;

			/** Work with stream */
			ElementData(IN std::stringstream& stream)
				: m_ParentConnectId(0)
				, m_Lengh(0)
			{
				FromStream(stream);
			}

			virtual void ToStream(OUT std::stringstream& stream) const override
			{
				BasicData::ToStream(stream);

				stream.write((char*)&m_ParentConnectId, sizeof(m_ParentConnectId));
				
				ULONG sz = ULONG(m_ConnectIDs.size());
				stream.write((char*)&sz, sizeof(sz));

				for (ULONG connId : m_ConnectIDs)
					stream.write((char*)&connId, sizeof(connId)); 

				stream.write((char*)&m_Lengh, sizeof(MA_REAL));
			}

			virtual void FromStream(IN std::stringstream& stream) override
			{
				BasicData::FromStream(stream);

				stream.read((char*)&m_ParentConnectId, sizeof(m_ParentConnectId));

				ULONG sizeIDs = 0;
				stream.read((char*)&sizeIDs, sizeof(sizeIDs));
		
				for (size_t i = 0; i < sizeIDs; i++)
				{
					ULONG currentId = 0;
					stream.read((char*)&currentId, sizeof(currentId));
					m_ConnectIDs.emplace(currentId);
				}

				stream.read((char*)&m_Lengh, sizeof(MA_REAL));
			}
		};


		/**
		*  Extents of freedom
		*/
		enum ExtentsFreedom
		{
			EF_Locked = 0,
			EF_YAW = 0x01,
			EF_PITCH = 0x02,
			EF_ROLL = 0x04,
			EF_YAW_PITCH = EF_YAW | EF_PITCH,
			EF_YAW_ROLL = EF_YAW | EF_ROLL,
			EF_PITCH_ROLL = EF_PITCH | EF_ROLL,
			EF_ALL = EF_YAW | EF_PITCH | EF_ROLL,
		};


		/**
		*  ConnectionData elements of sceleton
		*/
		template<class T_WCS>
		struct MECHA_API _ConnectionData : public BasicData
		{
			_ConnectionData()
				: m_ElementA(0)
				, m_ElementB(0)
				, m_ExtentsFreedom(EF_ALL)
			{ }

			_ConnectionData(ULONG id, const std::wstring& name = L"")
				: BasicData(id, name)
				, m_ElementA(0)
				, m_ElementB(0)
				, m_ExtentsFreedom(EF_ALL)
			{ }

			ULONG  m_ElementA;
			ULONG  m_ElementB;

			ExtentsFreedom  m_ExtentsFreedom;
			T_WCS           m_BasicLCS;

			/** Work with stream */
			_ConnectionData(IN std::stringstream& stream)
				: m_ElementA(0)
				, m_ElementB(0)
			{
				FromStream(stream);
			}

			virtual void ToStream(OUT std::stringstream& stream) const
			{
				BasicData::ToStream(stream);

				stream.write((char*)&m_ElementA, sizeof(m_ElementA));
				stream.write((char*)&m_ElementB, sizeof(m_ElementB));

				stream.write((char*)&m_ExtentsFreedom, sizeof(ExtentsFreedom));
				m_BasicLCS.ToStream(stream);
			}

			virtual void FromStream(IN std::stringstream& stream)
			{
				BasicData::FromStream(stream);

				stream.read((char*)&m_ElementA, sizeof(m_ElementA));
				stream.read((char*)&m_ElementB, sizeof(m_ElementB));

				stream.read((char*)&m_ExtentsFreedom, sizeof(ExtentsFreedom));
				m_BasicLCS.FromStream(stream);
			}
		};


		/**
		*  Begining state of sceleton
		*/
		template<class T_LCS, class T_WCS>
		struct MECHA_API _StateData
		{
			_StateData()
				: m_ID(0)
			{ }

			_StateData(ULONG id, const std::wstring& name)
				: m_ID(id)
				, m_Name(name)
			{ }

			_StateData(ULONG id, const std::wstring& name, const std::unordered_map<ULONG, T_WCS>& globalPoseElements, const std::unordered_map<ULONG, T_LCS>& localPoseElements)
				: m_ID(id)
				, m_Name(name)
				, m_GlobalPoseElements(globalPoseElements)
				, m_LocalPoseElements(localPoseElements)
			{ }

			ULONG         m_ID;
			std::wstring  m_Name;

			std::unordered_map<ULONG, T_WCS>     m_GlobalPoseElements;
			std::unordered_map<ULONG, T_LCS>     m_LocalPoseElements;

			/** Work with stream */
			_StateData(IN std::stringstream& stream)
			{
				FromStream(stream);
			}

			void ToStream(OUT std::stringstream& stream) const
			{
				stream.write((char*)&m_ID, sizeof(ULONG));
				WriteWstringToStream(m_Name, stream);

				MapToStream(m_GlobalPoseElements, stream);
				MapToStream(m_LocalPoseElements, stream);
			}

			void FromStream(IN std::stringstream& stream)
			{
				stream.read((char*)&m_ID, sizeof(ULONG));
				ReadWstringFromStream(m_Name, stream);

				MapFromStream(m_GlobalPoseElements, stream);
				MapFromStream(m_LocalPoseElements, stream);
			}
		};


		/**
		*  InterpolationData of motion
		*/
		enum InterpolationType
		{
			IT_Default = 0,
			IT_Linear_Quaternion = 1,
			IT_Cubical_Spline_Euler = 2,
			IT_Linear_Euler = 3,
		};


		/**
		*  InterpolationData of motion
		*/
		template<class T_LCS, InterpolationType IT>
		struct MECHA_API InterpolationData
		{
			InterpolationData()
			{ }

			/** Work with stream */
			InterpolationData(IN std::stringstream& stream)
			{
				FromStream(stream);
			}

			void ToStream(OUT std::stringstream& stream) const
			{ }

			void FromStream(IN std::stringstream& stream)
			{ }
		};


		/**
		*   Euler angles spline interpolation
		*/
		struct MECHA_API EulerAnglesSplineInterpolation
		{
			EulerAnglesSplineInterpolation()
			{ }

			EulerAnglesSplineInterpolation(const CubicSpline& yawIntrps, const CubicSpline& pitchIntrps, const CubicSpline& rollIntrps)
				: m_yawIntrps(yawIntrps)
				, m_pitchIntrps(pitchIntrps)
				, m_rollIntrps(rollIntrps)
			{ }

			CubicSpline  m_yawIntrps;
			CubicSpline  m_pitchIntrps;
			CubicSpline  m_rollIntrps;

			/** Work with stream */
			EulerAnglesSplineInterpolation(IN std::stringstream& stream)
			{
				FromStream(stream);
			}

			void ToStream(OUT std::stringstream& stream) const
			{
				m_yawIntrps.ToStream(stream);
				m_pitchIntrps.ToStream(stream);
				m_rollIntrps.ToStream(stream);
			}

			void FromStream(IN std::stringstream& stream)
			{
				m_yawIntrps.FromStream(stream);
				m_pitchIntrps.FromStream(stream);
				m_rollIntrps.FromStream(stream);
			}
		};

		typedef std::unordered_map<ULONG, EulerAnglesSplineInterpolation>    MapEulersCubSplIntrps;
		typedef std::map<MA_REAL, MapEulersCubSplIntrps>                     MapConnectionCubSplIntrps;

	}

	template<>
	inline void ValueToStream<sceleton::MapEulersCubSplIntrps>(const sceleton::MapEulersCubSplIntrps& val, OUT std::stringstream& stream)
	{
		MapToStream(val, stream);
	}

	template<>
	inline sceleton::MapEulersCubSplIntrps ValueFromStream<sceleton::MapEulersCubSplIntrps>(IN std::stringstream& stream)
	{
		sceleton::MapEulersCubSplIntrps res;
		MapFromStream(res, stream);
		return res;
	}

	namespace sceleton {

		/**
		*   Specialization for of EulerAngles
		*/
		template<>
		struct MECHA_API InterpolationData<mech_a::origin::EulerAnglesCS, InterpolationType::IT_Cubical_Spline_Euler>
		{
			InterpolationData()
			{ }

			MapConnectionCubSplIntrps  m_Splines; // Interpolation splines 

			/** Work with stream */
			InterpolationData(IN std::stringstream& stream)
			{
				FromStream(stream);
			}

			void ToStream(OUT std::stringstream& stream) const
			{
				MapToStream(m_Splines, stream);
			}

			void FromStream(IN std::stringstream& stream)
			{
				MapFromStream(m_Splines, stream);
			}
		};


		/**
		*   MotionData of sceleton
		*/
		template<class T_LCS, class T_WCS, InterpolationType IT>
		struct MECHA_API _MotionData
		{
			_MotionData()
				: m_ID(0)
				, m_ID_Relatively_Part(0)
			{ }

			_MotionData(ULONG id, ULONG id_Relatively_Part, const std::wstring& name)
				: m_ID(id)
				, m_ID_Relatively_Part(id_Relatively_Part)
				, m_Name(name)
			{ }

			ULONG                            m_ID;
			ULONG                            m_ID_Relatively_Part;
			std::wstring                     m_Name;

			std::map<MA_REAL, _StateData<T_LCS, T_WCS>>      m_States;               // states of sceleton
			InterpolationData<T_LCS, IT>                     m_InterpolationData;

			/** Work with stream */
			_MotionData(IN std::stringstream& stream) { FromStream(stream); }

			void ToStream(OUT std::stringstream& stream) const
			{
				stream.write((char*)&m_ID, sizeof(m_ID));
				stream.write((char*)&m_ID_Relatively_Part, sizeof(m_ID_Relatively_Part));
				WriteWstringToStream(m_Name, stream);

				MapToStream(m_States, stream);
				m_InterpolationData.ToStream(stream);
			}

			void FromStream(IN std::stringstream& stream)
			{
				stream.read((char*)&m_ID, sizeof(m_ID));
				stream.read((char*)&m_ID_Relatively_Part, sizeof(m_ID_Relatively_Part));
				ReadWstringFromStream(m_Name, stream);

				MapFromStream(m_States, stream);
				m_InterpolationData.FromStream(stream);
			}
		};


		/**
		*   Sceleton data
		*/
		template<class T_LCS, class T_WCS, InterpolationType IT>
		struct MECHA_API _SceletonData
		{
			/** Types */
			typedef T_LCS TypeLocalCS;
			typedef T_WCS TypeGlobalCS;
			typedef _ConnectionData<TypeGlobalCS> ConnectionData;
			typedef _MotionData<TypeLocalCS, TypeGlobalCS, IT> MotionData;
			typedef _StateData<T_LCS, T_WCS> StateData;

			/** Constructors */
			_SceletonData()
				: m_BeginState(1, L"Basic state")
			{ }

			_SceletonData(IN std::stringstream& stream) 
			{ 
				FromStream(stream); 
			}

			virtual ~_SceletonData() { }

			/** Fields */
			const static InterpolationType  sInterpolationType = IT;

			std::unordered_map<ULONG, _MotionData<T_LCS, T_WCS, IT>>  m_Motions;
			StateData                                                 m_BeginState;
			std::unordered_map<ULONG, ElementData>                    m_Elements;
			std::unordered_map<ULONG, ConnectionData>                 m_Connections;
			std::unordered_map<ULONG, StateData>                      m_SpecialStates;

			/** Work with stream */ 
			virtual void ToStream(OUT std::stringstream& stream)
			{
				DWORD vl = MAJOR_VERSION;
				stream.write((char*)&vl, sizeof(DWORD));
				
				vl = MINOR_VERSION;
				stream.write((char*)&vl, sizeof(DWORD));

				vl = LOCAL_WORLD_CS_TYPE;
				stream.write((char*)&vl, sizeof(DWORD));

				vl = sInterpolationType;
				stream.write((char*)&vl, sizeof(DWORD));

				MapToStream(m_Motions, stream);
				m_BeginState.ToStream(stream);
				MapToStream(m_Elements, stream);
				MapToStream(m_Connections, stream);
				MapToStream(m_SpecialStates, stream);
			}

			virtual void FromStream(IN std::stringstream& stream);
		};


#if LOCAL_WORLD_CS_TYPE == LOCAL_EULER_WORLD_SC

	#if INTERPOLATION_EULER_ANGLES == INTERPOLATION_CUBICAL_SPLINE

		typedef _SceletonData<mech_a::origin::EulerAnglesCS, mech_a::origin::SC, InterpolationType::IT_Cubical_Spline_Euler> SceletonData;
		typedef _StateData<mech_a::origin::EulerAnglesCS, mech_a::origin::SC> StateData;

	#endif

	#if INTERPOLATION_EULER_ANGLES == INTERPOLATION_LINEAR

		typedef _SceletonData<mech_a::origin::EulerAnglesCS, mech_a::origin::SC, InterpolationType::IT_Linear_Euler> SceletonData;
		typedef _StateData<mech_a::origin::EulerAnglesCS, mech_a::origin::SC> StateData;

	#endif

#endif

#if LOCAL_WORLD_CS_TYPE == LOCAL_QUAT_WORLD_SC

		typedef _SceletonData<mech_a::origin::QuaternionCS, mech_a::origin::SC, InterpolationType::IT_Linear_Quaternion> SceletonData;
		typedef _StateData<mech_a::origin::QuaternionCS, mech_a::origin::SC> StateData;

#endif

#if LOCAL_WORLD_CS_TYPE == LOCAL_QUAT_WORLD_QUAT

		typedef _SceletonData<mech_a::origin::QuaternionCS, mech_a::origin::QuaternionCS, InterpolationType::IT_Linear_Quaternion> SceletonData;
		typedef _StateData<mech_a::origin::QuaternionCS, mech_a::origin::QuaternionCS> StateData;

#endif


		typedef std::unordered_map<ULONG, SceletonData::TypeLocalCS> LocalPositions;
		typedef std::unordered_map<ULONG, SceletonData::TypeGlobalCS> GlobalPositions;

	}
}