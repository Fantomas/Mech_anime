#pragma once
#include <set>
#include "Constants.h"


using namespace mech_a;


namespace mech_a {
	namespace origin {

		class MECHA_API ID_Genearator
		{
		public:

			ID_Genearator(UINT counter = 1) 
				: m_Counter(counter) 
			{
				assert(counter);
			}

			UINT GetID()
			{
				if (m_DeletedItems.empty())
					return m_Counter++;

				auto it = m_DeletedItems.begin();
				UINT res = *it;
				
				m_DeletedItems.erase(it);
				return res;
			}

			bool RemoveID(UINT id)
			{
				if (!id)
					return false;

				if (id >= m_Counter)
					return false;

				auto res = m_DeletedItems.insert(id);
				return res.second;
			}

		private:

			UINT            m_Counter;
			std::set<UINT>  m_DeletedItems;
		};

	}
}
