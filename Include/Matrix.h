#pragma once
#include "Constants.h"
#include "Vector.h"


namespace mech_a {

	namespace math {


		struct MECHA_API Matrix
		{
			Matrix();

			Matrix(MA_REAL v_11, MA_REAL v_12, MA_REAL v_13, MA_REAL v_14
				, MA_REAL v_21, MA_REAL v_22, MA_REAL v_23, MA_REAL v_24
				, MA_REAL v_31, MA_REAL v_32, MA_REAL v_33, MA_REAL v_34
				, MA_REAL v_41, MA_REAL v_42, MA_REAL v_43, MA_REAL v_44
				);

			// operators
			MA_REAL* operator [] (UINT row);
			const MA_REAL* operator [] (UINT row) const;			
			MA_REAL operator () (UINT row, UINT col) const;

			Matrix operator * (MA_REAL vl) const;
			Matrix& operator *= (MA_REAL vl);
			Matrix operator / (MA_REAL vl) const;
			Matrix& operator /= (MA_REAL vl);

			Matrix operator + (const Matrix& mtr) const;
			Matrix& operator += (const Matrix& mtr);
			Matrix operator - (const Matrix& mtr) const;
			Matrix& operator -= (const Matrix& mtr);

			Matrix operator * (const Matrix& mtr) const;
			Matrix& operator *= (const Matrix& mtr);

			// methods

			// fields
			union {
				struct {
					MA_REAL        V_11, V_12, V_13, V_14;
					MA_REAL        V_21, V_22, V_23, V_24;
					MA_REAL        V_31, V_32, V_33, V_34;
					MA_REAL        V_41, V_42, V_43, V_44;
				};
				MA_REAL VLS[4][4];
			};

			Matrix(std::stringstream& stream)
				: V_11(1), V_12(0), V_13(0), V_14(0)
				, V_21(0), V_22(1), V_23(0), V_24(0)
				, V_31(0), V_32(0), V_33(1), V_34(0)
				, V_41(0), V_42(0), V_43(0), V_44(1)
			{
				FromStream(stream);
			}

			void ToStream(std::stringstream& stream) const
			{
				stream.write((char*)this, sizeof(Matrix));
			}

			void FromStream(std::stringstream& stream)
			{
				stream.read((char*)this, sizeof(Matrix));
			}
		};
	}
}