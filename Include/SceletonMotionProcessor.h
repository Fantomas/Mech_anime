#pragma once
#include <limits>
#include <memory>
#include <functional>

#include "Sceleton.h"
#include "SceletonSupport.h"


namespace mech_a {
	namespace sceleton {

		/**
		*  Tactic character
		*/
		enum TacticCharacter
		{
			TC_Seat = 1,
			TC_Ready = 2,
			TC_Normal = 3,
		};

		/**
		*  Event motion
		*/
		enum MotionEvent
		{
			ME_JumpStart = 1,
			ME_JumpEnd = 2,
			ME_Bump = 3,
		};


		struct MECHA_API MotionCharacteristics
		{
			MotionCharacteristics()
				: m_TimeMotion(0)
				, m_AverageSpeed(0)
			{ }

			MotionCharacteristics(MotionsMapPtr motionPtr, MA_REAL timeMotion, MA_REAL averageSpeed)
				: m_MotionPtr(motionPtr)
				, m_TimeMotion(timeMotion)
				, m_AverageSpeed(averageSpeed)
			{ }

			MotionsMapPtr  m_MotionPtr;
			MA_REAL  m_TimeMotion; 
			MA_REAL  m_AverageSpeed;
			
			// CubicSplineFunction  m_DifferenceInHeightOfConnections;
		};


		struct MECHA_API AltitudeDiffChars
		{
			AltitudeDiffChars()
				: m_ID_ClassAltitude(0)
				, m_Altitude(0)
			{ }

			ULONG    m_ID_ClassAltitude;
			MA_REAL  m_Altitude;

			// from 0 to 1. // difference heights
			std::map<MA_REAL, std::map<MA_REAL, MotionCharacteristics>>  m_MotionsChars;
		};


		struct MECHA_API AngularSpeedSettings  
		{
			AngularSpeedSettings()
				: m_ID_AngularSpeed(0)
				, m_AngularSpeed(0)
			{ }

			ULONG    m_ID_AngularSpeed;
			MA_REAL  m_AngularSpeed;

			std::map<MA_REAL, AltitudeDiffChars>  m_AltitudeDiffSettings;
		};


		struct MECHA_API LegsSettings
		{
			LegsSettings()
				: m_IsLeftLeg(true)
			{ }

			LegsSettings(bool isLeftLeg)
				: m_IsLeftLeg(isLeftLeg)
			{ }

			bool  m_IsLeftLeg;

			std::map<MA_REAL, AngularSpeedSettings>  m_AngularSpeeds;
		};


		struct MECHA_API SpeedSettings
		{
			SpeedSettings()
				: m_ID_SpeedClass(0)
				, m_Speed(0)
				, m_LeftLegSpeeds(true)
				, m_RightLegSpeeds(false)
			{ }

			ULONG  m_ID_SpeedClass;
			MA_REAL  m_Speed;

			LegsSettings  m_LeftLegSpeeds;
			LegsSettings  m_RightLegSpeeds;
		}; 


		/**
		*  Tactic character settings
		*/
		struct MECHA_API TacticCharacterSettings
		{
			TacticCharacterSettings()
				: m_TacticCharacter(TacticCharacter::TC_Normal)
				, m_MinTurnSpeedAbs(MA_REAL_MAX)
				, m_MaxTurnSpeedAbs(0)
			{ }

			TacticCharacter  m_TacticCharacter;

			std::map<MA_REAL, SpeedSettings>  m_ForwardSpeedSettings;
			std::map<MA_REAL, SpeedSettings>  m_BackSpeedSettings;

			MA_REAL  m_MinTurnSpeedAbs;
			MA_REAL  m_MaxTurnSpeedAbs;
			LegsSettings  m_TurningFromLeftLegSettingsRight;
			LegsSettings  m_TurningFromRightLegSettingsRight;
			LegsSettings  m_TurningFromLeftLegSettingsLeft;
			LegsSettings  m_TurningFromRightLegSettingsLeft;

			std::map<MA_REAL, LocalPositions>  m_StandingPosesByDiffAltitudeLegs;
		};


		/**
		*  Motion processor settings
		*/
		struct MECHA_API MotionProcessorSettings
		{
			std::unordered_map<TacticCharacter, TacticCharacterSettings>  m_SettingsByTacticCharacter;

			std::unordered_map<ULONG, SceletonData::TypeLocalCS::RotatorType>  m_LimitKinematicCharaters; // Limit kinematic rotation charaters
			MA_REAL  m_CoeffLimitCharacters;

			MA_REAL  m_MoveForvardTime;
			MA_REAL  m_MoveBackTime;
			MA_REAL  m_MoveTurnTime;
		};


		/**
		*  Motion processor state
		*/
		enum MotionProcessorState
		{
			MPS_MoveForward,
			MPS_MoveBack,
			MPS_TurnToLeft,
			MPS_TurnToRight,
			MPS_InToStay,
			MPS_Stay,
			MPS_InToJump,
			MPS_Jump,
			MPS_AfterJump,
		};


		/**
		*  Move result
		*/
		struct MECHA_API MoveResult
		{
			MoveResult()
				: m_dRotation(0)
			{ }

			mech_a::math::Vector  m_dPos;            // in XOY plane
			MA_REAL               m_dRotation;       // arround OZ direction
		};


		/**
		*  Move result
		*/
		struct IExternalInformer
		{
			virtual MA_REAL GetDiffAltitude() const = 0;
		};


		// typedef std::unordered_map<ULONG, SceletonData::TypeLocalCS> LocalPositions;


		/**
		*  Basic element of sceleton
		*/
		class MECHA_API SceletonMotionProcessor
		{
		public:

			SceletonMotionProcessor(IN std::stringstream& sceletonData, const std::string& settings_XML_frmt);

			// force - no stop parameter
			MoveResult Move(MA_REAL acceleration, MA_REAL angularAcceleration, MA_REAL alphaVertical, MA_REAL dT);

			MoveResult MoveOnEvent(MotionEvent motionEvent, MA_REAL dT);

			MA_REAL Velocity() const { return m_NextVelocity; }
			MA_REAL AngularVelocity() const { return m_NextAngularVelocity; }

			void StopForwardMotion() { m_Velocity = m_NextVelocity = 0; }
			void StopTurnMotion() { m_AngularVelocity = m_NextAngularVelocity = 0; }

			Connection* GetConnection(UINT id) const
			{
				auto itrtPart = m_Parts.find(id);
				if (itrtPart == m_Parts.end())
					return NULL;

				return dynamic_cast<Connection*>(itrtPart->second.get());
			}

			std::unordered_map<UINT, IPartSceleton*> GetParts() const 
			{
				std::unordered_map<UINT, IPartSceleton*> res;

				for (auto& itrtPair : m_Parts)
					res.emplace(itrtPair.first, itrtPair.second.get());

				return res; 
			}

		private:

			SceletonMotionProcessor(const SceletonMotionProcessor& sceletonMotionProcessor) { }
			SceletonMotionProcessor& operator = (const SceletonMotionProcessor& sceletonMotionProcessor) { return *this; }

			LocalPositions calculatePositions(const AltitudeDiffChars& altitudeSettings);
			LocalPositions calculatePositions(const AngularSpeedSettings& speedSettings, MA_REAL alphaVertical);
			LocalPositions calculatePositions(const LegsSettings& legSettings, MA_REAL alphaVertical);

			LocalPositions intermediateState(MotionsMapPtr motA, MotionsMapPtr motB, MA_REAL relationAtoB);
			LocalPositions intermediateState(const AltitudeDiffChars& charsA, const AltitudeDiffChars& charsB, MA_REAL relationAtoB);
			LocalPositions intermediateState(const AngularSpeedSettings& charsA, const AngularSpeedSettings& charsB, MA_REAL relationAtoB, MA_REAL alphaVertical);
			LocalPositions intermediateState(const LegsSettings& lowerLegSettings, const LegsSettings& upperLegSettings, MA_REAL relationAtoB, MA_REAL alphaVertical);

			LocalPositions move(const SpeedSettings& speedSettings, MA_REAL alphaVertical, MA_REAL moveTime, MA_REAL dT);
			LocalPositions move(const SpeedSettings& lowerSpStt, const SpeedSettings& upperSpStt, MA_REAL alphaVertical, MA_REAL moveTime, MA_REAL dT);
			
			LocalPositions moveForward(MA_REAL alphaVertical, MA_REAL dT);	
			LocalPositions moveBack(MA_REAL alphaVertical, MA_REAL dT);
			LocalPositions turning(MA_REAL alphaVertical, MA_REAL dT, bool toLeft);

			void selectStatePosesItrt();
			void setLocalPositions(const LocalPositions& localPoses, MA_REAL dT);

			// fields
			IExternalInformer*  m_pExternalInformer;

			SceletonData  m_SceletonData;
			StateData     m_ExecutableState;
			std::unordered_map<ULONG, std::unique_ptr<IPartSceleton>>  m_Parts;
			std::map<MA_REAL, LocalPositions>::const_iterator  m_SatatePosItrt;

			Connection*  m_pLeftToe;
			Connection*  m_pRightToe;
			Connection*  m_pTorso;

			MotionProcessorSettings  m_MotionProcessorSettings;		
			MotionProcessorState     m_MotionProcessorState;    // current state of motion

			bool                 m_LeftLeg;          // current motion produced from left leg
			TacticCharacter      m_TacticCharacter;  // current tactic
			MA_REAL              m_DiffAltitudeLegs; // current difference altitude of legs 

			MA_REAL  m_TimeStamp;     // current time stamp

			MA_REAL  m_Velocity;      // current velocity
			MA_REAL  m_NextVelocity;  // Velocity next time period

			MA_REAL  m_AngularVelocity;      // current angular velocity
			MA_REAL  m_NextAngularVelocity;  // AngularVelocity next time period
		};

	}
}