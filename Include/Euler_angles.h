#pragma once
#include "Constants.h"


namespace mech_a {

	namespace math {

		struct MECHA_API EulerAngles
		{
			EulerAngles() : m_yaw(0), m_pitch(0), m_roll(0)
			{ }

			EulerAngles(MA_REAL yaw, MA_REAL pitch, MA_REAL roll) 
				: m_yaw(yaw)
				, m_pitch(pitch)
				, m_roll(roll)
			{ }

			EulerAngles operator + (const EulerAngles& eAngs) const
			{
				return EulerAngles(m_pitch + eAngs.m_pitch, m_roll + eAngs.m_roll, m_yaw + eAngs.m_yaw);
			}

			EulerAngles operator - (const EulerAngles& eAngs) const
			{
				return EulerAngles(m_pitch - eAngs.m_pitch, m_roll - eAngs.m_roll, m_yaw - eAngs.m_yaw);
			}

			EulerAngles& operator *= (MA_REAL mltpl)
			{
				m_pitch *= mltpl;
				m_roll *= mltpl;
				m_yaw *= mltpl;

				return *this;
			}

			EulerAngles operator * (MA_REAL mltpl) const
			{
				return EulerAngles(m_pitch * mltpl, m_roll * mltpl, m_yaw * mltpl);
			}

			EulerAngles Normal() const;

			MA_REAL    m_yaw;          // around Y axis (1 rotation)
			MA_REAL    m_pitch;        // around X axis (2 rotation)
			MA_REAL    m_roll;         // around Z axis (3 rotation)

			// stream work
			EulerAngles(std::stringstream& stream)
				: m_yaw(0), m_pitch(0), m_roll(0)
			{
				FromStream(stream);
			}

			void ToStream(std::stringstream& stream) const
			{
				stream.write((char*)this, sizeof(EulerAngles));
			}

			void FromStream(std::stringstream& stream)
			{
				stream.read((char*)this, sizeof(EulerAngles));
			}
		};

	}
}