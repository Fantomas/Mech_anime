#pragma once

#pragma warning(disable : 4251)
#pragma warning(disable : 4267)
#pragma warning(disable : 4100)
#pragma warning(disable : 4702)
#pragma warning(disable : 4706)

#include <memory>
#include <string>
#include <sstream>

#include <map>
#include <unordered_map>
#include <algorithm>

#include <windows.h>


#define MAJOR_VERSION 0
#define MINOR_VERSION 1


#define OUT 
#define IN


#define DOUBLE_PRECISION
#undef DOUBLE_PRECISION


#ifdef DOUBLE_PRECISION
	#define MA_REAL	double
	#define DEF_TOL 1e-12
	#define PI		3.14159265358979323846
	#define PI2		6.283185307179586476925
	#define PIH2	1.57079632679489661923
	#define PIH4	0.785398163397448309616
	#define MA_REAL_MAX DBL_MAX
#else
	#define MA_REAL	float
	#define DEF_TOL 1e-6
	#define PI		3.1415926535f
	#define PI2		6.2831853071f
	#define PIH2	1.5707963267f
	#define PIH4	0.78539816339f
	#define MA_REAL_MAX FLT_MAX
#endif	


#ifdef _LIB
	#define MECHA_API
#else
	#ifdef MECH_ANIME_EXPORTS
		#define MECHA_API __declspec(dllexport)
	#else
		#define MECHA_API __declspec(dllimport)
	#endif
#endif

// types of local - world coordinate system for sceleton

#define LOCAL_EULER_WORLD_SC 1
#define LOCAL_QUAT_WORLD_SC 2
#define LOCAL_QUAT_WORLD_QUAT 3

#define LOCAL_WORLD_CS_TYPE LOCAL_QUAT_WORLD_QUAT /*LOCAL_EULER_WORLD_SC*/

// types of interpolation of euler angles
#define INTERPOLATION_CUBICAL_SPLINE 1
#define INTERPOLATION_LINEAR 2

#define INTERPOLATION_EULER_ANGLES INTERPOLATION_CUBICAL_SPLINE

//

namespace mech_a {

	inline void WriteWstringToStream(const std::wstring& str, std::stringstream& stream)
	{
		ULONG lnStr = ULONG(str.length());
		
		stream.write((char*)&lnStr, sizeof(lnStr));
		stream.write((char*)str.data(), sizeof(wchar_t)*lnStr);
	}

	inline void ReadWstringFromStream(std::wstring& str, std::stringstream& stream)
	{
		ULONG lnStr = 0;
		stream.read((char*)&lnStr, sizeof(lnStr));

		str.resize(lnStr, 0);	
		stream.read((char*)str.data(), sizeof(wchar_t)*lnStr);
	}


	template<class T>
	inline void ValueToStream(const T& val, OUT std::stringstream& stream)
	{
		val.ToStream(stream);
	}

	template<>
	inline void ValueToStream<ULONG>(const ULONG& val, OUT std::stringstream& stream)
	{
		stream.write((char*)&val, sizeof(val));
	}

	template<>
	inline void ValueToStream<MA_REAL>(const MA_REAL& val, OUT std::stringstream& stream)
	{
		stream.write((char*)&val, sizeof(MA_REAL));
	}

	template<class T_MAP>
	inline void MapToStream(const T_MAP& map, OUT std::stringstream& stream)
	{
		ULONG sz = ULONG(map.size());
		stream.write((char*)&sz, sizeof(sz));

		for (const auto& element : map)
		{
			ValueToStream(element.first, stream);
			ValueToStream(element.second, stream);
		}
	}


	template<class T>
	inline T ValueFromStream(IN std::stringstream& stream)
	{
		return T(stream);
	}

	template<>
	inline ULONG ValueFromStream<ULONG>(IN std::stringstream& stream)
	{
		ULONG res = 0;
		stream.read((char*)&res, sizeof(res));
		return res;
	}

	template<>
	inline MA_REAL ValueFromStream<MA_REAL>(IN std::stringstream& stream)
	{
		MA_REAL value = 0;
		stream.read((char*)&value, sizeof(MA_REAL));
		return value;
	}

	template<class T_MAP>
	inline void MapFromStream(OUT T_MAP& map, IN std::stringstream& stream)
	{
		ULONG szMap = 0;
		stream.read((char*)&szMap, sizeof(szMap));

		for (ULONG i = 0; i < szMap; i++)
		{
			const T_MAP::key_type& id = ValueFromStream<T_MAP::key_type>(stream);
			const T_MAP::mapped_type& value = ValueFromStream<T_MAP::mapped_type>(stream);
			map.emplace(id, value);
		}
	}


	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

	enum BoundaryResult
	{
		BR_EmptyContainer,
		BR_OK,
		BR_OutOfRangeLeft,
		BR_OutOfRangeRight,
	};

	template<class T_PARAMETR, class T_VALUE>
	inline BoundaryResult GetBoundary(const std::map<T_PARAMETR, T_VALUE>& container, T_PARAMETR parametr
		, OUT typename std::map<T_PARAMETR, T_VALUE>::const_iterator& lItrt
		, OUT typename std::map<T_PARAMETR, T_VALUE>::const_iterator& rItrt)
	{
		if (!container.size())
			return BoundaryResult::BR_EmptyContainer;

		rItrt = container.lower_bound(parametr);
		if (rItrt == container.end())
		{
			lItrt = std::prev(rItrt);
			return BoundaryResult::BR_OutOfRangeRight;
		}

		if (rItrt->first == parametr)
		{
			lItrt = rItrt;
			return BoundaryResult::BR_OK;
		}

		if (rItrt == container.begin())
		{
			lItrt = container.end();
			return BoundaryResult::BR_OutOfRangeLeft;
		}

		lItrt = std::prev(rItrt);
		return BoundaryResult::BR_OK;
	}

}

