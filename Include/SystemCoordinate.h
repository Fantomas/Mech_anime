#pragma once
#include "Constants.h"
#include "MathFunctions.h"
#include "Definitions.h"


using namespace mech_a;
using namespace mech_a::math;


namespace mech_a {
	
	namespace origin {

		/** 
		*  System coordinate implementation
		*/
		class MECHA_API SC
		{
		public:

			SC();
			SC(const Matrix& dirCosMtr);
			SC(const Vector& dirX, const Vector& dirY, const Vector& pos);
			SC(const Vector& dirX, const Vector& dirY, const Vector& dirZ, const Vector& pos);	
			SC(std::stringstream& stream) { FromStream(stream); }

			explicit SC(const EulerAnglesCS& eulSc);
			explicit SC(const QuaternionCS& quatSc);

			SC& operator = (const SC& sc);
			operator EulerAnglesCS () const;
			operator QuaternionCS () const;

			/** Get direction Z */
			Vector           DirZ() const;		

			/** Get direction X */
			Vector           DirX() const;	

			/** Get direction Y */
			Vector           DirY() const;	

			/** Get position */
			Vector           Pos() const;	

			/** Set position */
			void             Pos(const Vector& pos);

			/** Get matrix transform */
			const Matrix&    Tr_Matrix() const;	

			/** Set system coordinate */
			void SetSC(const Matrix& sc) { setSC(sc); onDirCosMtrChanged(); }

			/** Set system coordinate by normals */
			void SetSC(const Vector& dirX, const Vector& dirY, const Vector& pos);

			/** Set system coordinate by normals */
			void SetSC(const Vector& dirX, const Vector& dirY, const Vector& dirZ, const Vector& pos);

			/** Transform system coordinate */
			void Transform(const Matrix& tr);

			/** Transform system coordinate in place */
			void TransformInPlace(const Matrix& tr);

			// Rotates
			/** Rotate around world OX */
			const Matrix& Rotate_World_X(MA_REAL angl);

			/** Rotate around world OY */
			const Matrix& Rotate_World_Y(MA_REAL angl);

			/** Rotate around world OZ */
			const Matrix& Rotate_World_Z(MA_REAL angl);

			/** Rotate around position point SC world OX */
			const Matrix& Rotate_Pos_World_X(MA_REAL angl);

			/** Rotate around position point SC world OY */
			const Matrix& Rotate_Pos_World_Y(MA_REAL angl);

			/** Rotate around position point SC world OZ */
			const Matrix& Rotate_Pos_World_Z(MA_REAL angl);

			/** Rotate around position point and OX SC*/
			const Matrix& Rotate_SC_X(MA_REAL angl);

			/** Rotate around position point and OY SC*/
			const Matrix& Rotate_SC_Y(MA_REAL angl);

			/** Rotate around position point and OZ SC*/
			const Matrix& Rotate_SC_Z(MA_REAL angl);

			/** Save stream */
			virtual void ToStream(std::stringstream& stream) const { m_dirCosMtr.ToStream(stream); }

			/** Load from stream */
			virtual void FromStream(std::stringstream& stream) { m_dirCosMtr.FromStream(stream); }

			static SC Default() { return SC(); }

		protected:

			void setSC(const Matrix& sc);
			void setSC(const Vector& dirX, const Vector& dirY, const Vector& pos);
			void setSC(const Vector& dirX, const Vector& dirY, const Vector& dirZ, const Vector& pos);
			void setPos(const Vector& pos);

			virtual void onDirCosMtrChanged() { }

			// fields
			Matrix    m_dirCosMtr;
		};
	}
}