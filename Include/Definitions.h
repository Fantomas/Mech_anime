#pragma once
#include "Constants.h"
#include "Vector.h"
#include "Matrix.h"
#include "Quaternion.h"
#include "MathFunctions.h"
#include "Euler_angles.h"


using namespace mech_a;
using namespace mech_a::math;


namespace mech_a {
	namespace origin {

		struct MECHA_API EulerAnglesCS
		{
			EulerAnglesCS()
			{ }

			EulerAnglesCS(const EulerAngles& angles, const Vector& pos)
				: m_Angles(angles)
				, m_Pos(pos)
			{ }

			EulerAnglesCS(MA_REAL x, MA_REAL y, MA_REAL z, MA_REAL yaw, MA_REAL pitch, MA_REAL roll)
				: m_Angles(yaw, pitch, roll)
				, m_Pos(x, y, z)
			{ }

			typedef EulerAngles RotatorType;

			EulerAngles  m_Angles;
			Vector       m_Pos;

			RotatorType Rotator() const
			{
				return m_Angles;
			}


			EulerAnglesCS(IN std::stringstream& stream)
			{
				FromStream(stream);
			}

			virtual void ToStream(OUT std::stringstream& stream) const
			{
				m_Angles.ToStream(stream);
				m_Pos.ToStream(stream);
			}

			virtual void FromStream(IN std::stringstream& stream)
			{
				m_Angles.FromStream(stream);
				m_Pos.FromStream(stream);
			}

			static EulerAnglesCS Default()
			{ return EulerAnglesCS(); }
		};

		struct MECHA_API QuaternionCS
		{
			QuaternionCS() 
			{ }

			QuaternionCS(const EulerAnglesCS& cs);

			QuaternionCS(const Quaternion& quaternion, const Vector& pos)
				: m_Quaternion(quaternion)
				, m_Pos(pos)
			{ }

			typedef Quaternion RotatorType;

			Quaternion   m_Quaternion;
			Vector       m_Pos;

			static QuaternionCS Default() 
			{ return QuaternionCS(); }

			const Vector& Pos() const
			{
				return m_Pos;
			}

			Vector DirZ() const
			{
				Vector dir = Vector::OZ();
				RotateByQtNorm(m_Quaternion, dir);
				return dir;
			}

			Vector DirY() const
			{
				Vector dir = Vector::OY();
				RotateByQtNorm(m_Quaternion, dir);
				return dir;
			}

			Vector DirX() const
			{
				Vector dir = Vector::OX();
				RotateByQtNorm(m_Quaternion, dir);
				return dir;
			}

			QuaternionCS(IN std::stringstream& stream)
			{
				FromStream(stream);
			}

			QuaternionCS Inverse() const
			{
				Quaternion conjugCt = m_Quaternion.GetConjugate();
				Vector resVc = m_Pos * -1;
				RotateByQtNorm(conjugCt, resVc);
				return QuaternionCS(m_Quaternion.GetConjugate(), resVc);
			}

			Vector operator * (const Vector& vc) const
			{
				Vector resVc = vc;
				RotateByQtNorm(m_Quaternion, resVc);
				return resVc + m_Pos;
			}

			QuaternionCS operator * (const QuaternionCS& vc) const
			{
				return QuaternionCS(m_Quaternion * vc.m_Quaternion, *this * vc.m_Pos);
			}

			RotatorType Rotator() const
			{
				return m_Quaternion;
			}

			virtual void ToStream(OUT std::stringstream& stream) const
			{
				m_Quaternion.ToStream(stream);
				m_Pos.ToStream(stream);
			}

			virtual void FromStream(IN std::stringstream& stream)
			{
				m_Quaternion.FromStream(stream);
				m_Pos.FromStream(stream);
			}
		};


		class MECHA_API SC;

		void MECHA_API FromCS_ToCS(const QuaternionCS& from, OUT EulerAnglesCS& to);
		void MECHA_API FromCS_ToCS(const QuaternionCS& from, OUT SC& to);
		void MECHA_API FromCS_ToCS(const QuaternionCS& from, OUT QuaternionCS& to);

		void MECHA_API FromCS_ToCS(const SC& from, OUT EulerAnglesCS& to);
		void MECHA_API FromCS_ToCS(const SC& from, OUT QuaternionCS& to);
		void MECHA_API FromCS_ToCS(const SC& from, OUT SC& to);

		void MECHA_API FromCS_ToCS(const EulerAnglesCS& from, OUT EulerAnglesCS& to);	
		void MECHA_API FromCS_ToCS(const EulerAnglesCS& from, OUT QuaternionCS& to);
		void MECHA_API FromCS_ToCS(const EulerAnglesCS& from, OUT SC& to);
	}
}