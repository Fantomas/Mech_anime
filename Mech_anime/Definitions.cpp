#include "stdafx.h"
#include "Definitions.h"
#include "SystemCoordinate.h"
#include "Euler_angles.h"
#include "MathFunctions.h"


using namespace mech_a;
using namespace mech_a::math;


namespace mech_a {
	namespace origin {


		void FromCS_ToCS(const QuaternionCS& from, OUT EulerAnglesCS& to)
		{
			SC sc(from);
			to = (EulerAnglesCS)sc;
		}

		void FromCS_ToCS(const SC& from, OUT EulerAnglesCS& to)
		{
			to = (EulerAnglesCS)from;
		}

		void FromCS_ToCS(const EulerAnglesCS& from, OUT EulerAnglesCS& to)
		{
			to = from;
		}

		void FromCS_ToCS(const EulerAnglesCS& from, OUT QuaternionCS& to)
		{
			SC sc(from);
			to = (QuaternionCS)sc;
		}

		void FromCS_ToCS(const EulerAnglesCS& from, OUT SC& to)
		{
			to = SC(from);
		}


		void MECHA_API FromCS_ToCS(const QuaternionCS& from, OUT SC& to)
		{
			to = SC(from);
		}

		void MECHA_API FromCS_ToCS(const QuaternionCS& from, OUT QuaternionCS& to)
		{
			to = from;
		}

		void MECHA_API FromCS_ToCS(const SC& from, OUT QuaternionCS& to)
		{
			to = (QuaternionCS)from;
		}

		void MECHA_API FromCS_ToCS(const SC& from, OUT SC& to)
		{
			to = from;
		}


		QuaternionCS::QuaternionCS(const EulerAnglesCS& cs)
		{
			FromCS_ToCS(cs, *this);
		}

	}

	namespace math {

		EulerAngles EulerAngles::Normal() const
		{
			EulerAngles res;

			res.m_pitch = AngSetToRound(m_pitch);
			res.m_roll = AngSetToRound(m_roll);
			res.m_yaw = AngSetToRound(m_yaw);

			return res;
		}

	}

}