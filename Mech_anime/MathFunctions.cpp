#include "stdafx.h"
#include "MathFunctions.h"


using namespace std;


namespace mech_a {

	namespace math {

		bool Compare(MA_REAL vl1, MA_REAL vl2, MA_REAL tol)
		{
			return fabs(vl1 - vl2) <= tol;
		}

		MA_REAL Round(MA_REAL val, MA_REAL fraction)
		{
			if (val > 0)
				return (MA_REAL)((__int64)(val / fraction + MA_REAL(0.5001))) * fraction;

			if (val < 0)
				return (MA_REAL)((__int64)(val / fraction - MA_REAL(0.5001))) * fraction;

			return 0;
		}

		MA_REAL Min(MA_REAL vl1, MA_REAL vl2)
		{
			return vl1 < vl2 ? vl1 : vl2;
		}

		MA_REAL Max(MA_REAL vl1, MA_REAL vl2)
		{
			return vl1 > vl2 ? vl1 : vl2;
		}


		MA_REAL AngSetToRound(MA_REAL ang)
		{
			MA_REAL remdr = fmod(ang, PI2);

			if (fabs(remdr) > PI)
			{
				if (remdr > 0)
					remdr -= PI2 ;
				else
					remdr += PI2;
			}

			return remdr;
		}


		// =======================================================================================================

		bool Compare(const Vector& vc1, const Vector& vc2, MA_REAL tol)
		{
			if (Compare(vc1.X, vc2.X, tol))
				if (Compare(vc1.Y, vc2.Y, tol))
					if (Compare(vc1.Z, vc2.Z, tol))
						return true;

			return false;
		}

		Vector operator * (const Vector& vc, const Matrix& mt)
		{
			return Vector(
				mt.VLS[0][0] * vc.X + mt.VLS[1][0] * vc.Y + mt.VLS[2][0] * vc.Z + mt.VLS[3][0] * vc.W,
				mt.VLS[0][1] * vc.X + mt.VLS[1][1] * vc.Y + mt.VLS[2][1] * vc.Z + mt.VLS[3][1] * vc.W,
				mt.VLS[0][2] * vc.X + mt.VLS[1][2] * vc.Y + mt.VLS[2][2] * vc.Z + mt.VLS[3][2] * vc.W,
				mt.VLS[0][3] * vc.X + mt.VLS[1][3] * vc.Y + mt.VLS[2][3] * vc.Z + mt.VLS[3][3] * vc.W);
		}

		Vector& operator *= (Vector& vc, const Matrix& mt)
		{
			return vc = vc * mt;
		}

		Vector TransformNormal(const Vector& vc, const Matrix& mt)
		{
			return Vector(
				mt.VLS[0][0] * vc.X + mt.VLS[1][0] * vc.Y + mt.VLS[2][0] * vc.Z,
				mt.VLS[0][1] * vc.X + mt.VLS[1][1] * vc.Y + mt.VLS[2][1] * vc.Z,
				mt.VLS[0][2] * vc.X + mt.VLS[1][2] * vc.Y + mt.VLS[2][2] * vc.Z);
		}

		MA_REAL Dot(const Vector& vc1, const Vector& vc2)
		{
			return vc1.X*vc2.X + vc1.Y*vc2.Y + vc1.Z*vc2.Z;
		}

		Vector Cross(const Vector& vc1, const Vector& vc2)
		{
			return Vector(vc1.Y*vc2.Z - vc1.Z*vc2.Y, vc1.Z*vc2.X - vc1.X*vc2.Z, vc1.X*vc2.Y - vc1.Y*vc2.X);
		}

		bool IsNParallel(const Vector& vc1, const Vector& vc2, MA_REAL tol)
		{
			assert(Compare(vc1.Length(), 1, tol));
			assert(Compare(vc2.Length(), 1, tol));

			if (Compare(vc1, vc2, tol))
				return true;

			if (Compare(vc1.Revers(), vc2, tol))
				return true;

			return false;
		}

		bool IsParallel(const Vector& vc1, const Vector& vc2, MA_REAL tol)
		{
			const MA_REAL dot = Dot(vc1, vc2);
			return Compare(dot * dot, vc1.LenghtSquare() * vc2.LenghtSquare(), tol);
		}

		bool IsNCodir(const Vector& vc1, const Vector& vc2, MA_REAL tol)
		{
			assert(Compare(vc1.Length(), 1, tol));
			assert(Compare(vc2.Length(), 1, tol));

			return Compare(vc1, vc2, tol);
		}

		bool IsCodir(const Vector& vc1, const Vector& vc2, MA_REAL tol)
		{
			const MA_REAL dot = Dot(vc1, vc2);
			if (dot < 0)
				return false;

			return Compare(dot * dot, vc1.LenghtSquare() * vc2.LenghtSquare(), tol);
		}

		bool IsNNormals(const Vector& vc1, const Vector& vc2, MA_REAL tol)
		{
			assert(Compare(vc1.Length(), 1, tol));
			assert(Compare(vc1.Length(), 1, tol));

			return Compare(Dot(vc1, vc2), 0, tol);
		}

		MA_REAL AngleBetween(Vector vc1, Vector vc2, MA_REAL tol)
		{
			if (!vc1.Normalize(tol) || !vc2.Normalize(tol))
				return 0;

			return AngleBetweenNormals(vc1, vc2, tol);
		}

		MA_REAL AngleBetweenNormals(const Vector& vc1, const Vector& vc2, MA_REAL tol)
		{
// 			assert(Compare(vc1.Length(), 1, tol));
// 			assert(Compare(vc2.Length(), 1, tol));

			if (!Compare(vc1.Length(), 1, tol))
			{
				double f = 4 + 5;
			}

			if (!Compare(vc2.Length(), 1, tol))
			{
				double f = 4 + 5;
			}


			const MA_REAL cos = Dot(vc1, vc2);

			if (cos > 1.0) return 0;
			if (cos < -1.0) return PI;

		#ifdef DOUBLE_PRECISION

			if (fabs(cos) > 0.9) // sinus calculating, else losing of precision
			{
				// S ���. = a * b * sin(a ^ b) == cross.Lenght();
				Vector cross = Cross(vc1, vc2);
				MA_REAL rs = asin(cross.Length());

				if (cos < 0) // ���� �����
					rs = PI - rs;

				return rs;
			}
		#endif

			return acos(cos);
		}

		MA_REAL DistanceBetween(const Vector& vc1, const Vector& vc2)	{ return (vc1 - vc2).Length(); }

		MA_REAL ProjectionVectorTo(const Vector& vc1, const Vector& vc2)
		{
			MA_REAL dot = Dot(vc1, vc2);

			if (Compare(dot, 0))
				return 0;

			return dot / vc2.Length();
		}

		MECHA_API MA_REAL AngleToProjection(Vector vc1, Vector vc2, Vector norm)
		{
			if (!vc1.Normalize())
				return 0;
			if (!vc2.Normalize())
				return 0;
			if (!norm.Normalize())
				return 0;

			if (IsNParallel(vc1, norm) || IsNParallel(vc2, norm))
				return 0;

			Vector c1 = Cross(vc1, norm);
			Vector c2 = Cross(vc2, norm);

			MA_REAL ang = AngleBetween(c1, c2);
			if (Compare(ang, 0) || Compare(ang, PI))
				return ang;

			Vector cN = Cross(c2, c1);
			cN.Normalize();

			if (!Compare(norm, cN))
				ang *= -1;

			return ang;
		}

		Vector Min(const Vector& vc1, const Vector& vc2)
		{
			return Vector(Min(vc1.X, vc2.X), Min(vc1.Y, vc2.Y), Min(vc1.Z, vc2.Z));
		}

		Vector Max(const Vector& vc1, const Vector& vc2)
		{
			return Vector(Max(vc1.X, vc2.X), Max(vc1.Y, vc2.Y), Max(vc1.Z, vc2.Z));
		}

		// ============================================================================================================

		bool Compare(const Matrix& mtr1, const Matrix& mtr2, MA_REAL tol)
		{
			for (UINT i = 0; i < 4; i++)
				for (UINT j = 0; j < 4; j++)
					if (!Compare(mtr1.VLS[i][j], mtr2.VLS[i][j], tol))
						return false;

			return true;
		}


		Matrix LCSToWCS(const Vector& pos, const Vector& xAxis, const Vector& yAxis, const Vector& zAxis)
		{
			return Matrix(
				xAxis.X, xAxis.Y, xAxis.Z, 0,
				yAxis.X, yAxis.Y, yAxis.Z, 0,
				zAxis.X, zAxis.Y, zAxis.Z, 0,
				pos.X, pos.Y, pos.Z, 1
				);
		}

		Matrix WCSToLCS(const Vector& pos, const Vector& xAxis, const Vector& yAxis, const Vector& zAxis)
		{
			return Matrix(
				xAxis.X, yAxis.X, zAxis.X, 0,
				xAxis.Y, yAxis.Y, zAxis.Y, 0,
				xAxis.Z, yAxis.Z, zAxis.Z, 0,
				-Dot(xAxis, pos), -Dot(yAxis, pos), -Dot(zAxis, pos), 1
				);
		}

		Matrix InverseDirectionCosMatrix(const Matrix& mtr)
		{
			const Vector pos(mtr.VLS[3][0], mtr.VLS[3][1], mtr.VLS[3][2]);

			return Matrix(
				mtr.VLS[0][0], mtr.VLS[1][0], mtr.VLS[2][0], 0,
				mtr.VLS[0][1], mtr.VLS[1][1], mtr.VLS[2][1], 0,
				mtr.VLS[0][2], mtr.VLS[1][2], mtr.VLS[2][2], 0,
				-Dot(Vector(mtr.VLS[0][0], mtr.VLS[0][1], mtr.VLS[0][2]), pos),
				-Dot(Vector(mtr.VLS[1][0], mtr.VLS[1][1], mtr.VLS[1][2]), pos),
				-Dot(Vector(mtr.VLS[2][0], mtr.VLS[2][1], mtr.VLS[2][2]), pos),
				1
				);
		}

		Matrix MatrixTranslation(Vector vc)
		{
			Matrix res;
			res.VLS[3][0] = vc.X;
			res.VLS[3][1] = vc.Y;
			res.VLS[3][2] = vc.Z;
			return res;
		}


		Matrix MatrixRotationX(MA_REAL angle)
		{
			Matrix res;
			res.VLS[1][1] = cos(angle);
			res.VLS[2][2] = cos(angle);
			res.VLS[2][1] = -sin(angle);
			res.VLS[1][2] = sin(angle);
			return res;
		}

		Matrix MatrixRotationY(MA_REAL angle)
		{
			Matrix res;
			res.VLS[0][0] = cos(angle);
			res.VLS[2][2] = cos(angle);
			res.VLS[2][0] = sin(angle);
			res.VLS[0][2] = -sin(angle);
			return res;
		}

		Matrix MatrixRotationZ(MA_REAL angle)
		{
			Matrix res;
			res.VLS[0][0] = cos(angle);
			res.VLS[1][1] = cos(angle);
			res.VLS[1][0] = -sin(angle);
			res.VLS[0][1] = sin(angle);
			return res;
		}

		Matrix MatrixRotationAxis(const Vector& v, MA_REAL ang)
		{
			Matrix res;
			res.VLS[0][0] = (1 - cos(ang)) * v.X * v.X + cos(ang);
			res.VLS[1][0] = (1 - cos(ang)) * v.X * v.Y - sin(ang) * v.Z;
			res.VLS[2][0] = (1 - cos(ang)) * v.X * v.Z + sin(ang) * v.Y;
			res.VLS[0][1] = (1 - cos(ang)) * v.Y * v.X + sin(ang) * v.Z;
			res.VLS[1][1] = (1 - cos(ang)) * v.Y * v.Y + cos(ang);
			res.VLS[2][1] = (1 - cos(ang)) * v.Y * v.Z - sin(ang) * v.X;
			res.VLS[0][2] = (1 - cos(ang)) * v.Z * v.X - sin(ang) * v.Y;
			res.VLS[1][2] = (1 - cos(ang)) * v.Z * v.Y + sin(ang) * v.X;
			res.VLS[2][2] = (1 - cos(ang)) * v.Z * v.Z + cos(ang);
			return res;
		}

		Matrix TransformInPoint(const Matrix& mtr1, const Matrix& mtr2)
		{
			Matrix resMtr = mtr1;

			for (UINT i = 0; i < 3; i++)
			{
				for (UINT j = 0; j < 3; j++)
				{
					MA_REAL summ = 0;
					for (UINT k = 0; k < 3; k++)
						summ += mtr1.VLS[i][k] * mtr2.VLS[k][j];

					resMtr.VLS[i][j] = summ;
				}
			}

			return resMtr;
		}

		Matrix MatrixRotationYawPitchRoll(const EulerAngles& eulerAngles)
		{
/*			const Matrix& rotZ = MatrixRotationZ(roll);
			const Matrix& rotX = MatrixRotationX(pitch);
			const Matrix& rotY = MatrixRotationY(yaw);

			return rotZ * rotX * rotY;*/

			MA_REAL cos_yaw = cos(eulerAngles.m_yaw);
			MA_REAL sin_yaw = sin(eulerAngles.m_yaw);
			MA_REAL cos_pitch = cos(eulerAngles.m_pitch);
			MA_REAL sin_pitch = sin(eulerAngles.m_pitch);
			MA_REAL cos_roll = cos(eulerAngles.m_roll);
			MA_REAL sin_roll = sin(eulerAngles.m_roll);

			return Matrix(
				cos_roll*cos_yaw + sin_roll*sin_pitch*sin_yaw, sin_roll*cos_pitch, sin_roll*sin_pitch*cos_yaw - cos_roll*sin_yaw, 0,
				cos_roll*sin_pitch*sin_yaw - sin_roll*cos_yaw, cos_roll*cos_pitch, sin_roll*sin_yaw + cos_roll*sin_pitch*cos_yaw, 0,
				cos_pitch*sin_yaw, -sin_pitch, cos_pitch*cos_yaw, 0,
				0, 0, 0, 1);
		}

		void YawPitchRollFromMatrix(const Matrix& mtr, OUT EulerAngles& eulerAngles)
		{
			MA_REAL vl = -mtr.VLS[2][1];
			
			if (vl < -1)
				vl = MA_REAL(-1);
			else if (vl > 1) 
				vl = MA_REAL(1);

			eulerAngles.m_pitch = asin(vl);

			if (Compare(fabs(vl), 1, MA_REAL(1e-5)))
			{
				eulerAngles.m_yaw = -atan2(mtr.VLS[0][2], mtr.VLS[0][1]);
				eulerAngles.m_roll = 0;
			}
			else
			{
				eulerAngles.m_yaw = atan2(mtr.VLS[2][0], mtr.VLS[2][2]);
				eulerAngles.m_roll = atan2(mtr.VLS[0][1], mtr.VLS[1][1]);
			}
		}

		Matrix MatrixRotationYawPitchRollInverse(const EulerAngles& eulerAngles)
		{
			MA_REAL cos_yaw = cos(eulerAngles.m_yaw);
			MA_REAL sin_yaw = sin(eulerAngles.m_yaw);
			MA_REAL cos_pitch = cos(eulerAngles.m_pitch);
			MA_REAL sin_pitch = sin(eulerAngles.m_pitch);
			MA_REAL cos_roll = cos(eulerAngles.m_roll);
			MA_REAL sin_roll = sin(eulerAngles.m_roll);

			return Matrix(
				cos_roll*cos_yaw + sin_roll*sin_pitch*sin_yaw, cos_roll*sin_pitch*sin_yaw - sin_roll*cos_yaw, cos_pitch*sin_yaw, 0,
				sin_roll*cos_pitch, cos_roll*cos_pitch, -sin_pitch, 0,
				sin_roll*sin_pitch*cos_yaw - cos_roll*sin_yaw, sin_roll*sin_yaw + cos_roll*sin_pitch*cos_yaw, cos_pitch*cos_yaw, 0,
				0, 0, 0, 1);
		}

		Matrix MtrA_In_MtrB(const Matrix& mtrA, const Matrix& mtrB)
		{
			const Matrix& toMtrB = InverseDirectionCosMatrix(mtrB);
			return mtrA * toMtrB;
		}

		// ============================================================================================================
		MA_REAL QuaternionDot(const Quaternion& q1, const Quaternion& q2)
		{
			// copy from D3DXQuaternionDot
			return (q1.m_X) * (q2.m_X) + (q1.m_Y) * (q2.m_Y) + (q1.m_Z) * (q2.m_Z) + (q1.m_W) * (q2.m_W);
		}

		Quaternion QuaternionSlerp(const Quaternion& q1, const Quaternion& q2, MA_REAL t)
		{
			// copy from D3DXQuaternionSlerp
			MA_REAL dot, epsilon;			
			epsilon = 1;
			dot = QuaternionDot(q1, q2);

			if (dot < 0) 
				epsilon = -1;

			Quaternion res;

			res.m_X = (1 - t) * q1.m_X + epsilon * t * q2.m_X;
			res.m_Y = (1 - t) * q1.m_Y + epsilon * t * q2.m_Y;
			res.m_Z = (1 - t) * q1.m_Z + epsilon * t * q2.m_Z;
			res.m_W = (1 - t) * q1.m_W + epsilon * t * q2.m_W;

			res.Normalize();
			return res;
		}

		bool MatrixToQuaternion(const Matrix& mt, OUT Quaternion& res)
		{
			// copy from D3DXQuaternionRotationMatrix

			MA_REAL s;
			MA_REAL trace = mt[0][0] + mt[1][1] + mt[2][2] + 1;
			if (trace > 1)
			{
				s = 2 * sqrt(trace);
				res.m_X = (mt[1][2] - mt[2][1]) / s;
				res.m_Y = (mt[2][0] - mt[0][2]) / s;
				res.m_Z = (mt[0][1] - mt[1][0]) / s;
				res.m_W = MA_REAL(0.25) * s;
			}
			else
			{
				int maxi = 0;
				for (int i = 1; i < 3; i++)
				if (mt[i][i] > mt[maxi][maxi])
					maxi = i;

				switch (maxi)
				{
				case 0:
					{
						s = 2 * sqrt(1 + mt[0][0] - mt[1][1] - mt[2][2]);
						res.m_X = MA_REAL(0.25) * s;
						res.m_Y = (mt[0][1] + mt[1][0]) / s;
						res.m_Z = (mt[0][2] + mt[2][0]) / s;
						res.m_W = (mt[1][2] - mt[2][1]) / s;
					}
					break;
				case 1:
					{
						s = 2 * sqrt(1 + mt[1][1] - mt[0][0] - mt[2][2]);
						res.m_X = (mt[0][1] + mt[1][0]) / s;
						res.m_Y = MA_REAL(0.25) * s;
						res.m_Z = (mt[1][2] + mt[2][1]) / s;
						res.m_W = (mt[2][0] - mt[0][2]) / s;
					}
					break;
				case 2:
					{
						s = 2 * sqrt(1 + mt[2][2] - mt[0][0] - mt[1][1]);
						res.m_X = (mt[0][2] + mt[2][0]) / s;
						res.m_Y = (mt[1][2] + mt[2][1]) / s;
						res.m_Z = MA_REAL(0.25) * s;
						res.m_W = (mt[0][1] - mt[1][0]) / s;
					}
					break;
				}
			}

			return res.Normalize();
		}

		Matrix QuaternionToMatrix(const Quaternion& qt)
		{
			// copy from D3DXMatrixRotationQuaternion

			Matrix res;			

			res[0][0] = 1 - 2 * (qt.m_Y * qt.m_Y + qt.m_Z * qt.m_Z);
			res[0][1] = 2 * (qt.m_X * qt.m_Y + qt.m_Z * qt.m_W);
			res[0][2] = 2 * (qt.m_X * qt.m_Z - qt.m_Y * qt.m_W);
			res[1][0] = 2 * (qt.m_X * qt.m_Y - qt.m_Z * qt.m_W);
			res[1][1] = 1 - 2 * (qt.m_X * qt.m_X + qt.m_Z * qt.m_Z);
			res[1][2] = 2 * (qt.m_Y * qt.m_Z + qt.m_X * qt.m_W);
			res[2][0] = 2 * (qt.m_X * qt.m_Z + qt.m_Y * qt.m_W);
			res[2][1] = 2 * (qt.m_Y * qt.m_Z - qt.m_X * qt.m_W);
			res[2][2] = 1 - 2 * (qt.m_X * qt.m_X + qt.m_Y * qt.m_Y);

			return res;
		}

		void RotateByQt(Quaternion qt, IN OUT Vector& vc)
		{
			bool isNormalize = qt.Normalize();
			assert(isNormalize);

			RotateByQtNorm(qt, vc);
		}

		void RotateByQtNorm(const Quaternion& qt, IN OUT Vector& vc)
		{
			Quaternion conjQt = qt.GetConjugate();
			Quaternion rotCt(0, vc);

			Quaternion res = qt*rotCt*conjQt;
			vc = Vector(res.m_X, res.m_Y, res.m_Z);
		}
	}
}