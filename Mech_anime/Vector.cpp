#include "stdafx.h"
#include "Vector.h"
#include "MathFunctions.h"


using namespace std;


const mech_a::math::Vector NaN_VK(
	numeric_limits<MA_REAL>::quiet_NaN(),
	numeric_limits<MA_REAL>::quiet_NaN(),
	numeric_limits<MA_REAL>::quiet_NaN(),
	numeric_limits<MA_REAL>::quiet_NaN()
	);

const mech_a::math::Vector OX_VK(1, 0, 0);
const mech_a::math::Vector OY_VK(0, 1, 0);
const mech_a::math::Vector OZ_VK(0, 0, 1);
const mech_a::math::Vector Null_VK(0, 0, 0);


namespace mech_a {

	namespace math {

		// --------------------------------------------------------------
		// ----------------------- operators ----------------------------
		// -------------------------------------------------------------

		Vector Vector::operator + (const Vector& vc) const
		{
			return Vector(X + vc.X, Y + vc.Y, Z + vc.Z);
		}

		Vector Vector::operator - (const Vector& vc) const
		{
			return Vector(X - vc.X, Y - vc.Y, Z - vc.Z);
		}

		Vector Vector::operator * (MA_REAL vl) const
		{
			return Vector(X * vl, Y * vl, Z * vl);
		}

		Vector Vector::operator / (MA_REAL vl) const
		{
			return Vector(X / vl, Y / vl, Z / vl);
		}

		Vector& Vector::operator += (const Vector& vc)
		{
			X += vc.X; Y += vc.Y; Z += vc.Z; return *this;
		}

		Vector& Vector::operator -= (const Vector& vc)
		{
			X -= vc.X; Y -= vc.Y; Z -= vc.Z; return *this;
		}

		Vector& Vector::operator *= (MA_REAL vl)
		{
			X *= vl; Y *= vl; Z *= vl; return *this;
		}

		Vector& Vector::operator /= (MA_REAL vl)
		{
			X /= vl; Y /= vl; Z /= vl; return *this;
		}

		Vector operator * (const MA_REAL& vl, const Vector vc)
		{
			return vc * vl;
		}

		// --------------------------------------------------------------
		// ----------------------- methods ----------------------------
		// -------------------------------------------------------------

		Vector Vector::OrtX() const { return Vector(X, 0, 0); }
		Vector Vector::OrtY() const { return Vector(0, Y, 0); }
		Vector Vector::OrtZ() const { return Vector(0, 0, Z); }

		Vector Vector::Revers() const { return Vector(-X, -Y, -Z); }

		MA_REAL Vector::Length() const { return sqrt(X*X + Y*Y + Z*Z); }

		MA_REAL Vector::LenghtSquare() const { return X*X + Y*Y + Z*Z; }

		bool Vector::Normalize(MA_REAL tol)
		{
			MA_REAL ln = X*X + Y*Y + Z*Z;
			if (Compare(ln, 0, tol))
				return false;

			ln = sqrt(ln);

			X /= ln; Y /= ln; Z /= ln;
			return true;
		}

		Vector Vector::NormalWith(MA_REAL tol) const
		{
			Vector n = *this;
			if (!n.Normalize(tol))
				return Vector();

			Vector res;
			MA_REAL max = 0.015625;
			if (fabs(n.X) < max && fabs(n.Y) < max)
			{
				res.X = n.Z;
				res.Y = 0;
				res.Z = -1 * n.X;
			}
			else
			{
				res.X = -1 * n.Y;
				res.Y = n.X;
				res.Z = 0.0;
			}

			res.Normalize();
			return res;
		}

		bool Vector::IsNormal(MA_REAL tol) const
		{
			return Compare(X*X + Y*Y + Z*Z, 1, tol);
		}

		// statics
		const Vector& Vector::NaN() { return NaN_VK; }

		const Vector& Vector::Null() { return Null_VK; }

		const Vector& Vector::OX() { return OX_VK; }

		const Vector& Vector::OY() { return OY_VK; }

		const Vector& Vector::OZ() { return OZ_VK; }
	}
}