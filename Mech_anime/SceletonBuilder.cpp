#pragma once
#include "stdafx.h"
#include "SceletonBuilder.h"


namespace mech_a {
	namespace sceleton {

		SceletonBuilder::SceletonBuilder(const SceletonData& sceletonData, ISceletonBuilderListener* pListener)
			: m_SceletonData(sceletonData)
			, m_pListener(pListener)
		{
			LoadParts(m_SceletonData.m_Elements, m_SceletonData.m_Connections, m_SceletonData.m_BeginState, m_Parts);

			UINT maxId = 0;
			
			for (const auto& itPart : m_Parts)
				if (maxId < itPart.first)
					maxId = itPart.first;

			m_ID_Generator = ID_Genearator(++maxId);
		}

		std::unordered_set<IPartSceleton*> SceletonBuilder::GetParts() const
		{
			std::unordered_set<IPartSceleton*> resPart;

			for (const auto& it : m_Parts)
				resPart.insert(it.second.get());

			return resPart;
		}

		std::unordered_map<UINT, IPartSceleton*> SceletonBuilder::GetPartsMap() const
		{
			std::unordered_map<UINT, IPartSceleton*> resParts;

			for (const auto& it : m_Parts)
				resParts.emplace(it.first, it.second.get());

			return resParts;
		}

		bool SceletonBuilder::CreateConnection(const SC& sc, const std::wstring& name, OUT Connection** ppNewConnect, Element* pToElement)
		{
			if (!ppNewConnect)
				return false;

			UINT idNewConnect = m_ID_Generator.GetID();

			SceletonData::ConnectionData newConnect(idNewConnect, name);
			if (pToElement)
				newConnect.m_ElementA = pToElement->ID();

			// set global transform 
			auto itConn = m_SceletonData.m_Connections.emplace(idNewConnect, newConnect).first;
			auto itGlobalSC = m_SceletonData.m_BeginState.m_GlobalPoseElements.emplace(idNewConnect, sc).first;

			// set local transform
			SceletonData::TypeLocalCS lcs;

			if (pToElement)
				lcs = SceletonData::TypeLocalCS::Default();
			else
				FromCS_ToCS(sc, lcs);

			std::unordered_map<ULONG, SceletonData::TypeLocalCS>::iterator itLocalSC =
				m_SceletonData.m_BeginState.m_LocalPoseElements.emplace(idNewConnect, lcs).first;

			// create wrapper new connect
			*ppNewConnect = new Connection(itConn, pToElement, NULL, itGlobalSC, itLocalSC);
			m_Parts.emplace(idNewConnect, std::unique_ptr<IPartSceleton>(*ppNewConnect));

			// set basic lcs
			if (pToElement)
			{
				SceletonData::TypeGlobalCS basicLSC;
				GetLocalTransform(pToElement->GetParentConnect()->GetWCS(), sc, basicLSC);
				(*ppNewConnect)->setBasicLCS(basicLSC);
			}
			else
			{
				(*ppNewConnect)->setBasicLCS(SceletonData::TypeGlobalCS::Default());
			}

			// modify pTo Element
			if (pToElement)
				pToElement->addChildConnect(*ppNewConnect);

			// notify Listener
			if (m_pListener)
				m_pListener->OnCreatePart(*ppNewConnect);

			return true;
		}

		mech_a::origin::SC rotateByAxis(const mech_a::origin::SC& fromSC, const Vector& dirTransform, MA_REAL angleTransform)
		{
			assert(dirTransform.IsNormal());
			return TransformInPoint(fromSC.Tr_Matrix(), MatrixRotationAxis(dirTransform, angleTransform));
		}

		mech_a::origin::QuaternionCS rotateByAxis(const mech_a::origin::QuaternionCS& fromSC, const Vector& dirTransform, MA_REAL angleTransform)
		{
			assert(dirTransform.IsNormal());

			Quaternion qtRotate;
			qtRotate.SetAxisAngl(angleTransform, dirTransform);

			Quaternion resQt = qtRotate * fromSC.m_Quaternion;
			resQt.Normalize();
			
			return mech_a::origin::QuaternionCS { resQt, fromSC.m_Pos };
		}

		bool SceletonBuilder::CreateElement(Connection* pConnectA, Connection* pConnectB, const std::wstring& name, OUT Element** ppNewElement)
		{
			// validation
			if (!pConnectA || !pConnectB || !ppNewElement)
				return false;

			if (pConnectA->m_pElementB != 0)
				return false;

			if (pConnectB->m_pElementA != 0)
				return false;

			// forward dirZ connection A to direction of Connection B
			Vector newDirZ = pConnectB->GetWCS().Pos() - pConnectA->GetWCS().Pos();
			MA_REAL lengh = newDirZ.Length();

			if (lengh < 0 || Compare(lengh, 0) || !newDirZ.Normalize())
				return false;

			const Vector& oldDirZ = pConnectA->GetWCS().DirZ();
			
			if (!IsNCodir(newDirZ, oldDirZ))
			{
				MA_REAL anglBwNormals = AngleBetweenNormals(newDirZ, oldDirZ);
				Vector normalRotate;

				if (Compare(anglBwNormals, PI))
					normalRotate = pConnectA->GetWCS().DirY();
				else
				{
					normalRotate = Cross(oldDirZ, newDirZ);
					normalRotate.Normalize();
				}

				pConnectA->updateWCS_onlyConnect(rotateByAxis(pConnectA->GetWCS(), normalRotate, anglBwNormals));
			}

			// calc basic locsl sc 
			SceletonData::TypeGlobalCS basic_LSC;
			GetLocalTransform(*pConnectA->m_pWCS, *pConnectB->m_pWCS, basic_LSC);
			pConnectB->setLCS(SceletonData::TypeLocalCS::Default());
			pConnectB->setBasicLCS(basic_LSC);

			// create new element
			ULONG idNewElement = m_ID_Generator.GetID();

			ElementData newElement(idNewElement, name);
			newElement.m_ParentConnectId = pConnectA->ID();
			newElement.m_Lengh = lengh;
			auto itNewElement = m_SceletonData.m_Elements.emplace(idNewElement, newElement).first;

			// create wrapper
			*ppNewElement = new Element(itNewElement, pConnectA);
			m_Parts.emplace(idNewElement, std::unique_ptr<IPartSceleton>(*ppNewElement));

			// modify parts
			(*ppNewElement)->addChildConnect(pConnectB);
			pConnectA->setElementB(*ppNewElement);
			pConnectB->setElementA(*ppNewElement);

			// notify Listener
			if (m_pListener)
				m_pListener->OnCreatePart(*ppNewElement);

			return true;
		}

		bool SceletonBuilder::CreateElement(Connection* pConnectA, MA_REAL lenght, const std::wstring& name, OUT Element** ppNewElement)
		{
			// validation
			if (!pConnectA || !ppNewElement)
				return false;

			if (pConnectA->m_pElementB != 0)
				return false;

			if (lenght < 0 || Compare(lenght, 0))
				return false;

			// create new element
			ULONG idNewElement = m_ID_Generator.GetID();

			ElementData newElement(idNewElement, name);
			newElement.m_ParentConnectId = pConnectA->ID();
			newElement.m_Lengh = lenght;
			auto itNewElement = m_SceletonData.m_Elements.emplace(idNewElement, newElement).first;

			// create wrapper
			*ppNewElement = new Element(itNewElement, pConnectA);
			m_Parts.emplace(idNewElement, std::unique_ptr<IPartSceleton>(*ppNewElement));

			// modify parts
			pConnectA->setElementB(*ppNewElement);

			// notify Listener
			if (m_pListener)
				m_pListener->OnCreatePart(*ppNewElement);

			return true;
		}

		bool SceletonBuilder::RemoveSceletonPart(IPartSceleton* pRemovePart)
		{
			if (!pRemovePart)
				return false;

			if (m_pListener)
				m_pListener->OnRemovePart(pRemovePart);

			if (!m_ID_Generator.RemoveID(pRemovePart->ID()))
				return false;

			if (removeElement(dynamic_cast<Element*>(pRemovePart)))
				return true;
		
			return removeConnection(dynamic_cast<Connection*>(pRemovePart));
		}

		bool SceletonBuilder::removeElement(Element* pElement)
		{
			if (!pElement)
				return false;

			Connection* pParent = pElement->GetParentConnect();
			pParent->setElementB(NULL);

			const std::unordered_set<Connection*>& childConnects = pElement->GetChildConnects();
			for (Connection* pConn : childConnects)
				pConn->setElementA(NULL);

			const ULONG eraseID = pElement->ID();

			m_Parts.erase(eraseID);
			m_SceletonData.m_Elements.erase(eraseID);
			
			return true;
		}

		bool SceletonBuilder::removeConnection(Connection* pConnection)
		{
			if (!pConnection)
				return false;

			if (Element* pElement = pConnection->GetElementA())
				pElement->removeChildConnect(pConnection);

			Element* pElement = pConnection->GetElementB();
			if (pElement)
			{
				if (m_pListener)
					m_pListener->OnRemovePart(pElement);

				m_ID_Generator.RemoveID(pElement->ID());
				removeElement(pElement);
			}

			const ULONG eraseID = pConnection->ID();

			// erase in wrapper
			m_Parts.erase(eraseID);

			// erase in sceleton data
			m_SceletonData.m_Connections.erase(eraseID);
			m_SceletonData.m_BeginState.m_GlobalPoseElements.erase(eraseID);
			m_SceletonData.m_BeginState.m_LocalPoseElements.erase(eraseID);

			// eraase in motions
			removeMotionData(eraseID);
			return true;
		}

		void SceletonBuilder::removeMotionData(ULONG eraseID)
		{
			for (auto& itMotion : m_SceletonData.m_Motions)
			{
				RemoveInterpolationData(eraseID, itMotion.second.m_InterpolationData);

				for (auto& itState : itMotion.second.m_States)
				{
					itState.second.m_GlobalPoseElements.erase(eraseID);
					itState.second.m_LocalPoseElements.erase(eraseID);
				}
			}

			for (auto& itState : m_SceletonData.m_SpecialStates)
			{
				itState.second.m_GlobalPoseElements.erase(eraseID);
				itState.second.m_LocalPoseElements.erase(eraseID);
			}
		}

		void SceletonBuilder::SetListener(ISceletonBuilderListener* pListener)
		{
			m_pListener = pListener;
		}
	}
}