#include "stdafx.h"
#include "CubicSpline.h"
#include "MathFunctions.h"


namespace mech_a {

	namespace math {

		MA_REAL CubicSpline::Calculate(MA_REAL x) const
		{
			const MA_REAL H = x - m_Xk;
			return m_A + (m_B * H) + (m_C * H * H) + (m_D * H * H * H);
		}


// 		std::pair<MA_REAL, MA_REAL> CubicSpline::MaxValue(MA_REAL x1, MA_REAL x2) const  error implementation
// 		{
// 			if (x2 < x1)
// 				throw std::runtime_error("x2 < x1 ???!!!");
// 
// 			if (x1 == x2)
// 				return std::pair<MA_REAL, MA_REAL>(x1, Calculate(x1));
// 
// 			const MA_REAL firstVl = Calculate(x1);
// 			const MA_REAL secondVl = Calculate(x2);
// 
// 			std::pair<MA_REAL, MA_REAL> maxPair;	
// 			
// 			if (firstVl > secondVl)
// 			{
// 				maxPair.first = x1;
// 				maxPair.second = firstVl;	
// 			}
// 			else
// 			{
// 				maxPair.first = x2;
// 				maxPair.second = secondVl;
// 			}
// 
// 			const MA_REAL D_2 = (4 * m_C * m_C) - (12 * m_D * m_B);
// 
// 			if (D_2 < 0)
// 				return maxPair;
// 
// 			if (D_2 == 0)
// 			{
// 				const MA_REAL h_eks = (-m_C) / (3 * m_D);
// 				const MA_REAL x_eks = h_eks + m_Xk;
// 				const MA_REAL thirdVl = Calculate(x_eks);
// 
// 				if (thirdVl < maxPair.second)
// 					return maxPair;
// 
// 				return std::pair<MA_REAL, MA_REAL>(x_eks, thirdVl);
// 			}
// 
// 			const MA_REAL D = sqrt(D_2);
// 			
// 			MA_REAL h_eks = ((-m_C * 2) + D) / (6 * m_D);
// 			MA_REAL x_eks = h_eks + m_Xk;
// 			MA_REAL x_vl = Calculate(x_eks);
// 
// 			if (x_vl > maxPair.second)
// 				maxPair = std::pair<MA_REAL, MA_REAL>(x_eks, x_vl);
// 
// 			h_eks = ((-m_C * 2) - D) / (6 * m_D);
// 			x_eks = h_eks + m_Xk;
// 			x_vl = Calculate(x_eks);
// 
// 			if (x_vl > maxPair.second)
// 				return std::pair<MA_REAL, MA_REAL>(x_eks, x_vl);
// 
// 			return maxPair;
// 		}

		MA_REAL CubicSpline::CalculateDerivative(MA_REAL x) const
		{
			return (3 * m_D * x * x) - (6 * m_D * m_Xk * x) + (3 * m_D * m_Xk * m_Xk) + (m_B) + (2 * m_C * x) - (2 * m_C * m_Xk);
		}

		std::pair<MA_REAL, MA_REAL> CubicSpline::MaxDerivative(MA_REAL x1, MA_REAL x2) const // x, y
		{
			if (x2 < x1)
				throw std::runtime_error("x2 < x1 ???!!!");

			if (x1 == x2)
				return std::pair<MA_REAL, MA_REAL>(x1, CalculateDerivative(x1));

			if (mech_a::math::Compare(m_D, 0))
			{
				if (mech_a::math::Compare(m_C, 0))
					return std::pair<MA_REAL, MA_REAL>(x1, CalculateDerivative(x1));

				MA_REAL drvtX1 = CalculateDerivative(x1);
				MA_REAL drvtX2 = CalculateDerivative(x2);

				if (fabs(drvtX1) > fabs(drvtX2))
					return std::pair<MA_REAL, MA_REAL>(x1, drvtX1);

				return std::pair<MA_REAL, MA_REAL>(x2, drvtX2);
			}

			std::pair<MA_REAL, MA_REAL> res;

			MA_REAL drvtX1 = CalculateDerivative(x1);
			MA_REAL drvtX2 = CalculateDerivative(x2);

			if (fabs(drvtX1) > fabs(drvtX2))
				res = std::pair<MA_REAL, MA_REAL>(x1, drvtX1);
			else
				res = std::pair<MA_REAL, MA_REAL>(x2, drvtX2);

			MA_REAL x_Eks = (3 * m_D * m_Xk - m_C) / (3 * m_D);
			if (x_Eks <= x1 || x_Eks >= x2)
				return res;
			
			MA_REAL drvtEks = CalculateDerivative(x_Eks);

			if (fabs(drvtEks) > fabs(res.second))
				return std::pair<MA_REAL, MA_REAL>(x_Eks, drvtEks);

			return res;
		}

		vector<CubicSpline> CubicSplineInterpolation(const map<MA_REAL, MA_REAL>& values)
		{
			const size_t szNodes = values.size();
			
			assert(szNodes > 2);

			// ���������� ����������� ���������� � ����������� ��������� 
			const size_t szSplines = szNodes - 1;
			vector<MA_REAL> dNodes, splitDiffs;
			dNodes.reserve(szSplines);
			splitDiffs.reserve(szSplines);

			auto it = values.begin();
			MA_REAL preNode = it->first;
			MA_REAL preFunc = it->second;

			while (++it != values.end())
			{
				const MA_REAL dNode = it->first - preNode;
				dNodes.push_back(dNode);
				preNode = it->first;

				splitDiffs.push_back((it->second - preFunc) / dNode);
				preFunc = it->second;
			}

			// ���������� ����������� �������������
			const size_t szKoeffs = szNodes - 2;
			vector<MA_REAL> sigmas, lambdas;
			sigmas.reserve(szKoeffs);
			lambdas.reserve(szKoeffs);

			sigmas.push_back(
				-dNodes[1]
					/ (2 * (dNodes[0] + dNodes[1]))
			);

			lambdas.push_back(
				(3 * (splitDiffs[1] - splitDiffs[0])) 
					/ (2 * (dNodes[0] + dNodes[1]))
			);

			for (size_t i = 1; i < szKoeffs; i++)
			{
				sigmas.push_back(
					-dNodes[i + 1] 
						/ (2 * (dNodes[i] + dNodes[i + 1]) + dNodes[i] * sigmas[i - 1])
				);
				
				lambdas.push_back(
					(3 * (splitDiffs[i + 1] - splitDiffs[i]) - dNodes[i] * lambdas[i - 1])
						/ (2 * (dNodes[i] + dNodes[i + 1]) + dNodes[i] * sigmas[i - 1])
				);
			}

			// ���������� ������������� ��������
			vector<CubicSpline> splines;
			splines.assign(szSplines, CubicSpline());

			size_t posSpln = szSplines - 1;
			while (posSpln--)
				splines[posSpln].m_C = sigmas[posSpln] * splines[posSpln + 1].m_C + lambdas[posSpln];

			splines[0].m_D = splines[0].m_C / (3*dNodes[0]);
			splines[0].m_B = splitDiffs[0] + (MA_REAL(2.0 / 3.0) * dNodes[0] * splines[0].m_C);

			for (posSpln = 1; posSpln < szSplines; posSpln++)
			{
				splines[posSpln].m_D = (splines[posSpln].m_C - splines[posSpln - 1].m_C) 
					/ (3 * dNodes[posSpln]);

				splines[posSpln].m_B = splitDiffs[posSpln]
					+ (MA_REAL(2.0 / 3.0) * dNodes[posSpln] * splines[posSpln].m_C)
					+ (MA_REAL(1.0 / 3.0) * dNodes[posSpln] * splines[posSpln - 1].m_C);
			}

			posSpln = 0;
			it = values.begin();

			while (++it != values.end())
			{
				splines[posSpln].m_A = it->second;
				splines[posSpln++].m_Xk = it->first;
			}

			return splines;
		}

		inline map<MA_REAL, CubicSpline> CubicSplineInterpolation2(const map<MA_REAL, MA_REAL>& values)
		{
			const vector<CubicSpline>& splinesArr = CubicSplineInterpolation(values);
			map<MA_REAL, CubicSpline> resSplines;

			size_t posArr = 0;
			auto itEnd = values.end();
			itEnd--;

			for (auto it = values.begin(); it != itEnd; it++)
				resSplines[it->first] = splinesArr[posArr++];

			return resSplines;
		}


		CubicSplineFunction::CubicSplineFunction(const map<MA_REAL, MA_REAL>& values)
		{
			if (values.size() < 3)
				throw std::runtime_error("Invalid size of values");

			m_Splines = CubicSplineInterpolation2(values);
		}


		MA_REAL CubicSplineFunction::Calculate(MA_REAL x) const
		{
			assert(m_Splines.size());

			auto itrtSplines = m_Splines.lower_bound(x);
			if (itrtSplines == m_Splines.end())
				return m_Splines.rbegin()->second.Calculate(x);

			if (itrtSplines != m_Splines.begin())
				itrtSplines--;

			return itrtSplines->second.Calculate(x);
		}

	}
}

