#include "stdafx.h"
#include "SystemCoordinate.h"



namespace mech_a {

	namespace origin {

		SC::SC()
		{ }

		SC::SC(const Matrix& dirCosMtr)
			: m_dirCosMtr(dirCosMtr)
		{ }

		SC::SC(const Vector& dirX, const Vector& dirY, const Vector& pos)
		{
			setSC(dirX, dirY, pos);
		}

		SC::SC(const Vector& dirX, const Vector& dirY, const Vector& dirZ, const Vector& pos)
		{
			setSC(dirX, dirY, dirZ, pos);
		}

		SC::SC(const EulerAnglesCS& eulSc)
			: m_dirCosMtr(MatrixRotationYawPitchRoll(eulSc.m_Angles))
		{
			setPos(eulSc.m_Pos);
		}

		SC::SC(const QuaternionCS& quatSc)
			: m_dirCosMtr(QuaternionToMatrix(quatSc.m_Quaternion))
		{
			setPos(quatSc.m_Pos);
		}

		SC& SC::operator = (const SC& sc)
		{
			if (this != &sc)
			{
				m_dirCosMtr = sc.m_dirCosMtr;
				onDirCosMtrChanged();
			}

			return *this;
		}

		SC::operator EulerAnglesCS () const
		{
			EulerAnglesCS res;
			YawPitchRollFromMatrix(m_dirCosMtr, res.m_Angles);
			res.m_Pos = Pos();
			return res;
		}

		SC::operator QuaternionCS () const
		{
			QuaternionCS res;
			MatrixToQuaternion(m_dirCosMtr, res.m_Quaternion);
			res.m_Pos = Pos();
			return res;
		}

		/** Get direction Z */
		Vector           SC::DirZ() const
		{
			return Vector(m_dirCosMtr.VLS[2][0], m_dirCosMtr.VLS[2][1], m_dirCosMtr.VLS[2][2]);
		}

		/** Get direction X */
		Vector           SC::DirX() const
		{
			return Vector(m_dirCosMtr.VLS[0][0], m_dirCosMtr.VLS[0][1], m_dirCosMtr.VLS[0][2]);
		}

		/** Get direction Y */
		Vector           SC::DirY() const
		{
			return Vector(m_dirCosMtr.VLS[1][0], m_dirCosMtr.VLS[1][1], m_dirCosMtr.VLS[1][2]);
		}

		/** Get position */
		Vector           SC::Pos() const
		{
			return Vector(m_dirCosMtr.VLS[3][0], m_dirCosMtr.VLS[3][1], m_dirCosMtr.VLS[3][2]);
		}

		void SC::setPos(const Vector& pos)
		{
			m_dirCosMtr.VLS[3][0] = pos.X;
			m_dirCosMtr.VLS[3][1] = pos.Y;
			m_dirCosMtr.VLS[3][2] = pos.Z;
		}

		/** Get position */
		void             SC::Pos(const Vector& pos)
		{
			setPos(pos);
			onDirCosMtrChanged();
		}

		/** Get matrix transform */
		const Matrix&    SC::Tr_Matrix() const
		{
			return m_dirCosMtr;
		}

		void SC::setSC(const Matrix& sc)
		{
			m_dirCosMtr = sc;
		}

		void SC::setSC(const Vector& dirX, const Vector& dirY, const Vector& pos)
		{
			assert(Compare(AngleBetweenNormals(dirX, dirY), PIH2));
			m_dirCosMtr = LCSToWCS(pos, dirX, dirY, Cross(dirX, dirY));
		}

		void SC::setSC(const Vector& dirX, const Vector& dirY, const Vector& dirZ, const Vector& pos)
		{
			assert(Compare(AngleBetweenNormals(dirX, dirY), PIH2));
			assert(Compare(AngleBetweenNormals(dirX, dirZ), PIH2));
			assert(Compare(AngleBetweenNormals(dirY, dirZ), PIH2));
			assert(IsNCodir(dirZ, Cross(dirX, dirY)));

			m_dirCosMtr = LCSToWCS(pos, dirX, dirY, dirZ);
		}

		/** Set system coordinate by normals */
		void SC::SetSC(const Vector& dirX, const Vector& dirY, const Vector& pos)
		{
			setSC(dirX, dirY, pos);
			onDirCosMtrChanged();
		}

		/** Set system coordinate by normals */
		void SC::SetSC(const Vector& dirX, const Vector& dirY, const Vector& dirZ, const Vector& pos)
		{
			setSC(dirX, dirY, dirZ, pos);
			onDirCosMtrChanged();
		}

		/** Transform system coordinate */
		void SC::Transform(const Matrix& tr)
		{
			m_dirCosMtr *= tr;
			onDirCosMtrChanged();
		}

		void SC::TransformInPlace(const Matrix& tr)
		{
			m_dirCosMtr = TransformInPoint(m_dirCosMtr, tr);
			onDirCosMtrChanged();
		}

		/** Rotate around world OX */
		const Matrix& SC::Rotate_World_X(MA_REAL angl)
		{
			Transform(MatrixRotationX(angl));
			return m_dirCosMtr;
		}

		/** Rotate around world OY */
		const Matrix& SC::Rotate_World_Y(MA_REAL angl)
		{
			Transform(MatrixRotationY(angl));
			return m_dirCosMtr;
		}

		/** Rotate around world OZ */
		const Matrix& SC::Rotate_World_Z(MA_REAL angl)
		{
			Transform(MatrixRotationZ(angl));
			return m_dirCosMtr;
		}

		/** Rotate around position point SC world OX */
		const Matrix& SC::Rotate_Pos_World_X(MA_REAL angl)
		{
			TransformInPlace(MatrixRotationX(angl));
			return m_dirCosMtr;
		}

		/** Rotate around position point SC world OY */
		const Matrix& SC::Rotate_Pos_World_Y(MA_REAL angl)
		{
			TransformInPlace(MatrixRotationY(angl));
			return m_dirCosMtr;
		}

		/** Rotate around position point SC world OZ */
		const Matrix& SC::Rotate_Pos_World_Z(MA_REAL angl)
		{
			TransformInPlace(MatrixRotationZ(angl));
			return m_dirCosMtr;
		}

		/** Rotate around position point and OX SC*/
		const Matrix& SC::Rotate_SC_X(MA_REAL angl)
		{
			TransformInPlace(MatrixRotationAxis(DirX(), angl));
			return m_dirCosMtr;
		}

		/** Rotate around position point and OY SC*/
		const Matrix& SC::Rotate_SC_Y(MA_REAL angl)
		{
			TransformInPlace(MatrixRotationAxis(DirY(), angl));
			return m_dirCosMtr;
		}

		/** Rotate around position point and OZ SC*/
		const Matrix& SC::Rotate_SC_Z(MA_REAL angl)
		{
			TransformInPlace(MatrixRotationAxis(DirZ(), angl));
			return m_dirCosMtr;
		}
	}
}