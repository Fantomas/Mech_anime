#include "stdafx.h"
#include "Quaternion.h"
#include "MathFunctions.h"


namespace mech_a {

	namespace math {

		MA_REAL Quaternion::GetValue() const
		{
			return pow(m_W*m_W + m_X*m_X + m_Y*m_Y + m_Z*m_Z, MA_REAL(0.5));
		}

		bool Quaternion::Normalize()
		{
			MA_REAL vl = GetValue();
			if (Compare(0, vl))
				return false;

			m_W /= vl; m_X /= vl; m_Y /= vl; m_Z /= vl;
			return true;
		}

		Quaternion Quaternion::operator * (const Quaternion& qt) const
		{
			Vector v1 = *this;
			Vector v2 = qt;

			MA_REAL dot = m_W * qt.m_W - Dot(v1, v2);
			Vector vct = m_W * v2 + qt.m_W * v1 + Cross(v1, v2);

			return Quaternion(dot, vct);
		}

		// сопряженный данному
		Quaternion Quaternion::GetConjugate() const
		{
			return Quaternion(m_W, -m_X, -m_Y, -m_Z);
		}

		void Quaternion::SetAxisAngl(MA_REAL angle, Vector axis)
		{
			if (!axis.Normalize())
			{
				m_X = m_Y = m_Z = 0;
				m_W = 1;
				return;
			}

			const MA_REAL sin_h = sin(angle / 2);
			const MA_REAL cos_h = cos(angle / 2);

			axis *= sin_h;

			m_W = cos_h;
			m_X = axis.X; m_Y = axis.Y; m_Z = axis.Z;
		}

		void Quaternion::GetAxisAngl(OUT MA_REAL& angle, OUT Vector& axis) const
		{
			const MA_REAL cos_h = m_W;
			angle = acos(cos_h) * 2;

			if (Compare(angle, 0) || Compare(angle, PI2))
			{
				angle = 0;
				axis = Vector::OZ();
				return;
			}

			MA_REAL sin_h = MA_REAL(sqrt(1.0 - cos_h*cos_h));

			axis.X = m_X / sin_h;
			axis.Y = m_Y / sin_h;
			axis.Z = m_Z / sin_h;

			bool res = axis.Normalize();
			assert(res);

			if (angle > PI)
			{
				angle = PI2 - angle;
				axis *= -1;
			}
		}
	}
}