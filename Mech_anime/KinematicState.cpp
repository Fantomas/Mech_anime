#include "stdafx.h"
#include "KinematicState.h"


using namespace std;
using namespace mech_a;
using namespace mech_a::math;


namespace mech_a {

	namespace origin {


		KinematicState::KinematicState()
			: m_dirVelocity(Vector::OZ())
			, m_velocity(0)
			, m_dirAngularVelocity(Vector::OZ())
			, m_angularVelocity(0)
		{ }

		KinematicState::KinematicState(const Vector& dirVelocity, MA_REAL velocity, Vector dirAngularVelocity, MA_REAL angularVelocity)
			: m_dirVelocity(dirVelocity)
			, m_velocity(velocity)
			, m_dirAngularVelocity(dirAngularVelocity)
			, m_angularVelocity(angularVelocity)
		{
			assert(m_dirVelocity.IsNormal());
			assert(m_dirAngularVelocity.IsNormal());
			assert(m_velocity >= 0);
			assert(m_angularVelocity >= 0);
		}

		void KinematicState::Set(const Vector& dirVelocity, MA_REAL velocity)
		{
			assert(dirVelocity.IsNormal());
			assert(velocity >= 0);

			m_dirVelocity = dirVelocity;
			m_velocity = velocity;
		}

		void KinematicState::SetAngularChars(const Vector& dirAngularVelocity, MA_REAL angularVelocity)
		{
			assert(dirAngularVelocity.IsNormal());
			assert(angularVelocity >= 0);

			m_dirAngularVelocity = dirAngularVelocity;
			m_angularVelocity = angularVelocity;
		}

		void KinematicState::Set(const Vector& dirVelocity, MA_REAL velocity, const Vector& dirAngularVelocity, MA_REAL angularVelocity)
		{
			assert(dirVelocity.IsNormal());
			assert(dirAngularVelocity.IsNormal());
			assert(velocity >= 0);
			assert(angularVelocity >= 0);

			m_dirVelocity = dirVelocity;
			m_velocity = velocity;
			m_dirAngularVelocity = dirAngularVelocity;
			m_angularVelocity = angularVelocity;
		}

		const Vector&  KinematicState::DirVelocity() const { return m_dirVelocity; }
		MA_REAL           KinematicState::Velocity() const { return m_velocity; }
		const Vector&  KinematicState::DirAngularVelocity() const { return m_dirAngularVelocity; }
		MA_REAL           KinematicState::AngularVelocity() const { return m_angularVelocity; }

		void  KinematicState::SetDirVelocity(const Vector& dir)
		{
			assert(dir.IsNormal());
			m_dirVelocity = dir;
		}

		void  KinematicState::SetVelocity(MA_REAL vl)
		{
			assert(vl >= 0);
			m_velocity = vl;
		}

		void  KinematicState::SetDirAngularVelocity(const Vector& dir)
		{
			assert(dir.IsNormal());
			m_dirAngularVelocity = dir;
		}

		void  KinematicState::AngularVelocity(MA_REAL vl)
		{
			assert(vl >= 0);
			m_angularVelocity = vl;
		}
	}
}