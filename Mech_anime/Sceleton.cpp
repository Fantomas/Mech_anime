#include "stdafx.h"
#include "Definitions.h"
#include "Sceleton.h"


using namespace mech_a;
using namespace mech_a::origin;
using namespace mech_a::math;


namespace mech_a {
	namespace sceleton {


		template<class SC_TYPE_IN, class SC_TYPE_OUT>
		void convertPoses(const std::unordered_map<ULONG, SC_TYPE_IN>& globPoses, OUT std::unordered_map<ULONG, SC_TYPE_OUT>& resGlobPoses)
		{
			for (const auto& itrtPairInPoses : globPoses)
			{
				SC_TYPE_OUT outSC;
				FromCS_ToCS(itrtPairInPoses.second, outSC);

				resGlobPoses.emplace(itrtPairInPoses.first, outSC);
			}
		}


		void SceletonData::FromStream(IN std::stringstream& stream)
		{
			DWORD majorVer = 0;
			stream.read((char*)&majorVer, sizeof(DWORD));

			DWORD minorVer = 0;
			stream.read((char*)&minorVer, sizeof(DWORD));

			DWORD locWtldCsType = 0;
			stream.read((char*)&locWtldCsType, sizeof(DWORD));

			DWORD interpolationType = 0;
			stream.read((char*)&interpolationType, sizeof(DWORD));

#if LOCAL_WORLD_CS_TYPE == LOCAL_QUAT_WORLD_QUAT

			if (locWtldCsType != LOCAL_QUAT_WORLD_QUAT)
			{
				if (locWtldCsType != LOCAL_EULER_WORLD_SC)
					throw std::runtime_error("fail read sc type");

// 				if (interpolationType != IT_Cubical_Spline_Euler)
// 					throw std::runtime_error("fail read sc type");

				// convert to quaternions sc
				std::unordered_map<ULONG, _MotionData<EulerAnglesCS, SC, IT_Cubical_Spline_Euler>>  eulMotions;
				_StateData<EulerAnglesCS, SC> eulBeginState;
				std::unordered_map<ULONG, _ConnectionData<SC>> eulConnections;
				std::unordered_map<ULONG, _StateData<EulerAnglesCS, SC>> eulSpecialStates;

				MapFromStream(eulMotions, stream);
				eulBeginState.FromStream(stream);
				MapFromStream(m_Elements, stream);
				MapFromStream(eulConnections, stream);
				MapFromStream(eulSpecialStates, stream);

				// motions
				for (auto& itrtMotPair : eulMotions)
				{
					_MotionData<EulerAnglesCS, SC, IT_Cubical_Spline_Euler>& currMotData = itrtMotPair.second;

					auto itrtQuatMot = m_Motions.emplace(itrtMotPair.first, _MotionData<QuaternionCS, QuaternionCS, IT_Linear_Quaternion>(
						currMotData.m_ID, currMotData.m_ID_Relatively_Part, currMotData.m_Name)
						);

					assert(itrtQuatMot.second);

					// states
					for (auto& itrtStatePair : currMotData.m_States)
					{
						_StateData<EulerAnglesCS, SC>& currStateData = itrtStatePair.second;

						auto itrtQuatState = itrtQuatMot.first->second.m_States.emplace(itrtStatePair.first, _StateData<QuaternionCS, QuaternionCS>
							(currStateData.m_ID, currStateData.m_Name)
							);

						assert(itrtQuatState.second);

						// global poses						
						convertPoses<SC, QuaternionCS>(currStateData.m_GlobalPoseElements, itrtQuatState.first->second.m_GlobalPoseElements);

						// local poses
						convertPoses<EulerAnglesCS, QuaternionCS>(currStateData.m_LocalPoseElements, itrtQuatState.first->second.m_LocalPoseElements);
					}
				}

				// begin states
				m_BeginState = _StateData<QuaternionCS, QuaternionCS>(eulBeginState.m_ID, eulBeginState.m_Name);
				convertPoses<SC, QuaternionCS>(eulBeginState.m_GlobalPoseElements, m_BeginState.m_GlobalPoseElements);
				convertPoses<EulerAnglesCS, QuaternionCS>(eulBeginState.m_LocalPoseElements, m_BeginState.m_LocalPoseElements);

				// connections
				for (auto& itrtConnectPair : eulConnections)
				{
					_ConnectionData<SC>& currConnect = itrtConnectPair.second;

					auto itrtConnect = m_Connections.emplace(itrtConnectPair.first, _ConnectionData<QuaternionCS>(currConnect.m_ID, currConnect.m_Name));
					assert(itrtConnect.second);

					itrtConnect.first->second.m_ExtentsFreedom = currConnect.m_ExtentsFreedom;
					itrtConnect.first->second.m_ElementA = currConnect.m_ElementA;
					itrtConnect.first->second.m_ElementB = currConnect.m_ElementB;

					FromCS_ToCS(currConnect.m_BasicLCS, itrtConnect.first->second.m_BasicLCS);
				}

				// special states
				for (auto& itrtSpecStatePair : eulSpecialStates)
				{
					_StateData<EulerAnglesCS, SC>& specState = itrtSpecStatePair.second;

					auto itrtQuatState = m_SpecialStates.emplace(itrtSpecStatePair.first, _StateData<QuaternionCS, QuaternionCS>
						(specState.m_ID, specState.m_Name)
						);

					assert(itrtQuatState.second);

					// global poses						
					convertPoses<SC, QuaternionCS>(specState.m_GlobalPoseElements, itrtQuatState.first->second.m_GlobalPoseElements);

					// local poses
					convertPoses<EulerAnglesCS, QuaternionCS>(specState.m_LocalPoseElements, itrtQuatState.first->second.m_LocalPoseElements);
				}

				return;
			}

#endif


			MapFromStream(m_Motions, stream);
			m_BeginState.FromStream(stream);
			MapFromStream(m_Elements, stream);
			MapFromStream(m_Connections, stream);
			MapFromStream(m_SpecialStates, stream);
		}


	}
}