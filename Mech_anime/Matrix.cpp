#include "stdafx.h"
#include "Matrix.h"


using namespace std;


namespace mech_a {

	namespace math {

		Matrix::Matrix()
			: V_11(1), V_12(0), V_13(0), V_14(0)
			, V_21(0), V_22(1), V_23(0), V_24(0)
			, V_31(0), V_32(0), V_33(1), V_34(0)
			, V_41(0), V_42(0), V_43(0), V_44(1)
		{ }

		Matrix::Matrix(MA_REAL v_11, MA_REAL v_12, MA_REAL v_13, MA_REAL v_14
			, MA_REAL v_21, MA_REAL v_22, MA_REAL v_23, MA_REAL v_24
			, MA_REAL v_31, MA_REAL v_32, MA_REAL v_33, MA_REAL v_34
			, MA_REAL v_41, MA_REAL v_42, MA_REAL v_43, MA_REAL v_44
			)
			: V_11(v_11), V_12(v_12), V_13(v_13), V_14(v_14)
			, V_21(v_21), V_22(v_22), V_23(v_23), V_24(v_24)
			, V_31(v_31), V_32(v_32), V_33(v_33), V_34(v_34)
			, V_41(v_41), V_42(v_42), V_43(v_43), V_44(v_44)
		{ }

		MA_REAL* Matrix::operator [] (UINT row)
		{
			assert(row < 4);
			return VLS[row];
		}

		const MA_REAL* Matrix::operator [] (UINT row) const
		{
			assert(row < 4);
			return VLS[row];
		}

		MA_REAL Matrix::operator()(UINT row, UINT col) const
		{
			assert(row < 4 && col < 4);
			return VLS[row][col];
		}

		Matrix Matrix::operator * (MA_REAL vl) const
		{
			return Matrix(V_11 * vl, V_12 * vl, V_13 * vl, V_14 * vl,
				V_21 * vl, V_22 * vl, V_23 * vl, V_24 * vl,
				V_31 * vl, V_32 * vl, V_33 * vl, V_34 * vl,
				V_41 * vl, V_42 * vl, V_43 * vl, V_44 * vl);
		}

		Matrix& Matrix::operator *= (MA_REAL vl)
		{
			for (UINT i = 0; i < 4; i++)
				for (UINT j = 0; j < 4; j++)
					VLS[i][j] *= vl;

			return *this;
		}

		Matrix Matrix::operator / (MA_REAL vl) const
		{
			return Matrix(V_11 / vl, V_12 / vl, V_13 / vl, V_14 / vl,
				V_21 / vl, V_22 / vl, V_23 / vl, V_24 / vl,
				V_31 / vl, V_32 / vl, V_33 / vl, V_34 / vl,
				V_41 / vl, V_42 / vl, V_43 / vl, V_44 / vl);
		}

		Matrix& Matrix::operator /= (MA_REAL vl)
		{
			for (UINT i = 0; i < 4; i++)
				for (UINT j = 0; j < 4; j++)
					VLS[i][j] /= vl;

			return *this;
		}

		Matrix Matrix::operator + (const Matrix& mtr) const
		{
			Matrix outMtr;

			for (UINT i = 0; i < 4; i++)
				for (UINT j = 0; j < 4; j++)
					outMtr.VLS[i][j] = VLS[i][j] + mtr.VLS[i][j];

			return outMtr;
		}

		Matrix& Matrix::operator += (const Matrix& mtr)
		{
			for (UINT i = 0; i < 4; i++)
				for (UINT j = 0; j < 4; j++)
					VLS[i][j] += mtr.VLS[i][j];

			return *this;
		}

		Matrix Matrix::operator - (const Matrix& mtr) const
		{
			Matrix outMtr;

			for (UINT i = 0; i < 4; i++)
				for (UINT j = 0; j < 4; j++)
					outMtr.VLS[i][j] = VLS[i][j] - mtr.VLS[i][j];

			return outMtr;
		}

		Matrix& Matrix::operator -= (const Matrix& mtr)
		{
			for (UINT i = 0; i < 4; i++)
				for (UINT j = 0; j < 4; j++)
					VLS[i][j] -= mtr.VLS[i][j];

			return *this;
		}

		Matrix Matrix::operator * (const Matrix& mtr) const
		{
			Matrix out;

			for (UINT i = 0; i < 4; i++)
			{
				for (UINT j = 0; j < 4; j++)
				{
					MA_REAL summ = 0;
					for (UINT k = 0; k < 4; k++)
						summ += VLS[i][k] * mtr.VLS[k][j];

					out.VLS[i][j] = summ;
				}
			}

			return out;
		}

		Matrix& Matrix::operator *= (const Matrix& mtr)
		{
			return *this = *this * mtr;
		}
	}
}