#include "stdafx.h"
#include "SceletonSupport.h"
#include "SceletonMethods.h"


using namespace mech_a;
using namespace mech_a::sceleton;
using namespace mech_a::origin;


namespace mech_a {
	namespace sceleton {


		void Connection::setElementA(Element* pElementA)
		{
			m_pElementA = pElementA;
			m_pConnectionData->m_ElementA = pElementA ? pElementA->ID() : 0;

			if (!pElementA)
			{
				setBasicLCS(SceletonData::TypeGlobalCS::Default());
				setLCS_WCS(GetWCS());
			}

			OnPropertyChanged(L"ElemantA");
		}

		void Connection::setElementB(Element* pElementB)
		{
			m_pElementB = pElementB;
			m_pConnectionData->m_ElementB = pElementB ? pElementB->ID() : 0;

			OnPropertyChanged(L"ElemantB");
		}

		void Element::addChildConnect(Connection* pConnect)
		{
			if (!pConnect)
				return;

			m_psConnects.insert(pConnect);
			m_pElementData->m_ConnectIDs.insert(pConnect->ID());

			OnPropertyChanged(L"ChildConnects");
		}

		void Element::removeChildConnect(Connection* pConnect)
		{
			if (!pConnect)
				return;

			m_psConnects.erase(pConnect);
			m_pElementData->m_ConnectIDs.erase(pConnect->ID());

			OnPropertyChanged(L"ChildConnects");
		}

		IPartSceleton* Connection::GetParentPart() const
		{
			return m_pElementA;
		}

		std::unordered_set<IPartSceleton*> Connection::GetChildParts() const
		{
			std::unordered_set<IPartSceleton*> resArr;

			if (m_pElementB)
				resArr.insert(m_pElementB);

			return resArr;
		}

		void Connection::RetransformChilds(const SceletonData::TypeGlobalCS& fromWCS)
		{
			UpdateSceletonDownWCS(this, fromWCS);
		}

		void Connection::Retransform(const SceletonData::TypeGlobalCS& fromWCS)
		{
			UpdateSceleton(this, fromWCS);
		}

		void Connection::RetransformLocal(const SceletonData::TypeLocalCS& newLCS, bool down)
		{
			if (down)
				UpdateSceletonDownLCS(this, newLCS);
			else
				UpdateSceletonUpLCS(this, newLCS);
		}

		void Connection::RetransformBasicLocal(const SceletonData::TypeGlobalCS& newBasicLCS)
		{
			UpdateSceletonDownBasicLCS(this, newBasicLCS);
		}

		void Connection::notifyWCS()
		{
			OnPropertyChanged(L"WCS");

			if (m_pElementB)
				m_pElementB->OnPropertyChanged(L"WCS");
		}

		void Connection::setWCS(const SceletonData::TypeGlobalCS& wcs)
		{
			*m_pWCS = wcs;
			notifyWCS();
		}

		void Connection::updateWCS_onlyConnect(const SceletonData::TypeGlobalCS& wcs)
		{
			setWCS(wcs);

			if (!m_pElementA)
			{
				setBasicLCS(SceletonData::TypeGlobalCS::Default());
				setLCS_WCS(wcs);
				return;
			}

			setLCS(SceletonData::TypeLocalCS::Default());

			Connection* pParentConnection = m_pElementA->GetParentConnect();
			assert(pParentConnection);

			SceletonData::TypeGlobalCS basicLSC;
			GetLocalTransform(*pParentConnection->m_pWCS, *m_pWCS, basicLSC);
			setBasicLCS(basicLSC);
		}

		SceletonData::TypeGlobalCS Connection::GetBasicWCS() const
		{
			if (!m_pElementA)
				return SceletonData::TypeGlobalCS::Default();

			Connection* pParentConnect = m_pElementA->GetParentConnect();
			assert(pParentConnect);

			SceletonData::TypeGlobalCS res;
			ReTransform(*pParentConnect->m_pWCS, m_pConnectionData->m_BasicLCS, res);
			return res;
		}

		void Connection::UpdateWCSFromElementA()
		{
			ReTransform(GetBasicWCS(), *m_pLCS, *m_pWCS);
			notifyWCS();
		}

		void Connection::UpdateWCSFromLCS_AndUpdate()
		{
			SceletonData::TypeGlobalCS wcs;
			FromCS_ToCS(*m_pLCS, wcs);
			
			setWCS(wcs);
			setBasicLCS(SceletonData::TypeGlobalCS::Default());

			UpdateSceletonFromConnection(this);
		}


		SceletonData::TypeGlobalCS Connection::GetGlobalLCS() const
		{
			SceletonData::TypeGlobalCS gLCS;
			ReTransform(m_pConnectionData->m_BasicLCS, *m_pLCS, gLCS);
			return gLCS;
		}


		SC calculateParentWCS(const SC& wcs, const SC& lcs, const SC& basicLcs)
		{
			return InverseDirectionCosMatrix(basicLcs.Tr_Matrix()) * InverseDirectionCosMatrix(lcs.Tr_Matrix()) * wcs.Tr_Matrix();
		}

		QuaternionCS calculateParentWCS(const QuaternionCS& wcs, const QuaternionCS& lcs, const QuaternionCS& basicLcs)
		{
			return wcs * lcs.Inverse() * basicLcs.Inverse();
		}

		SceletonData::TypeGlobalCS Connection::CalculateParentWCS() const
		{
			SceletonData::TypeGlobalCS lcsWorldCoord;
			FromCS_ToCS(*m_pLCS, lcsWorldCoord);

			return calculateParentWCS(*m_pWCS, lcsWorldCoord, m_pConnectionData->m_BasicLCS);
		}

	}
}