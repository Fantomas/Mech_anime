#pragma once

#include <SDKDDKVer.h>

// #define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#pragma warning(disable: 4201)
#pragma warning(disable: 4251)
#pragma warning(disable: 4503)

// crt headers


// stl hedaers
#include <memory>
#include <limits>
#include <cassert>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <functional>
#include <queue>
#include <map>

// Windows Header Files:
#include <windows.h>


