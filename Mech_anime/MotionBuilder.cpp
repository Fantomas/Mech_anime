#include "stdafx.h"
#include "MotionBuilder.h"

using namespace mech_a;
using namespace mech_a::sceleton;
using namespace mech_a::origin;


namespace mech_a {
	namespace sceleton {

		template<class StateDataPtr>
		State<StateDataPtr>::State(StateDataPtr stateData, IStateBuilder* pMotion)
			: m_StateData(stateData)
		{
			assert(stateData && pMotion);

			SceletonData& sceletonData = pMotion->GetSceletonData();
			LoadParts(sceletonData.m_Elements, sceletonData.m_Connections, *stateData, m_Parts);

			m_PartsNotifier.reset(new PartStateNotifier(pMotion));

			for (std::pair <const ULONG, std::unique_ptr<IPartSceleton>>& scelPart : m_Parts)
				scelPart.second->AddPropertyNotifier(m_PartsNotifier.get());
		}

		template<class StateDataPtr>
		State<StateDataPtr>::State(StateDataPtr stateData, IN SceletonData& scelData)
			: m_StateData(stateData)
		{
			assert(stateData);

			LoadParts(scelData.m_Elements, scelData.m_Connections, *stateData, m_Parts);
		}

		ULONG IState::ID() const
		{
			StateData* pStateData = GetStateDataRef();
			assert(pStateData);

			return pStateData->m_ID;
		}

		const std::wstring& IState::Name() const
		{
			StateData* pStateData = GetStateDataRef();
			assert(pStateData);

			return pStateData->m_Name;
		}

		void IState::SetName(const std::wstring& name)
		{
			StateData* pStateData = GetStateDataRef();
			assert(pStateData);

			pStateData->m_Name = name;
		}

		std::unordered_set<IPartSceleton*> IState::GetParts() const
		{
			std::unordered_set<IPartSceleton*> resPart;

			for (const auto& it : m_Parts)
				resPart.insert(it.second.get());

			return resPart;
		}

		std::unordered_map<UINT, IPartSceleton*> IState::GetPartsMap() const
		{
			std::unordered_map<UINT, IPartSceleton*> resParts;

			for (const auto& it : m_Parts)
				resParts.emplace(it.first, it.second.get());

			return resParts;
		}

		std::unordered_map<UINT, Connection*> IState::GetConnections() const
		{
			std::unordered_map<UINT, Connection*> resConnects;

			for (const auto& it : m_Parts)
			{
				if (Connection* pConn = dynamic_cast<Connection*>(it.second.get()))
					resConnects.emplace(it.first, pConn);
			}

			return resConnects;
		}

		IPartSceleton* IState::GetPartByName(const std::wstring& name) const
		{
			for (const auto& it : m_Parts)
				if (it.second->Name() == name)
					return it.second.get();

			return NULL;
		}

		LocalPositions IState::GetLocalPositions() const
		{
			LocalPositions resPositions;

			for (const auto& it : m_Parts)
			{
				if (Connection* pConn = dynamic_cast<Connection*>(it.second.get()))
					resPositions.emplace(pConn->ID(), pConn->GetLCS());
			}

			return resPositions;
		}

		GlobalPositions IState::GetGlobalPositions() const
		{
			GlobalPositions resPositions;

			for (const auto& it : m_Parts)
			{
				if (Connection* pConn = dynamic_cast<Connection*>(it.second.get()))
					resPositions.emplace(pConn->ID(), pConn->GetWCS());
			}

			return resPositions;
		}

		void IState::ToStream(OUT std::stringstream& stream)
		{
			for (const auto& it : m_Parts)
			{
				Connection* pConnect = dynamic_cast<Connection*>(it.second.get());
				if (!pConnect)
					continue;

				pConnect->SetCoordinatesToStream(stream);
			}
		}

		void IState::FromStream(IN std::stringstream& stream)
		{
			for (const auto& it : m_Parts)
			{
				Connection* pConnect = dynamic_cast<Connection*>(it.second.get());
				if (!pConnect)
					continue;

				pConnect->LoadCoordinatesFromStream(stream);
			}
		}

		// ------------------------------------------------------------------------------------------------------------

		MotionPlayer::MotionPlayer(const StateData& startState, MA_REAL startTimeStamp, Motion* pMotion)
			: m_StateData(startState)
			, m_DefautStateData(startState)
			, m_CurrentTimeStamp(startTimeStamp)
			, m_pMotion(pMotion)
		{
			assert(pMotion);
			assert(startTimeStamp >= 0 && startTimeStamp <= 1);

			m_State.reset(new StateBasic(&m_StateData, pMotion->GetSceletonData()));
		}

		void MotionPlayer::setStateData(const StateData& stateData)
		{
			for (auto& globPos : stateData.m_GlobalPoseElements)
			{
				auto itGlobPos = m_StateData.m_GlobalPoseElements.find(globPos.first);
				assert(itGlobPos != m_StateData.m_GlobalPoseElements.end());
				itGlobPos->second = globPos.second;
			}

			for (auto& localPos : stateData.m_LocalPoseElements)
			{
				auto itLocalPos = m_StateData.m_LocalPoseElements.find(localPos.first);
				assert(itLocalPos != m_StateData.m_LocalPoseElements.end());
				itLocalPos->second = localPos.second;
			}
		}

		MA_REAL MotionPlayer::SetTimeStamp(MA_REAL timeStamp)
		{
			if (timeStamp < 0 || timeStamp > 1)
				return 0;

			const size_t szStates = m_pMotion->m_States.size();
			const std::map<MA_REAL, StateData>& states = m_pMotion->m_MotionData->m_States;

			if (!szStates)
			{
				m_CurrentTimeStamp = 0;
				setStateData(m_DefautStateData);
				return m_CurrentTimeStamp;
			}
			else if (szStates < 3)
			{
				m_CurrentTimeStamp = 0;
				setStateData(states.begin()->second);
				return m_CurrentTimeStamp;
			}
			else
			{
				MA_REAL bVl = states.begin()->first;
				MA_REAL eVl = states.rbegin()->first;

				const MA_REAL dTime = eVl - bVl;

				m_CurrentTimeStamp = bVl + (dTime * timeStamp);
			}

			recalcTimePosition();
			return timeStamp;
		}

		void MotionPlayer::UpdatePlayer()
		{
			// validation time stamp
			const size_t szStates = m_pMotion->m_States.size();
			const std::map<MA_REAL, StateData>& states = m_pMotion->m_MotionData->m_States;
		
			if (szStates < 3)
			{
				m_CurrentTimeStamp = 0;

				if (!szStates)
					setStateData(m_DefautStateData);
				else
					setStateData(states.begin()->second);
				
				return;
			}

			if (m_CurrentTimeStamp < states.begin()->first)
			{
				m_CurrentTimeStamp = states.begin()->first;
				setStateData(states.begin()->second);
				return;
			}
			
			if (m_CurrentTimeStamp > states.rbegin()->first)
			{
				m_CurrentTimeStamp = states.rbegin()->first;
				setStateData(states.rbegin()->second);
				return;
			}

			// recalc position
			recalcTimePosition();
		}

		void MotionPlayer::recalcTimePosition()
		{
			assert(m_pMotion->m_States.size() >= 3);

			// set new local coordinates
			std::unordered_map<ULONG, SceletonData::TypeLocalCS> newLCS;
			InterpolationState(*m_pMotion->m_MotionData, m_CurrentTimeStamp, m_pMotion->GetSceletonData().m_Connections, newLCS);

			for (auto& localPos : newLCS)
			{
				auto itLocalPos = m_StateData.m_LocalPoseElements.find(localPos.first);
				assert(itLocalPos != m_StateData.m_LocalPoseElements.end());
				itLocalPos->second = localPos.second;
			}

			// set new world coordinates
			ULONG idRelPart = m_pMotion->ID_Relatively_Part();

			const std::unordered_map<UINT, IPartSceleton*>& parts = m_State->GetPartsMap();
			auto itrtPart = parts.find(idRelPart);
			assert(itrtPart != parts.end());

			Connection* pRelativeConnect = dynamic_cast<Connection*>(itrtPart->second);
			assert(pRelativeConnect);

			if (pRelativeConnect->GetElementA()) // connection is non autonomus
			{
				const std::map<MA_REAL, StateData>& states = m_pMotion->m_MotionData->m_States;

				auto rightState = states.lower_bound(m_CurrentTimeStamp);
				auto leftState = rightState;

				if (rightState == states.begin())
					rightState++;
				else
					leftState--;

				auto itrtL_WCS = leftState->second.m_GlobalPoseElements.find(idRelPart);
				assert(itrtL_WCS != leftState->second.m_GlobalPoseElements.end());

				auto itrtR_WCS = rightState->second.m_GlobalPoseElements.find(idRelPart);
				assert(itrtR_WCS != rightState->second.m_GlobalPoseElements.end());

				const MA_REAL timePos = (m_CurrentTimeStamp - leftState->first) / (rightState->first - leftState->first);
				const SceletonData::TypeGlobalCS& newWCS = LinearInterpolation(itrtL_WCS->second, itrtR_WCS->second, timePos);

				pRelativeConnect->Retransform(newWCS);
			}
			else
			{
				pRelativeConnect->UpdateWCSFromLCS_AndUpdate();
			}
		}

		// -------------------------------------------------------------------------------------------------------------


		Motion::Motion(MotionsMapPtr motionData, MotionBuilder* pMotionBuilder)
			: m_MotionData(motionData)
			, m_pMotionBuilder(pMotionBuilder)
		{
			assert(m_MotionData);
			assert(m_pMotionBuilder);

			std::map<MA_REAL, SceletonData::StateData>& states = motionData->m_States;

			for (auto itStates = states.begin(); itStates != states.end(); itStates++)
				m_States.emplace(itStates->second.m_ID, std::unique_ptr<StateMotion>(new StateMotion(itStates, this)));

			if (states.size())
				m_MotionPlayer.reset(new MotionPlayer((*states.begin()).second, (*states.begin()).first, this));
			else
				m_MotionPlayer.reset(new MotionPlayer(pMotionBuilder->m_SceletonData.m_BeginState, 0, this));
		}

		SceletonData& Motion::GetSceletonData()
		{
			return m_pMotionBuilder->GetSceletonData();
		}

		ULONG  Motion::ID() const
		{
			assert(m_MotionData);
			return m_MotionData->m_ID;
		}

		ULONG  Motion::ID_Relatively_Part() const
		{
			assert(m_MotionData);
			return m_MotionData->m_ID_Relatively_Part;
		}

		bool Motion::SetID_Relatively_Part(ULONG idRelPart)
		{
			assert(m_MotionData && m_pMotionBuilder && m_MotionPlayer);

			if (m_pMotionBuilder->m_SceletonData.m_Connections.find(idRelPart) == m_pMotionBuilder->m_SceletonData.m_Connections.end())
				return false;

			m_MotionData->m_ID_Relatively_Part = idRelPart;
			recalcInterpolationData();
			m_MotionPlayer->UpdatePlayer();
			return true;
		}

		const std::wstring& Motion::Name() const
		{
			assert(m_MotionData);
			return m_MotionData->m_Name;
		}

		void Motion::SetName(const std::wstring& name)
		{
			assert(m_MotionData);
			m_MotionData->m_Name = name;
		}

		std::unordered_set<StateMotion*> Motion::GetStates() const
		{
			std::unordered_set<StateMotion*> resStates;

			for (const auto& it : m_States)
				resStates.emplace(it.second.get());

			return resStates;
		}

		std::unordered_map<UINT, StateMotion*> Motion::GetStatesIDMap() const
		{
			std::unordered_map<UINT, StateMotion*> resStates;

			for (const auto& it : m_States)
				resStates.emplace(it.first, it.second.get());

			return resStates;
		}

		std::unordered_map<MA_REAL, StateMotion*> Motion::GetStatesMap() const
		{
			std::unordered_map<MA_REAL, StateMotion*> resStates;

			for (const auto& it : m_States)
				resStates.emplace(it.second->m_StateData.GetKey(), it.second.get());

			return resStates;
		}

		StateMotion* Motion::CreateState(ULONG idBaseState, const std::wstring& name, MA_REAL timeStamp)
		{
			assert(m_pMotionBuilder);
			assert(m_MotionData);

			if (timeStamp < 0 || timeStamp > 1)
				return NULL;

			if (m_MotionData->m_States.find(timeStamp) != m_MotionData->m_States.end())
				return NULL;

			const std::unordered_map<ULONG, IState*>& states = m_pMotionBuilder->GetStates();
			auto itrtBaseState = states.find(idBaseState);
			if (itrtBaseState == states.end())
				return NULL;

			UINT idNewState = m_pMotionBuilder->m_StateIdGenerator.GetID();
			StateData* pBaseStateData = (*itrtBaseState).second->GetStateDataRef();

			auto resNewState = m_MotionData->m_States.emplace(timeStamp, StateData(idNewState, name, pBaseStateData->m_GlobalPoseElements, pBaseStateData->m_LocalPoseElements));
			assert(resNewState.second);

			auto resStateMotion = m_States.emplace(resNewState.first->second.m_ID, std::unique_ptr<StateMotion>(new StateMotion(resNewState.first, this)));

			assert(m_MotionPlayer);
			recalcInterpolationData();
			m_MotionPlayer->UpdatePlayer();

			return resStateMotion.second ? resStateMotion.first->second.get() : NULL;
		}

		void Motion::RemoveState(ULONG idState)
		{
			auto itState = m_States.find(idState);
			if (itState == m_States.end())
				return;

			m_MotionData->m_States.erase(itState->second->m_StateData.GetKey());
			m_States.erase(itState);
			m_pMotionBuilder->m_StateIdGenerator.RemoveID(idState);
		}


		template<class T_LCS, class T_WCS, InterpolationType IT>
		void _recalcInterpolationData(IN _MotionData<T_LCS, T_WCS, IT>& motionData)
		{ }

		template<>
		void _recalcInterpolationData<EulerAnglesCS, SC, InterpolationType::IT_Cubical_Spline_Euler>(IN _MotionData<EulerAnglesCS, SC
			, InterpolationType::IT_Cubical_Spline_Euler>& motionData)
		{
			motionData.m_InterpolationData.m_Splines.clear();

			if (motionData.m_States.size() < 3)
				return;

			struct EulersPrvData
			{
				std::map<MA_REAL, MA_REAL>  m_Yaw;
				std::map<MA_REAL, MA_REAL>  m_Pitch;
				std::map<MA_REAL, MA_REAL>  m_Roll;
			};

			std::unordered_map<ULONG, EulersPrvData> prvData;
 
			for (const std::pair<MA_REAL, _StateData<EulerAnglesCS, SC>>& stateStamp : motionData.m_States)
			{
				for (const std::pair<ULONG, EulerAnglesCS>& connectLocalPose : stateStamp.second.m_LocalPoseElements)
				{
					EulersPrvData& eulersPrvData = prvData[connectLocalPose.first];
					eulersPrvData.m_Yaw[stateStamp.first] = connectLocalPose.second.m_Angles.m_yaw;
					eulersPrvData.m_Pitch[stateStamp.first] = connectLocalPose.second.m_Angles.m_pitch;
					eulersPrvData.m_Roll[stateStamp.first] = connectLocalPose.second.m_Angles.m_roll;
				}
			}

			for (const std::pair<ULONG, EulersPrvData>& eulPrvData : prvData)
			{
				const std::map<MA_REAL, CubicSpline>& resSplinesYaw = CubicSplineInterpolation2(eulPrvData.second.m_Yaw);
				const std::map<MA_REAL, CubicSpline>& resSplinesPitch = CubicSplineInterpolation2(eulPrvData.second.m_Pitch);
				const std::map<MA_REAL, CubicSpline>& resSplinesRoll = CubicSplineInterpolation2(eulPrvData.second.m_Roll);

				for (const std::pair<MA_REAL, CubicSpline>& crSplineYaw : resSplinesYaw)
				{
					motionData.m_InterpolationData.m_Splines[crSplineYaw.first][eulPrvData.first] 
						= EulerAnglesSplineInterpolation{ resSplinesYaw.at(crSplineYaw.first), resSplinesPitch.at(crSplineYaw.first), resSplinesRoll.at(crSplineYaw.first) };
				}
			}
		}

		void Motion::recalcInterpolationData()
		{
			_recalcInterpolationData(*m_MotionData);
		}

		MA_REAL Motion::GetStateTimeStamp(UINT idState)
		{
			auto itrtState = m_States.find(idState);
			if (itrtState == m_States.end())
				return -1;

			return itrtState->second.get()->m_StateData.GetKey();
		}

		// -------------------------------------------------------------------------------------------------------------------

		MotionBuilder::MotionBuilder(const SceletonData& sceletonData)
			: m_SceletonData(sceletonData)
		{ 
			m_StateBasic.reset(new StateBasic(&m_SceletonData.m_BeginState, m_SceletonData));

			for (auto motionItrt = m_SceletonData.m_Motions.begin(); motionItrt != m_SceletonData.m_Motions.end(); motionItrt++)
				m_Motions.emplace(motionItrt->first, std::unique_ptr<Motion>(new Motion(motionItrt, this)));

			for (auto specStateItrt = m_SceletonData.m_SpecialStates.begin(); specStateItrt != m_SceletonData.m_SpecialStates.end(); specStateItrt++)
				m_SpecialStates.emplace(specStateItrt->first, std::unique_ptr<StateSpecial>(new StateSpecial(specStateItrt, m_SceletonData)));

			recalcID_Generators();
		}

		void MotionBuilder::recalcID_Generators()
		{
			// recalc states gen
			UINT nextID = 2;

			for (const auto& statePair : m_SpecialStates)
				if (nextID <= statePair.first)
					nextID = statePair.first + 1;

			for (const auto& motionPair : m_SceletonData.m_Motions)
				for (const auto& statePair : motionPair.second.m_States)
					if (nextID <= statePair.second.m_ID)
						nextID = statePair.second.m_ID + 1;

			m_StateIdGenerator = ID_Genearator(nextID);

			// recalc motion gen
			nextID = 1;

			for (const auto& motionPair : m_SceletonData.m_Motions)
				if (nextID <= motionPair.first)
					nextID = motionPair.first + 1;

			m_MotionIdGenerator = ID_Genearator(nextID);
		}

		std::unordered_map<ULONG, IState*> MotionBuilder::GetStates() const
		{
			std::unordered_map<ULONG, IState*> res;
			res.emplace(1, m_StateBasic.get());

			for (const auto& statePair : m_SpecialStates)
				res.emplace(statePair.first, statePair.second.get());

			for (const auto& motionPair : m_Motions)
				for (const auto& statePair : motionPair.second->GetStatesIDMap())
					res.emplace(statePair.first, statePair.second);

			return res;
		}

		std::unordered_map<ULONG, StateSpecial*> MotionBuilder::GetSpecialStates() const
		{
			std::unordered_map<ULONG, StateSpecial*> res;

			for (const auto& specStatePair : m_SpecialStates)
				res.emplace(specStatePair.first, specStatePair.second.get());

			return res;
		}

		std::unordered_map<ULONG, Motion*> MotionBuilder::GetMotions() const
		{
			std::unordered_map<ULONG, Motion*> res;

			for (const auto& motionPair : m_Motions)
				res.emplace(motionPair.first, motionPair.second.get());

			return res;
		}

		StateSpecial* MotionBuilder::CreateSpecialState(ULONG idBaseState, const std::wstring& name)
		{
			const std::unordered_map<ULONG, IState*>& states = GetStates();
			auto itrtBaseState = states.find(idBaseState);
			if (itrtBaseState == states.end())
				return NULL;

			const UINT idNewState = m_StateIdGenerator.GetID();
			StateData* pBaseStateData = (*itrtBaseState).second->GetStateDataRef();

			auto res = m_SceletonData.m_SpecialStates.emplace(idNewState, StateData(idNewState, name, pBaseStateData->m_GlobalPoseElements, pBaseStateData->m_LocalPoseElements));
			if (!res.second)
				return NULL;

			auto resCreateNewState = m_SpecialStates.emplace(idNewState, std::unique_ptr<StateSpecial>(new StateSpecial(res.first, m_SceletonData)));
			assert(resCreateNewState.second);

			return resCreateNewState.first->second.get();
		}

		void MotionBuilder::RemoveSpecialState(ULONG idState)
		{
			if (m_SpecialStates.find(idState) == m_SpecialStates.end())
				return;

			m_StateIdGenerator.RemoveID(idState);

			m_SpecialStates.erase(idState);
			m_SceletonData.m_SpecialStates.erase(idState);
		}

		Motion* MotionBuilder::CreateMotion(const std::wstring& name, ULONG idRelativeConnect, ULONG idBaseState, const std::wstring& baseStateName)
		{
			const std::unordered_map<ULONG, IState*>& states = GetStates();
			auto itrtBaseState = states.find(idBaseState);
			if (itrtBaseState == states.end())
				return NULL;

			if (m_SceletonData.m_Connections.find(idRelativeConnect) == m_SceletonData.m_Connections.end())
				return NULL;

			const UINT idNewMotion = m_MotionIdGenerator.GetID();
			StateData* pBaseStateData = (*itrtBaseState).second->GetStateDataRef();

			auto res = m_SceletonData.m_Motions.emplace(idNewMotion, SceletonData::MotionData(idNewMotion, idRelativeConnect, name));
			if (!res.second)
				return NULL;

			const UINT idNewState = m_StateIdGenerator.GetID();
			auto resCreateNewState = res.first->second.m_States.emplace(MA_REAL(0), SceletonData::StateData(idNewState, baseStateName
				, pBaseStateData->m_GlobalPoseElements, pBaseStateData->m_LocalPoseElements));

			assert(resCreateNewState.second);

			auto resCreateMotion = m_Motions.emplace(idNewMotion, std::unique_ptr<Motion>(new Motion(res.first, this)));
			if (!resCreateMotion.second)
				return NULL;

			return resCreateMotion.first->second.get();
		}


		Motion* MotionBuilder::CreateMotionAsInterpolation(const std::wstring& name, ULONG idInterpMotion, ULONG idBaseState, float coeffInterp, ULONG idRelativeConnect)
		{
			// validation
			const std::unordered_map<ULONG, IState*>& states = GetStates();
			auto itrtBaseState = states.find(idBaseState);
			if (itrtBaseState == states.end())
				return NULL;

			if (m_SceletonData.m_Connections.find(idRelativeConnect) == m_SceletonData.m_Connections.end())
				return NULL;

			auto itrtInterpMot = m_Motions.find(idInterpMotion);
			if (itrtInterpMot == m_Motions.end())
				return NULL;

			// create new data for motion
			const UINT idNewMotion = m_MotionIdGenerator.GetID();
			auto newMotionItrt = m_SceletonData.m_Motions.emplace(idNewMotion, SceletonData::MotionData(idNewMotion, idRelativeConnect, name));
			if (!newMotionItrt.second)
				return NULL;

			// create new states
			IState* pBasicState = itrtBaseState->second;	
			Motion* pInterpMotion = itrtInterpMot->second.get();

			SceletonData::MotionData& newMotionData = newMotionItrt.first->second;
			const std::unordered_set<StateMotion*>& statesMotion = pInterpMotion->GetStates();
			std::unordered_map<UINT, SceletonData::TypeGlobalCS> newGlobalCS;

			for (StateMotion* pCurrentState : statesMotion)
			{
				const LocalPositions& motionLocalPositions = pCurrentState->GetLocalPositions();
				const LocalPositions& baseLocalPositions = pBasicState->GetLocalPositions();

				const LocalPositions& newLocalPoses = IntermediateState(baseLocalPositions, motionLocalPositions, coeffInterp);

				const UINT idNewState = m_StateIdGenerator.GetID();
				auto resCreateNewState = newMotionData.m_States.emplace(pCurrentState->GetStateDataPtr().GetKey(), SceletonData::StateData(idNewState, pCurrentState->Name()
					, pCurrentState->GetGlobalPositions(), newLocalPoses));

				assert(resCreateNewState.second);

				// calculate global cs
				const auto& currentStateConnects = pCurrentState->GetConnections();
				auto itrtMotRelConn = currentStateConnects.find(idRelativeConnect);
				assert(itrtMotRelConn != currentStateConnects.end());

				const auto& basicStateConnects = pBasicState->GetConnections();
				auto itrtBaseRelConn = basicStateConnects.find(idRelativeConnect);
				assert(itrtBaseRelConn != basicStateConnects.end());

				newGlobalCS.emplace(idNewState, LinearInterpolation(itrtBaseRelConn->second->GetWCS(), itrtMotRelConn->second->GetWCS(), coeffInterp));
			}

			// create Motion
			Motion* pNewMotion = new Motion(newMotionItrt.first, this);
			auto resCreateMotion = m_Motions.emplace(idNewMotion, std::unique_ptr<Motion>(pNewMotion));
			assert(resCreateMotion.second);

			// retransform wcs of states of the motion
			std::unordered_set<StateMotion*> newStates = pNewMotion->GetStates();
			
			for (StateMotion* pCurrentState : newStates)
			{
				const auto& currentStateConnection = pCurrentState->GetConnections();
				auto itrtConn = currentStateConnection.find(idRelativeConnect);
				assert(itrtConn != currentStateConnection.end());

				Connection* pCurrentConnect = itrtConn->second;
				pCurrentConnect->Retransform(newGlobalCS[pCurrentState->ID()]);
			}
			
			return pNewMotion;
		}


		void MotionBuilder::RemoveMotion(ULONG idMotion)
		{
			if (m_Motions.find(idMotion) == m_Motions.end())
				return;

			m_MotionIdGenerator.RemoveID(idMotion);

			m_Motions.erase(idMotion);
			m_SceletonData.m_Motions.erase(idMotion);
		}
	}
}

