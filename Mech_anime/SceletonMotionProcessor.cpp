#include "stdafx.h"

#define TIXML_USE_STL

#include "tinystr.h"
#include "tinyxml.h"

#include "SceletonMotionProcessor.h"
#include "SceletonMethods.h"


namespace mech_a {
	namespace sceleton {

		// =================================== M O V E sceleton ================================================================================================

		LocalPositions SceletonMotionProcessor::intermediateState(MotionsMapPtr motA, MotionsMapPtr motB, MA_REAL relationAtoB)
		{
			LocalPositions posesA;
			InterpolationState(*motA, m_TimeStamp, m_SceletonData.m_Connections, posesA);

			LocalPositions posesB;
			InterpolationState(*motB, m_TimeStamp, m_SceletonData.m_Connections, posesB);

			return IntermediateState(posesA, posesB, relationAtoB);
		}


		LocalPositions SceletonMotionProcessor::calculatePositions(const AltitudeDiffChars& altitudeSettings)
		{
			if (!altitudeSettings.m_MotionsChars.size())
				throw std::runtime_error("Empty container motionsChars");

			std::map<MA_REAL, std::map<MA_REAL, MotionCharacteristics>>::const_iterator itrtLeftDiffLegs = altitudeSettings.m_MotionsChars.lower_bound(m_TimeStamp);
			
			if (itrtLeftDiffLegs == altitudeSettings.m_MotionsChars.end() ||
				(itrtLeftDiffLegs != altitudeSettings.m_MotionsChars.begin() && !Compare(itrtLeftDiffLegs->first, m_TimeStamp))
				)
				itrtLeftDiffLegs--;

			std::map<MA_REAL, MotionCharacteristics>::const_iterator itrtLowerMotion;
			std::map<MA_REAL, MotionCharacteristics>::const_iterator itrtUpperMotion;
			
			BoundaryResult boundRes = GetBoundary(itrtLeftDiffLegs->second, m_DiffAltitudeLegs, itrtLowerMotion, itrtUpperMotion);
			if (boundRes == BR_EmptyContainer)
				throw std::runtime_error("Empty motion container");

			std::map<MA_REAL, MotionCharacteristics>::const_iterator itrtMotion;

			if (boundRes == BoundaryResult::BR_OK)
			{
				if (itrtLowerMotion == itrtUpperMotion)
					itrtMotion = itrtLowerMotion;
				else
				{
					MA_REAL dAltLegs = itrtUpperMotion->first - itrtLowerMotion->first;
					MA_REAL relationAtoB = (m_DiffAltitudeLegs - itrtLowerMotion->first) / dAltLegs;

					return intermediateState(itrtLowerMotion->second.m_MotionPtr, itrtUpperMotion->second.m_MotionPtr, relationAtoB);
				}
			}
			else if (boundRes == BoundaryResult::BR_OutOfRangeLeft)
				itrtMotion = itrtUpperMotion;
			else
				itrtMotion = itrtLowerMotion;

			LocalPositions localPoses;
			InterpolationState(*itrtMotion->second.m_MotionPtr, m_TimeStamp, m_SceletonData.m_Connections, localPoses);
			return localPoses;
		}

		LocalPositions SceletonMotionProcessor::intermediateState(const AltitudeDiffChars& charsA, const AltitudeDiffChars& charsB, MA_REAL relationAtoB)
		{
		 	const LocalPositions& lowerPoses = calculatePositions(charsA);
			const LocalPositions& upperPoses = calculatePositions(charsB);

			return IntermediateState(lowerPoses, upperPoses, relationAtoB);
		}


		LocalPositions SceletonMotionProcessor::calculatePositions(const AngularSpeedSettings& speedSettings, MA_REAL alphaVertical)
		{
			std::map<MA_REAL, AltitudeDiffChars>::const_iterator itrtAltLower;
			std::map<MA_REAL, AltitudeDiffChars>::const_iterator itrtAltUpper;

			BoundaryResult boundRes = GetBoundary(speedSettings.m_AltitudeDiffSettings, alphaVertical, itrtAltLower, itrtAltUpper);
			if (boundRes == BoundaryResult::BR_EmptyContainer)
				throw std::runtime_error("Empty container altitudeDiffSettings");

			std::map<MA_REAL, AltitudeDiffChars>::const_iterator itrtSpeedSettings;

			if (boundRes == BoundaryResult::BR_OK)
			{
				if (itrtAltLower == itrtAltUpper)
					itrtSpeedSettings = itrtAltLower;
				else
				{
					MA_REAL dAlt = itrtAltUpper->first - itrtAltLower->first;
					MA_REAL relationAtoB = (alphaVertical - itrtAltLower->first) / dAlt;
					
					return intermediateState(itrtAltLower->second, itrtAltUpper->second, relationAtoB);
				}
			}
			else if (boundRes == BoundaryResult::BR_OutOfRangeLeft)
				itrtSpeedSettings = itrtAltUpper;
			else
				itrtSpeedSettings = itrtAltLower;

			return calculatePositions(itrtSpeedSettings->second);
		}

		LocalPositions SceletonMotionProcessor::intermediateState(const AngularSpeedSettings& charsA, const AngularSpeedSettings& charsB, MA_REAL relationAtoB, MA_REAL alphaVertical)
		{
			const LocalPositions& lowerLPs = calculatePositions(charsA, alphaVertical);
			const LocalPositions& upperLPs = calculatePositions(charsB, alphaVertical);
			return IntermediateState(lowerLPs, upperLPs, relationAtoB);
		}


		LocalPositions SceletonMotionProcessor::calculatePositions(const LegsSettings& legSettings, MA_REAL alphaVertical)
		{
			std::map<MA_REAL, AngularSpeedSettings>::const_iterator itrtAngSpeedLower;
			std::map<MA_REAL, AngularSpeedSettings>::const_iterator itrtAngSpeedUpper;

			BoundaryResult boundRes = GetBoundary(legSettings.m_AngularSpeeds, m_AngularVelocity, itrtAngSpeedLower, itrtAngSpeedUpper);
			if (boundRes == BoundaryResult::BR_EmptyContainer)
				throw std::runtime_error("Empty container angular speeds");

			std::map<MA_REAL, AngularSpeedSettings>::const_iterator itrtAngSpeed;

			if (boundRes == BoundaryResult::BR_OK)
			{
				if (itrtAngSpeedUpper == itrtAngSpeedLower)
					itrtAngSpeed = itrtAngSpeedLower;
				else
				{
					MA_REAL dAngSpeed = itrtAngSpeedUpper->first - itrtAngSpeedLower->first;
					MA_REAL relationAtoB = (m_AngularVelocity - itrtAngSpeedLower->first) / dAngSpeed;

					return intermediateState(itrtAngSpeedLower->second, itrtAngSpeedUpper->second, relationAtoB, alphaVertical);
				}
			}
			else if (boundRes == BoundaryResult::BR_OutOfRangeLeft)
				itrtAngSpeed = itrtAngSpeedUpper;
			else
				itrtAngSpeed = itrtAngSpeedLower;

			return calculatePositions(itrtAngSpeed->second, alphaVertical);
		}

		LocalPositions SceletonMotionProcessor::intermediateState(const LegsSettings& lowerLegSettings, const LegsSettings& upperLegSettings, MA_REAL relationAtoB, MA_REAL alphaVertical)
		{
			const LocalPositions& lowerLocalPoses = calculatePositions(lowerLegSettings, alphaVertical);
			const LocalPositions& upperLocalPoses = calculatePositions(upperLegSettings, alphaVertical);

			return IntermediateState(lowerLocalPoses, upperLocalPoses, relationAtoB);
		}


		LocalPositions SceletonMotionProcessor::move(const SpeedSettings& speedSettings, MA_REAL alphaVertical, MA_REAL moveTime, MA_REAL dT)
		{
			MA_REAL dT_Fact = dT / moveTime;
			dT_Fact *= m_Velocity / speedSettings.m_Speed;

			m_TimeStamp += dT_Fact;

			if (m_TimeStamp > 1)
			{
				m_TimeStamp -= 1;
				m_LeftLeg = !m_LeftLeg;
			}

			const LegsSettings& legSettings = m_LeftLeg ? speedSettings.m_LeftLegSpeeds : speedSettings.m_RightLegSpeeds;
			return calculatePositions(legSettings, alphaVertical);
		}

		LocalPositions SceletonMotionProcessor::move(const SpeedSettings& lowerSpStt, const SpeedSettings& upperSpStt, MA_REAL alphaVertical, MA_REAL moveTime, MA_REAL dT)
		{
			MA_REAL dT_Fact = dT / moveTime;
			m_TimeStamp += dT_Fact;

			if (m_TimeStamp > 1)
			{
				m_TimeStamp -= 1;
				m_LeftLeg = !m_LeftLeg;
			}

			const LegsSettings& lowerLegSettings = m_LeftLeg ? lowerSpStt.m_LeftLegSpeeds : lowerSpStt.m_RightLegSpeeds;
			const LegsSettings& upperLegSettings = m_LeftLeg ? upperSpStt.m_LeftLegSpeeds : upperSpStt.m_RightLegSpeeds;

			MA_REAL dV = upperSpStt.m_Speed - lowerSpStt.m_Speed;
			MA_REAL relationAtoB = (m_Velocity - lowerSpStt.m_Speed) / dV;

			return intermediateState(lowerLegSettings, upperLegSettings, relationAtoB, alphaVertical);
		}


		LocalPositions SceletonMotionProcessor::moveForward(MA_REAL alphaVertical, MA_REAL dT)
		{
			assert(m_Velocity > 0);

			const TacticCharacterSettings& tacticSettings = m_MotionProcessorSettings.m_SettingsByTacticCharacter.at(m_TacticCharacter);			
			const std::map<MA_REAL, SpeedSettings>& forwardSpeeds = tacticSettings.m_ForwardSpeedSettings;

			if (!forwardSpeeds.size())
				throw std::runtime_error("Invalid TacticCharacterSettings !!!");

			auto itrtSpeedSettingsRight = forwardSpeeds.lower_bound(m_Velocity);
			if (itrtSpeedSettingsRight == forwardSpeeds.end())
			{
				return move(std::prev(itrtSpeedSettingsRight)->second, alphaVertical, m_MotionProcessorSettings.m_MoveForvardTime, dT);
			}

			if (Compare(itrtSpeedSettingsRight->first, m_Velocity, MA_REAL(0.01)))
			{
				return move(itrtSpeedSettingsRight->second, alphaVertical, m_MotionProcessorSettings.m_MoveForvardTime, dT);
			}

			if (itrtSpeedSettingsRight == forwardSpeeds.begin())
			{
				return move(itrtSpeedSettingsRight->second, alphaVertical, m_MotionProcessorSettings.m_MoveForvardTime, dT);
			}
			
			return move(std::prev(itrtSpeedSettingsRight)->second, itrtSpeedSettingsRight->second, alphaVertical, m_MotionProcessorSettings.m_MoveForvardTime, dT);
		}


		LocalPositions SceletonMotionProcessor::moveBack(MA_REAL alphaVertical, MA_REAL dT)
		{
			assert(m_Velocity < 0);

			const TacticCharacterSettings& tacticSettings = m_MotionProcessorSettings.m_SettingsByTacticCharacter.at(m_TacticCharacter);
			const std::map<MA_REAL, SpeedSettings>& backSpeeds = tacticSettings.m_BackSpeedSettings;

			if (!backSpeeds.size())
				throw std::runtime_error("Invalid TacticCharacterSettings !!!");

			auto itrtSpeedSettingsRight = backSpeeds.lower_bound(m_Velocity);

			if (itrtSpeedSettingsRight == backSpeeds.end())
			{
				return move(std::prev(itrtSpeedSettingsRight)->second, alphaVertical, m_MotionProcessorSettings.m_MoveBackTime, dT);
			}

			if (Compare(itrtSpeedSettingsRight->first, m_Velocity, MA_REAL(0.01)))
			{
				return move(itrtSpeedSettingsRight->second, alphaVertical, m_MotionProcessorSettings.m_MoveBackTime, dT);
			}

			if (itrtSpeedSettingsRight == backSpeeds.begin())
			{
				return move(itrtSpeedSettingsRight->second, alphaVertical, m_MotionProcessorSettings.m_MoveBackTime, dT);
			}

			return move(std::prev(itrtSpeedSettingsRight)->second, itrtSpeedSettingsRight->second, alphaVertical, m_MotionProcessorSettings.m_MoveBackTime, dT);
		}


		LocalPositions SceletonMotionProcessor::turning(MA_REAL alphaVertical, MA_REAL dT, bool toLeft)
		{
			const TacticCharacterSettings& tacticSettings = m_MotionProcessorSettings.m_SettingsByTacticCharacter.at(m_TacticCharacter);

			MA_REAL absAngVelocity = fabs(m_AngularVelocity);	
			MA_REAL dT_Fact = dT / m_MotionProcessorSettings.m_MoveTurnTime;
	
			if (absAngVelocity < tacticSettings.m_MinTurnSpeedAbs)
				dT_Fact *= absAngVelocity / tacticSettings.m_MinTurnSpeedAbs;
			else if (absAngVelocity > tacticSettings.m_MaxTurnSpeedAbs)
				dT_Fact *= absAngVelocity / tacticSettings.m_MaxTurnSpeedAbs;

			m_TimeStamp += dT_Fact;

			if (m_TimeStamp > 1)
			{
				m_TimeStamp -= 1;
				m_LeftLeg = !m_LeftLeg;
			}

			if (toLeft)
			{
				const LegsSettings& currentLegSettings = m_LeftLeg ? tacticSettings.m_TurningFromLeftLegSettingsLeft : tacticSettings.m_TurningFromRightLegSettingsLeft;
				return calculatePositions(currentLegSettings, alphaVertical);
			}
			else
			{
				const LegsSettings& currentLegSettings = m_LeftLeg ? tacticSettings.m_TurningFromLeftLegSettingsRight : tacticSettings.m_TurningFromRightLegSettingsRight;
				return calculatePositions(currentLegSettings, alphaVertical);
			}
		}

		void SceletonMotionProcessor::selectStatePosesItrt()
		{
			MA_REAL currentDiffAltitude = 0;
			if (m_pExternalInformer)
				currentDiffAltitude = m_pExternalInformer->GetDiffAltitude();

			const TacticCharacterSettings& tacticSettings = m_MotionProcessorSettings.m_SettingsByTacticCharacter.at(m_TacticCharacter);
			
			std::map<MA_REAL, LocalPositions>::const_iterator itrtLowerPoses;
			std::map<MA_REAL, LocalPositions>::const_iterator itrtUpperPoses;

			BoundaryResult boundRes = GetBoundary(tacticSettings.m_StandingPosesByDiffAltitudeLegs, currentDiffAltitude, itrtLowerPoses, itrtUpperPoses);
			if (boundRes == BoundaryResult::BR_EmptyContainer)
				throw std::runtime_error("Empty container Standing Poses By Diff Altitude Legs");

			if (boundRes == BR_OK)
			{
				if (itrtLowerPoses == itrtUpperPoses)
					m_SatatePosItrt = itrtLowerPoses;
				else
				{
					if ((itrtUpperPoses->first - currentDiffAltitude) <= (currentDiffAltitude - itrtLowerPoses->first))
						m_SatatePosItrt = itrtUpperPoses;
					else
						m_SatatePosItrt = itrtLowerPoses;
				}
			}
			else if (boundRes == BoundaryResult::BR_OutOfRangeLeft)
				m_SatatePosItrt = itrtUpperPoses;
			else
				m_SatatePosItrt = itrtLowerPoses;
		}


		std::unordered_map<ULONG, EulerAnglesCS::RotatorType> calculateAngularSpeeds(const std::unordered_map<ULONG, EulerAnglesCS>& dPoses, MA_REAL dT)
		{
			std::unordered_map<ULONG, EulerAnglesCS::RotatorType> resAngSpeeds;

			for (const auto& dPosesPair : dPoses)
			{
				EulerAngles rotAngs;
				rotAngs.m_roll = fabs(dPosesPair.second.m_Angles.m_roll) / dT;
				rotAngs.m_pitch = fabs(dPosesPair.second.m_Angles.m_pitch) / dT;
				rotAngs.m_yaw = fabs(dPosesPair.second.m_Angles.m_yaw) / dT;

				resAngSpeeds.emplace(dPosesPair.first, rotAngs);
			}

			return resAngSpeeds;
		}

		std::unordered_map<ULONG, QuaternionCS::RotatorType> calculateAngularSpeeds(const std::unordered_map<ULONG, QuaternionCS>& dPoses, MA_REAL dT)
		{
			std::unordered_map<ULONG, QuaternionCS::RotatorType> resAngSpeeds;

			for (const auto& dPosesPair : dPoses)
			{
				MA_REAL angle; Vector axisRotate;
				dPosesPair.second.m_Quaternion.GetAxisAngl(angle, axisRotate);

				const MA_REAL angSpeedScalar = angle / dT;

				Quaternion rotAngs;

				rotAngs.m_X = axisRotate.X * angSpeedScalar;
				rotAngs.m_Y = axisRotate.Y * angSpeedScalar;
				rotAngs.m_Z = axisRotate.Z * angSpeedScalar;

				resAngSpeeds.emplace(dPosesPair.first, rotAngs);
			}

			return resAngSpeeds;
		}

		MA_REAL calculateMoveProportion(const EulerAnglesCS::RotatorType& reqAngSpeed, const EulerAnglesCS::RotatorType& limitAngSpeed)
		{
			MA_REAL moveProp = 1;

			if (limitAngSpeed.m_pitch < reqAngSpeed.m_pitch)
			{
				MA_REAL mP = limitAngSpeed.m_pitch / reqAngSpeed.m_pitch;
				if (mP < moveProp)
					moveProp = mP;
			}

			if (limitAngSpeed.m_roll < reqAngSpeed.m_roll)
			{
				MA_REAL mP = limitAngSpeed.m_roll / reqAngSpeed.m_roll;
				if (mP < moveProp)
					moveProp = mP;
			}

			if (limitAngSpeed.m_yaw < reqAngSpeed.m_yaw)
			{
				MA_REAL mP = limitAngSpeed.m_yaw / reqAngSpeed.m_yaw;
				if (mP < moveProp)
					moveProp = mP;
			}

			return moveProp;
		}

		MA_REAL calculateMoveProportion(const QuaternionCS::RotatorType& reqAngSpeed, const QuaternionCS::RotatorType& limitAngSpeed)
		{
			MA_REAL moveProp = 1;

			if (limitAngSpeed.m_X < reqAngSpeed.m_X)
			{
				MA_REAL mP = fabs(limitAngSpeed.m_X / reqAngSpeed.m_X);
				if (mP < moveProp)
					moveProp = mP;
			}

			if (limitAngSpeed.m_Y < reqAngSpeed.m_Y)
			{
				MA_REAL mP = fabs(limitAngSpeed.m_Y / reqAngSpeed.m_Y);
				if (mP < moveProp)
					moveProp = mP;
			}

			if (limitAngSpeed.m_Z < reqAngSpeed.m_Z)
			{
				MA_REAL mP = fabs(limitAngSpeed.m_Z / reqAngSpeed.m_Z);
				if (mP < moveProp)
					moveProp = mP;
			}

			return moveProp;
		}

		QuaternionCS addPose(const QuaternionCS& pose, const QuaternionCS& dPose, MA_REAL coefficient)
		{
			QuaternionCS res;
			res.m_Pos = pose.m_Pos + dPose.m_Pos * coefficient;

			Quaternion nextQuat = dPose.m_Quaternion * pose.m_Quaternion;
			nextQuat.Normalize();

			res.m_Quaternion = QuaternionSlerp(pose.m_Quaternion, nextQuat, coefficient);
			return res;
		}

		EulerAnglesCS addPose(const EulerAnglesCS& pose, const EulerAnglesCS& dPose, MA_REAL coefficient)
		{
			// 			EulerAnglesCS res;
			// 			res.m_Pos = pose.m_Pos + dPose.m_Pos * coefficient;
			// 
			// 			res.m_Angles = res.m_Angles + dPose.m_Angles * coefficient;
			// 			return res;

			QuaternionCS qtPose;
			QuaternionCS qtDPose;

			FromCS_ToCS(pose, qtPose);
			FromCS_ToCS(dPose, qtDPose);

			QuaternionCS res = addPose(qtPose, qtDPose, coefficient);

			EulerAnglesCS eRes;
			FromCS_ToCS(res, eRes);

			return eRes;
		}

		void SceletonMotionProcessor::setLocalPositions(const LocalPositions& localPoses, MA_REAL dT)
		{
			// calculate diffs poses 
			const LocalPositions& dPoses = DiffStates(m_ExecutableState.m_LocalPoseElements, localPoses);

			// calculate required angular speeds
			const std::unordered_map<ULONG, SceletonData::TypeLocalCS::RotatorType>& requiredAngularSpeeds = calculateAngularSpeeds(dPoses, dT);

			// calculate move proportion
			MA_REAL moveProportion = 1;

			for (const auto& reqAngSpeedPair : requiredAngularSpeeds)
			{
				auto itrtKinematicChars = m_MotionProcessorSettings.m_LimitKinematicCharaters.find(reqAngSpeedPair.first);
				if (itrtKinematicChars == m_MotionProcessorSettings.m_LimitKinematicCharaters.end())
					throw std::runtime_error("Error Limit Kinematic Charaters");

				MA_REAL mp = calculateMoveProportion(reqAngSpeedPair.second, itrtKinematicChars->second);
				if (mp < moveProportion)
					moveProportion = mp;
			}

			moveProportion *= m_MotionProcessorSettings.m_CoeffLimitCharacters;
			if (moveProportion > 1)
				moveProportion = 1;

			// calculate new position
			for (auto& localPosPair : m_ExecutableState.m_LocalPoseElements)
			{
				LocalPositions::const_iterator itrtDPoses = dPoses.find(localPosPair.first);
				if (itrtDPoses == dPoses.end())
					throw std::runtime_error("Error dPoses");

				localPosPair.second = addPose(localPosPair.second, itrtDPoses->second, moveProportion);
			}
		}


		MoveResult SceletonMotionProcessor::Move(MA_REAL acceleration, MA_REAL angularAcceleration, MA_REAL alphaVertical, MA_REAL dT)
		{
			MA_REAL dV = (acceleration * dT) / 2;
			m_Velocity = m_NextVelocity + dV;
			m_NextVelocity = m_Velocity + dV;

			MA_REAL dV_Ang = (angularAcceleration * dT) / 2;
			m_AngularVelocity = m_NextAngularVelocity + dV_Ang;
			m_NextAngularVelocity = m_AngularVelocity + dV_Ang;

			LocalPositions resLocalPositions;
			bool forceDirect = !Compare(acceleration, 0, MA_REAL(0.01));
			bool forceTurn = !Compare(angularAcceleration, 0, MA_REAL(0.01));
			bool stayHere = Compare(m_Velocity, 0, MA_REAL(0.01));
			bool stopTurn = Compare(m_AngularVelocity, 0, MA_REAL(0.01));

			switch (m_MotionProcessorState)
			{
			case MotionProcessorState::MPS_MoveForward:
				{
					if (stayHere && !forceDirect)
					{
						if (!stopTurn)
						{
							if (m_AngularVelocity > 0)
							{
								m_MotionProcessorState = MotionProcessorState::MPS_TurnToRight;
								m_TimeStamp = 0;
								m_LeftLeg = false;
								resLocalPositions = turning(alphaVertical, dT, false);
							}
							else
							{
								m_MotionProcessorState = MotionProcessorState::MPS_TurnToLeft;
								m_TimeStamp = 0;
								m_LeftLeg = true;
								resLocalPositions = turning(alphaVertical, dT, true);
							}
						}
						else
						{
							m_TimeStamp = 0;
							m_MotionProcessorState = MotionProcessorState::MPS_InToStay;
							selectStatePosesItrt();
							resLocalPositions = m_SatatePosItrt->second;
						}
					}
					else
					{
						if (m_Velocity < 0)
						{
							m_MotionProcessorState = MotionProcessorState::MPS_MoveBack;
							m_TimeStamp = 1 - m_TimeStamp;
							resLocalPositions = moveBack(alphaVertical, dT);
						}
						else
							resLocalPositions = moveForward(alphaVertical, dT);
					}
				}
				break;
			case MotionProcessorState::MPS_MoveBack:
				{
					if (stayHere && !forceDirect)
					{
						if (!stopTurn)
						{
							if (m_AngularVelocity > 0)
							{
								m_MotionProcessorState = MotionProcessorState::MPS_TurnToRight;
								m_TimeStamp = 0;
								m_LeftLeg = false;
								resLocalPositions = turning(alphaVertical, dT, false);
							}
							else
							{
								m_MotionProcessorState = MotionProcessorState::MPS_TurnToLeft;
								m_TimeStamp = 0;
								m_LeftLeg = true;
								resLocalPositions = turning(alphaVertical, dT, true);
							}
						}
						else
						{
							m_TimeStamp = 0;
							m_MotionProcessorState = MotionProcessorState::MPS_InToStay;
							selectStatePosesItrt();
							resLocalPositions = m_SatatePosItrt->second;
						}
					}
					else
					{
						if (m_Velocity > 0)
						{
							m_MotionProcessorState = MotionProcessorState::MPS_MoveForward;
							m_TimeStamp = 1 - m_TimeStamp;
							resLocalPositions = moveForward(alphaVertical, dT);
						}
						else
							resLocalPositions = moveBack(alphaVertical, dT);
					}
				}
				break;
			case MotionProcessorState::MPS_TurnToLeft:
				{
					if (!stayHere)
					{
						// begin move
						m_TimeStamp = MA_REAL(0.5);

						if (m_Velocity > 0)
						{
							m_MotionProcessorState = MotionProcessorState::MPS_MoveForward;
							resLocalPositions = moveForward(alphaVertical, dT);
						}
						else
						{
							m_MotionProcessorState = MotionProcessorState::MPS_MoveBack;
							resLocalPositions = moveBack(alphaVertical, dT);
						}
					}
					else
					{
						if (stopTurn && !forceTurn)
						{
							// stop 
							m_TimeStamp = 0;
							m_MotionProcessorState = MotionProcessorState::MPS_InToStay;
							selectStatePosesItrt();
							resLocalPositions = m_SatatePosItrt->second;
						}
						else if (m_AngularVelocity > 0)
						{
							// change turn direction
							m_TimeStamp = 1 - m_TimeStamp;
							m_MotionProcessorState = MotionProcessorState::MPS_TurnToRight;
							resLocalPositions = turning(alphaVertical, dT, false);
						}
						else
							resLocalPositions = turning(alphaVertical, dT, true);
					}
				}
				break;
			case MotionProcessorState::MPS_TurnToRight:
				{
					if (!stayHere)
					{
						// begin move
						m_TimeStamp = MA_REAL(0.5);

						if (m_Velocity > 0)
						{
							m_MotionProcessorState = MotionProcessorState::MPS_MoveForward;
							resLocalPositions = moveForward(alphaVertical, dT);
						}
						else
						{
							m_MotionProcessorState = MotionProcessorState::MPS_MoveBack;
							resLocalPositions = moveBack(alphaVertical, dT);
						}
					}
					else
					{
						if (stopTurn && !forceTurn)
						{
							// stop 
							m_TimeStamp = 0;
							m_MotionProcessorState = MotionProcessorState::MPS_InToStay;
							selectStatePosesItrt();
							resLocalPositions = m_SatatePosItrt->second;
						}
						else if (m_AngularVelocity < 0)
						{
							// change turn direction
							m_TimeStamp = 1 - m_TimeStamp;
							m_MotionProcessorState = MotionProcessorState::MPS_TurnToLeft;
							resLocalPositions = turning(alphaVertical, dT, true);
						}
						else
							resLocalPositions = turning(alphaVertical, dT, false);
					}
				}
				break;
			case MotionProcessorState::MPS_InToStay:
			case MotionProcessorState::MPS_Stay:
				{
					if (stayHere)
					{
						if (stopTurn)
						{
							// stay again
							resLocalPositions = m_SatatePosItrt->second;
						}
						else
						{
							// begin turning
							m_TimeStamp = 0;
							bool toLeft = true;

							if (m_AngularVelocity > 0)
							{
								m_LeftLeg = false;
								toLeft = false;
								m_MotionProcessorState = MotionProcessorState::MPS_TurnToRight;
							}
							else
							{
								m_LeftLeg = true;
								m_MotionProcessorState = MotionProcessorState::MPS_TurnToLeft;
							}

							resLocalPositions = turning(alphaVertical, dT, toLeft);
						}
					}
					else
					{
						// begin moving
						m_TimeStamp = MA_REAL(0.5);

						if (m_Velocity > 0)
						{
							m_MotionProcessorState = MotionProcessorState::MPS_MoveForward;
							resLocalPositions = moveForward(alphaVertical, dT);
						}
						else
						{
							m_MotionProcessorState = MotionProcessorState::MPS_MoveBack;
							resLocalPositions = moveBack(alphaVertical, dT);
						}
					}
				}
				break;
			}

			assert(m_pLeftToe && m_pRightToe && m_pTorso);

			// calc begin diffs between legs
			Connection* pForwardConnect = m_LeftLeg ? (m_TimeStamp > MA_REAL(0.5) ? m_pLeftToe : m_pRightToe ) : (m_TimeStamp > MA_REAL(0.5) ? m_pRightToe : m_pLeftToe);

			MA_REAL beginingX_diffs = fabs(pForwardConnect->GetWCS().Pos().X - m_pTorso->GetWCS().Pos().X);
			MA_REAL beginZDiff = fabs(m_pTorso->GetWCS().Pos().Z - (m_LeftLeg ? m_pRightToe : m_pLeftToe)->GetWCS().Pos().Z);

			setLocalPositions(resLocalPositions, dT);

			SceletonData::TypeGlobalCS globCs;
			FromCS_ToCS(m_pTorso->GetLCS(), globCs);
			m_pTorso->Retransform(globCs);

			// recalculate m_DiffAltitudeLegs
			m_DiffAltitudeLegs = m_pRightToe->GetWCS().Pos().Z - m_pLeftToe->GetWCS().Pos().Z;

			// calculate Z move
			MA_REAL zDiff = fabs(m_pTorso->GetWCS().Pos().Z - (m_LeftLeg ? m_pRightToe : m_pLeftToe)->GetWCS().Pos().Z);

			MoveResult res;
			res.m_dPos.Z = zDiff - beginZDiff;

			if (m_MotionProcessorState == MotionProcessorState::MPS_InToStay)
			{
				// calculate X move for stand move
				MA_REAL nextX_diffs = fabs(pForwardConnect->GetWCS().Pos().X - m_pTorso->GetWCS().Pos().X);
				res.m_dPos.X = fabs(beginingX_diffs - nextX_diffs);
			}
			else
			{
				res.m_dRotation = m_AngularVelocity * dT;
				res.m_dPos.X = m_Velocity * dT;
			}

			return res;
		}

		// =============================================== LOAD settings ====================================================================================


		SceletonMotionProcessor::SceletonMotionProcessor(IN std::stringstream& sceletonData, const std::string& settings_XML)
			: m_pExternalInformer(NULL)
			, m_SceletonData(sceletonData)
			, m_pLeftToe(NULL)
			, m_pRightToe(NULL)
			, m_pTorso(NULL)
			, m_MotionProcessorState(MotionProcessorState::MPS_Stay)
			, m_LeftLeg(true)
			, m_TacticCharacter(TacticCharacter::TC_Normal)
			, m_DiffAltitudeLegs(0)
			, m_TimeStamp(0)
			, m_Velocity(0)
			, m_AngularVelocity(0)
			, m_NextVelocity(0)
			, m_NextAngularVelocity(0)
		{
			// create base elements
			m_ExecutableState = m_SceletonData.m_BeginState;
			LoadParts(m_SceletonData.m_Elements, m_SceletonData.m_Connections, m_ExecutableState, m_Parts);

			TiXmlDocument xml;
			if(!xml.LoadFile(settings_XML, TiXmlEncoding::TIXML_ENCODING_UTF8))
				throw std::runtime_error("settings_XML_frmt fail !!!");

			TiXmlElement* pTiRoot = xml.RootElement();
			if (!pTiRoot)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			// speeds
			TiXmlNode* pSpeedsNode = pTiRoot->FirstChild("speeds");
			if (!pSpeedsNode)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			std::unordered_map<UINT, MA_REAL> speeds;

			TiXmlElement* pTiElement = NULL;
			TiXmlNode* pTiNode = NULL;
			
			while ((pTiNode = pSpeedsNode->IterateChildren("speed", pTiNode)) && (pTiElement = pTiNode->ToElement()))
			{
				UINT id = 0;
				if (pTiElement->QueryUnsignedAttribute("id", &id) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				double vl;
				if (pTiElement->QueryDoubleAttribute("value", &vl) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				speeds.emplace(id, MA_REAL(vl));
			}

			// angular speeds
			TiXmlNode* pAngSpeedsNode = pTiRoot->FirstChild("angular_speeds");
			if (!pAngSpeedsNode)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			std::unordered_map<UINT, MA_REAL> ang_speeds;

			pTiElement = NULL; 
			pTiNode = NULL;

			while ((pTiNode = pAngSpeedsNode->IterateChildren("ang_speed", pTiNode)) && (pTiElement = pTiNode->ToElement()))
			{
				UINT id = 0;
				if (pTiElement->QueryUnsignedAttribute("id", &id) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				double vl;
				if (pTiElement->QueryDoubleAttribute("value", &vl) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				ang_speeds.emplace(id, MA_REAL(vl));
			}

			// altitudes
			TiXmlNode* pAltitudes = pTiRoot->FirstChild("altitudes");
			if (!pAltitudes)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			std::unordered_map<UINT, MA_REAL> altitudes;

			pTiElement = NULL;
			pTiNode = NULL;

			while ((pTiNode = pAltitudes->IterateChildren("altitude", pTiNode)) && (pTiElement = pTiNode->ToElement()))
			{
				UINT id = 0;
				if (pTiElement->QueryUnsignedAttribute("id", &id) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				double vl;
				if (pTiElement->QueryDoubleAttribute("value", &vl) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				altitudes.emplace(id, MA_REAL(vl));
			}

			// times
			TiXmlNode* pTimesNode = pTiRoot->FirstChild("times");
			if (!pTimesNode)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			// move_forward
			if (!(pTiNode = pTimesNode->FirstChild("move_forward")) || !(pTiElement = pTiNode->ToElement()))
				throw std::runtime_error("settings_XML_frmt fail !!!");

			double vl;
			if (pTiElement->QueryDoubleAttribute("time", &vl) != TIXML_SUCCESS)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			m_MotionProcessorSettings.m_MoveForvardTime = MA_REAL(vl);

			// move_back
			if (!(pTiNode = pTimesNode->FirstChild("move_back")) || !(pTiElement = pTiNode->ToElement()))
				throw std::runtime_error("settings_XML_frmt fail !!!");

			if (pTiElement->QueryDoubleAttribute("time", &vl) != TIXML_SUCCESS)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			m_MotionProcessorSettings.m_MoveBackTime = MA_REAL(vl);

			// move_turning
			if (!(pTiNode = pTimesNode->FirstChild("move_turning")) || !(pTiElement = pTiNode->ToElement()))
				throw std::runtime_error("settings_XML_frmt fail !!!");

			if (pTiElement->QueryDoubleAttribute("time", &vl) != TIXML_SUCCESS)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			m_MotionProcessorSettings.m_MoveTurnTime = MA_REAL(vl);

			// parts
			TiXmlNode* pPartsNode = pTiRoot->FirstChild("parts");
			if (!pPartsNode)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			// leftToe
			if (!(pTiNode = pPartsNode->FirstChild("leftToe")) || !(pTiElement = pTiNode->ToElement()))
				throw std::runtime_error("settings_XML_frmt fail !!!");

			UINT id;
			if (pTiElement->QueryUnsignedAttribute("id_Elem", &id) != TIXML_SUCCESS)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			auto itrtPart = m_Parts.find(id);
			if (itrtPart == m_Parts.end())
				throw std::runtime_error("settings_XML_frmt fail !!!");

			m_pLeftToe = dynamic_cast<Connection*>(itrtPart->second.get());
			if (!m_pLeftToe)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			// rightToe
			if (!(pTiNode = pPartsNode->FirstChild("rightToe")) || !(pTiElement = pTiNode->ToElement()))
				throw std::runtime_error("settings_XML_frmt fail !!!");

			if (pTiElement->QueryUnsignedAttribute("id_Elem", &id) != TIXML_SUCCESS)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			itrtPart = m_Parts.find(id);
			if (itrtPart == m_Parts.end())
				throw std::runtime_error("settings_XML_frmt fail !!!");

			m_pRightToe = dynamic_cast<Connection*>(itrtPart->second.get());
			if (!m_pRightToe)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			// torso
			if (!(pTiNode = pPartsNode->FirstChild("torso")) || !(pTiElement = pTiNode->ToElement()))
				throw std::runtime_error("settings_XML_frmt fail !!!");

			if (pTiElement->QueryUnsignedAttribute("id_Elem", &id) != TIXML_SUCCESS)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			itrtPart = m_Parts.find(id);
			if (itrtPart == m_Parts.end())
				throw std::runtime_error("settings_XML_frmt fail !!!");

			m_pTorso = dynamic_cast<Connection*>(itrtPart->second.get());
			if (!m_pTorso)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			// coeffLimitCharacters
			if (!(pTiNode = pTiRoot->FirstChild("coeffLimitCharacters")) || !(pTiElement = pTiNode->ToElement()))
				throw std::runtime_error("settings_XML_frmt fail !!!");

			if (pTiElement->QueryDoubleAttribute("value", &vl) != TIXML_SUCCESS)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			m_MotionProcessorSettings.m_CoeffLimitCharacters = MA_REAL(vl);

			// basic_states
			TiXmlNode* pBasicStatesNode = pTiRoot->FirstChild("basic_states");
			if (!pBasicStatesNode)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			pTiElement = NULL;
			pTiNode = NULL;

			while ((pTiNode = pBasicStatesNode->IterateChildren("basic_state", pTiNode)) && (pTiElement = pTiNode->ToElement()))
			{
				if (pTiElement->QueryUnsignedAttribute("id", &id) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				UINT tacticID;
				if (pTiElement->QueryUnsignedAttribute("tactic_id", &tacticID) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				StateData* pState = NULL;
				if (m_SceletonData.m_BeginState.m_ID == id)
					pState = &m_SceletonData.m_BeginState;
				else
				{
					auto itrtState = m_SceletonData.m_SpecialStates.find(id);
					if (itrtState != m_SceletonData.m_SpecialStates.end())
						pState = &itrtState->second;
					else
						throw std::runtime_error("settings_XML_frmt fail !!!");
				}

				MA_REAL diff = pState->m_GlobalPoseElements.at(m_pRightToe->ID()).Pos().Z - pState->m_GlobalPoseElements.at(m_pLeftToe->ID()).Pos().Z;

				auto& settingsByTacticCharacter = m_MotionProcessorSettings.m_SettingsByTacticCharacter[(TacticCharacter)tacticID];
				settingsByTacticCharacter.m_TacticCharacter = (TacticCharacter)tacticID;
				settingsByTacticCharacter.m_StandingPosesByDiffAltitudeLegs[diff] = pState->m_LocalPoseElements;
			}

			// stamps_measuring_diffs_count
			if(!(pTiNode = pTiRoot->FirstChild("stamps_measuring_diffs_count")))
				throw std::runtime_error("settings_XML_frmt fail !!!");

			TiXmlElement* pStamps_measuring_diffs_count = pTiNode->ToElement();
			if (!pStamps_measuring_diffs_count)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			UINT stamps_measuring_diffs_count = 0;
			if (pStamps_measuring_diffs_count->QueryUnsignedAttribute("count", &stamps_measuring_diffs_count) != TIXML_SUCCESS)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			std::unordered_map<ULONG, MA_REAL> motionTimeRelationCoeffs;
			MA_REAL stepStamp = MA_REAL(1.0 / stamps_measuring_diffs_count);

			// motions_forward
			TiXmlNode* pMotionsFrwrdNode = pTiRoot->FirstChild("motions_forward");
			if (!pMotionsFrwrdNode)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			pTiElement = NULL;
			pTiNode = NULL;

			while ((pTiNode = pMotionsFrwrdNode->IterateChildren("motion", pTiNode)) && (pTiElement = pTiNode->ToElement()))
			{
				if (pTiElement->QueryUnsignedAttribute("id", &id) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				motionTimeRelationCoeffs.emplace(id, m_MotionProcessorSettings.m_MoveForvardTime);

				UINT tacticID;
				if (pTiElement->QueryUnsignedAttribute("tactic_id", &tacticID) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				bool leftLeg;
				if (pTiElement->QueryBoolAttribute("leftLeg", &leftLeg) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				UINT speedID;
				if (pTiElement->QueryUnsignedAttribute("speed_id", &speedID) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				UINT angSpeedID;
				if (pTiElement->QueryUnsignedAttribute("ang_speed_id", &angSpeedID) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				UINT altitude_id;
				if (pTiElement->QueryUnsignedAttribute("altitude_id", &altitude_id) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				auto itrtMots = m_SceletonData.m_Motions.find(id);
				if (itrtMots == m_SceletonData.m_Motions.end())
					throw std::runtime_error("settings_XML_frmt fail !!!");

				MA_REAL speed = speeds.at(speedID);
				MA_REAL angSpeed = ang_speeds.at(angSpeedID);
				MA_REAL altitude = altitudes.at(altitude_id);

				auto& tacticCharacterSettings = m_MotionProcessorSettings.m_SettingsByTacticCharacter[(TacticCharacter)tacticID];
				tacticCharacterSettings.m_TacticCharacter = (TacticCharacter)tacticID;

				auto& speedSettings = tacticCharacterSettings.m_ForwardSpeedSettings[speed];
				speedSettings.m_ID_SpeedClass = speedID;
				speedSettings.m_Speed = speed;
				
				for (MA_REAL currentStamp = 0; currentStamp <= 1; currentStamp += stepStamp)
				{
					LocalPositions locals;
					InterpolationState(itrtMots->second, currentStamp, m_SceletonData.m_Connections, locals);

					UpdateLocalPositions(locals, std::pair<ULONG, SceletonData::TypeGlobalCS>(m_pTorso->ID(), SceletonData::TypeGlobalCS::Default()), m_Parts);
					MA_REAL diff = m_pRightToe->GetWCS().Pos().Z - m_pLeftToe->GetWCS().Pos().Z;

					MotionCharacteristics motChars = { itrtMots, m_MotionProcessorSettings.m_MoveForvardTime, speed };
					auto& legSpeeds = leftLeg ? speedSettings.m_LeftLegSpeeds : speedSettings.m_RightLegSpeeds;

					auto& angularSpeedSettings = legSpeeds.m_AngularSpeeds[angSpeed];
					angularSpeedSettings.m_ID_AngularSpeed = angSpeedID;
					angularSpeedSettings.m_AngularSpeed = angSpeed;
					
					auto& altitudeDiffChars = angularSpeedSettings.m_AltitudeDiffSettings[altitude];
					altitudeDiffChars.m_ID_ClassAltitude = altitude_id;
					altitudeDiffChars.m_Altitude = altitude;
					altitudeDiffChars.m_MotionsChars[currentStamp].emplace(diff, motChars);
				}
			}

			// motion back
			TiXmlNode* pMotionsBackNode = pTiRoot->FirstChild("motions_back");
			if (!pMotionsBackNode)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			pTiElement = NULL;
			pTiNode = NULL;

			while ((pTiNode = pMotionsBackNode->IterateChildren("motion", pTiNode)) && (pTiElement = pTiNode->ToElement()))
			{
				if (pTiElement->QueryUnsignedAttribute("id", &id) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				motionTimeRelationCoeffs.emplace(id, m_MotionProcessorSettings.m_MoveBackTime);

				UINT tacticID;
				if (pTiElement->QueryUnsignedAttribute("tactic_id", &tacticID) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				bool leftLeg;
				if (pTiElement->QueryBoolAttribute("leftLeg", &leftLeg) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				UINT speedID;
				if (pTiElement->QueryUnsignedAttribute("speed_id", &speedID) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				UINT angSpeedID;
				if (pTiElement->QueryUnsignedAttribute("ang_speed_id", &angSpeedID) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				UINT altitude_id;
				if (pTiElement->QueryUnsignedAttribute("altitude_id", &altitude_id) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				auto itrtMots = m_SceletonData.m_Motions.find(id);
				if (itrtMots == m_SceletonData.m_Motions.end())
					throw std::runtime_error("settings_XML_frmt fail !!!");

				MA_REAL speed = speeds.at(speedID);
				MA_REAL angSpeed = ang_speeds.at(angSpeedID);
				MA_REAL altitude = altitudes.at(altitude_id);

				auto& tacticCharacterSettings = m_MotionProcessorSettings.m_SettingsByTacticCharacter[(TacticCharacter)tacticID];
				tacticCharacterSettings.m_TacticCharacter = (TacticCharacter)tacticID;

				auto& speedSettings = tacticCharacterSettings.m_BackSpeedSettings[speed];
				speedSettings.m_ID_SpeedClass = speedID;
				speedSettings.m_Speed = speed;

				for (MA_REAL currentStamp = 0; currentStamp <= 1; currentStamp += stepStamp)
				{
					LocalPositions locals;
					InterpolationState(itrtMots->second, currentStamp, m_SceletonData.m_Connections, locals);

					UpdateLocalPositions(locals, std::pair<ULONG, SceletonData::TypeGlobalCS>(m_pTorso->ID(), SceletonData::TypeGlobalCS::Default()), m_Parts);
					MA_REAL diff = m_pRightToe->GetWCS().Pos().Z - m_pLeftToe->GetWCS().Pos().Z;

					MotionCharacteristics motChars = { itrtMots, m_MotionProcessorSettings.m_MoveBackTime, speed };
					auto& legsSettings = (leftLeg ? speedSettings.m_LeftLegSpeeds : speedSettings.m_RightLegSpeeds);

					auto& angularSpeedSettings = legsSettings.m_AngularSpeeds[angSpeed];
					angularSpeedSettings.m_ID_AngularSpeed = angSpeedID;
					angularSpeedSettings.m_AngularSpeed = angSpeed;

					auto& altitudeDiffChars = angularSpeedSettings.m_AltitudeDiffSettings[altitude];
					altitudeDiffChars.m_Altitude = altitude;
					altitudeDiffChars.m_ID_ClassAltitude = altitude_id;

					altitudeDiffChars.m_MotionsChars[currentStamp].emplace(diff, motChars);
				}
			}

			// motion turn
			TiXmlNode* pMotionsTurn = pTiRoot->FirstChild("motion_turn");
			if (!pMotionsTurn)
				throw std::runtime_error("settings_XML_frmt fail !!!");

			pTiElement = NULL;
			pTiNode = NULL;

			while ((pTiNode = pMotionsTurn->IterateChildren("motion", pTiNode)) && (pTiElement = pTiNode->ToElement()))
			{
				if (pTiElement->QueryUnsignedAttribute("id", &id) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				motionTimeRelationCoeffs.emplace(id, m_MotionProcessorSettings.m_MoveTurnTime);

				UINT tacticID;
				if (pTiElement->QueryUnsignedAttribute("tactic_id", &tacticID) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				bool leftLeg;
				if (pTiElement->QueryBoolAttribute("leftLeg", &leftLeg) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				UINT angSpeedID;
				if (pTiElement->QueryUnsignedAttribute("ang_speed_id", &angSpeedID) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				UINT altitude_id;
				if (pTiElement->QueryUnsignedAttribute("altitude_id", &altitude_id) != TIXML_SUCCESS)
					throw std::runtime_error("settings_XML_frmt fail !!!");

				auto itrtMots = m_SceletonData.m_Motions.find(id);
				if (itrtMots == m_SceletonData.m_Motions.end())
					throw std::runtime_error("settings_XML_frmt fail !!!");

				MA_REAL angSpeed = ang_speeds.at(angSpeedID);
				MA_REAL altitude = altitudes.at(altitude_id);

				auto& tacticSettings = m_MotionProcessorSettings.m_SettingsByTacticCharacter[(TacticCharacter)tacticID];
				tacticSettings.m_TacticCharacter = (TacticCharacter)tacticID;

				if (angSpeedID != 1)
				{
					MA_REAL fabs_angSpeed = fabs(angSpeed);
					if (fabs_angSpeed > tacticSettings.m_MaxTurnSpeedAbs)
						tacticSettings.m_MaxTurnSpeedAbs = fabs_angSpeed;

					if (fabs_angSpeed < tacticSettings.m_MinTurnSpeedAbs)
						tacticSettings.m_MinTurnSpeedAbs = fabs_angSpeed;
				}

				LegsSettings* pLegSettings = NULL;
				if (leftLeg)
				{
					if (angSpeed > 0)
						pLegSettings = &tacticSettings.m_TurningFromLeftLegSettingsRight;
					else
						pLegSettings = &tacticSettings.m_TurningFromLeftLegSettingsLeft;
				}
				else
				{
					if (angSpeed > 0)
						pLegSettings = &tacticSettings.m_TurningFromRightLegSettingsRight;
					else
						pLegSettings = &tacticSettings.m_TurningFromRightLegSettingsLeft;
				}

				auto& angSpeedSettings = pLegSettings->m_AngularSpeeds[angSpeed];
				angSpeedSettings.m_AngularSpeed = angSpeed;
				angSpeedSettings.m_ID_AngularSpeed = angSpeedID;

				for (MA_REAL currentStamp = 0; currentStamp <= 1; currentStamp += stepStamp)
				{
					LocalPositions locals;
					InterpolationState(itrtMots->second, currentStamp, m_SceletonData.m_Connections, locals);

					UpdateLocalPositions(locals, std::pair<ULONG, SceletonData::TypeGlobalCS>(m_pTorso->ID(), SceletonData::TypeGlobalCS::Default()), m_Parts);
					MA_REAL diff = m_pRightToe->GetWCS().Pos().Z - m_pLeftToe->GetWCS().Pos().Z;

					MotionCharacteristics motChars = { itrtMots, m_MotionProcessorSettings.m_MoveTurnTime, 0 };
					
					auto& altitudeDiffChars = angSpeedSettings.m_AltitudeDiffSettings[altitude];
					altitudeDiffChars.m_Altitude = altitude;
					altitudeDiffChars.m_ID_ClassAltitude = altitude_id;

					altitudeDiffChars.m_MotionsChars[currentStamp].emplace(diff, motChars);
				}
			}

			// set begin state
			UpdateLocalPositions(m_SceletonData.m_BeginState.m_LocalPoseElements, std::pair<ULONG, SceletonData::TypeGlobalCS>(m_pTorso->ID()
				, m_SceletonData.m_BeginState.m_GlobalPoseElements[m_pTorso->ID()]), m_Parts
				);

			// calculate kinematic charaters
			m_MotionProcessorSettings.m_LimitKinematicCharaters = CalculateLimitKinematicCharaters(m_SceletonData, motionTimeRelationCoeffs);

			// update basic state position
			selectStatePosesItrt();
		}

	}
}