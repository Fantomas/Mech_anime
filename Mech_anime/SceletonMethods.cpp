#include "stdafx.h"

#include <queue>
#include <algorithm>

#include "SceletonMethods.h"
#include "SceletonSupport.h"


using namespace mech_a;
using namespace mech_a::math;
using namespace mech_a::origin;


namespace mech_a {
	namespace sceleton {


		EulerAnglesCS linearInterpolationEulers(const EulerAnglesCS& posA, const EulerAnglesCS& posB, ExtentsFreedom extentsFreedom, MA_REAL timePos)
		{
			assert(timePos >= 0 && timePos <= 1); // error !!!

			EulerAnglesCS res;

			if (extentsFreedom & ExtentsFreedom::EF_PITCH)
				res.m_Angles.m_pitch = posA.m_Angles.m_pitch + (posB.m_Angles.m_pitch - posA.m_Angles.m_pitch) * timePos;

			if (extentsFreedom & ExtentsFreedom::EF_ROLL)
				res.m_Angles.m_roll = posA.m_Angles.m_roll + (posB.m_Angles.m_roll - posA.m_Angles.m_roll) * timePos;

			if (extentsFreedom & ExtentsFreedom::EF_YAW)
				res.m_Angles.m_yaw = posA.m_Angles.m_yaw + (posB.m_Angles.m_yaw - posA.m_Angles.m_yaw) * timePos;

			res.m_Pos = posA.m_Pos + (posB.m_Pos - posA.m_Pos) * timePos;
			return res;
		}

		EulerAnglesCS cubicalInterpolationEulers(const EulerAnglesCS& posA, const EulerAnglesCS& posB, ExtentsFreedom extentsFreedom, MA_REAL timePos
			, const EulerAnglesSplineInterpolation& splines, MA_REAL timeStamp)
		{
			assert(timePos >= 0 && timePos <= 1);
			assert(timeStamp >= 0);

			EulerAnglesCS res;
			
			if (extentsFreedom & ExtentsFreedom::EF_PITCH)
				res.m_Angles.m_pitch = splines.m_pitchIntrps.Calculate(timeStamp);
			
			if (extentsFreedom & ExtentsFreedom::EF_ROLL)
				res.m_Angles.m_roll = splines.m_rollIntrps.Calculate(timeStamp);
			
			if (extentsFreedom & ExtentsFreedom::EF_YAW)
				res.m_Angles.m_yaw = splines.m_yawIntrps.Calculate(timeStamp);

			res.m_Pos = posA.m_Pos + (posB.m_Pos - posA.m_Pos) * timePos;
			return res;
		}

		QuaternionCS quaternionInterpolation(const QuaternionCS& qA, const QuaternionCS& qB, MA_REAL timePos)
		{
			assert(timePos >= 0 && timePos <= 1);

			QuaternionCS res;
			res.m_Quaternion = QuaternionSlerp(qA.m_Quaternion, qB.m_Quaternion, timePos);
			res.m_Pos = qA.m_Pos + (qB.m_Pos - qA.m_Pos) * timePos;
			return res;
		}

		void interpolationState(const std::unordered_map<ULONG, EulerAnglesCS>& lBound, const std::unordered_map<ULONG, EulerAnglesCS>& rBound
			, std::unordered_map<ULONG, SceletonData::ConnectionData>& connections , MA_REAL timePos, MA_REAL timeStamp
			, const InterpolationData<EulerAnglesCS, InterpolationType::IT_Cubical_Spline_Euler>& interpData, OUT std::unordered_map<ULONG, EulerAnglesCS>& stateRes)
		{
			assert(lBound.size() == rBound.size());

			const MapConnectionCubSplIntrps& splines = interpData.m_Splines;
			assert(splines.size() && splines.begin()->first <= timeStamp);
			
			auto leftTimeItrt = splines.lower_bound(timeStamp);
			if (leftTimeItrt != splines.begin())
				leftTimeItrt--;

			const MapEulersCubSplIntrps& cubSplIntrps = leftTimeItrt->second;
			assert(cubSplIntrps.size() == lBound.size());

			for (const auto& itLB : lBound)
			{
				const auto& itRB = rBound.find(itLB.first);
				assert(itRB != rBound.end());

				const auto& itInterpolation = cubSplIntrps.find(itLB.first);
				assert(itInterpolation != cubSplIntrps.end());

				const auto& itConn = connections.find(itLB.first);
				assert(itConn != connections.end());
				
				ExtentsFreedom extentsFreedom = itConn->second.m_ExtentsFreedom;
				stateRes.emplace(itLB.first, cubicalInterpolationEulers(itLB.second, itRB->second, extentsFreedom, timePos, itInterpolation->second, timeStamp));
			}
		}

		void interpolationState(const std::unordered_map<ULONG, EulerAnglesCS>& lBound, const std::unordered_map<ULONG, EulerAnglesCS>& rBound
			, std::unordered_map<ULONG, SceletonData::ConnectionData>& connections, MA_REAL timePos, MA_REAL timeStamp
			, const InterpolationData<EulerAnglesCS, InterpolationType::IT_Linear_Euler>& interpData, OUT std::unordered_map<ULONG, EulerAnglesCS>& stateRes)
		{
			assert(lBound.size() == rBound.size());

			for (const auto& itLB : lBound)
			{
				const auto& itRB = rBound.find(itLB.first);
				assert(itRB != rBound.end());

				const auto& itConn = connections.find(itLB.first);
				assert(itConn != connections.end());

				ExtentsFreedom extentsFreedom = itConn->second.m_ExtentsFreedom;
				stateRes.emplace(itLB.first, linearInterpolationEulers(itLB.second, itRB->second, extentsFreedom, timePos));
			}
		}

		void interpolationState(const std::unordered_map<ULONG, QuaternionCS>& lBound, const std::unordered_map<ULONG, QuaternionCS>& rBound
			, std::unordered_map<ULONG, SceletonData::ConnectionData>& connections, MA_REAL timePos, MA_REAL timeStamp, const InterpolationData<QuaternionCS, InterpolationType::IT_Linear_Quaternion>& interpData, OUT std::unordered_map<ULONG, QuaternionCS>& stateRes)
		{
			assert(lBound.size() == rBound.size());

			for (const auto& itLB : lBound)
			{
				const auto& itRB = rBound.find(itLB.first);
				assert(itRB != rBound.end());

				stateRes.emplace(itLB.first, quaternionInterpolation(itLB.second, itRB->second, timePos));
			}
		}

		void InterpolationState(const SceletonData::MotionData& motion, MA_REAL timeStamp, std::unordered_map<ULONG, SceletonData::ConnectionData>& connections
			, OUT std::unordered_map<ULONG, SceletonData::TypeLocalCS>& stateRes)
		{
			assert(motion.m_States.size() > 1);
			assert(motion.m_States.begin()->first <= timeStamp && timeStamp <= motion.m_States.rbegin()->first);

			auto rightState = motion.m_States.lower_bound(timeStamp);
			auto leftState = rightState;

			if (rightState == motion.m_States.begin())
				rightState++;
			else
				leftState--;

			MA_REAL timePos = (timeStamp - leftState->first) / (rightState->first - leftState->first);
			assert(timePos >= 0 && timePos <= 1);

			interpolationState(leftState->second.m_LocalPoseElements, rightState->second.m_LocalPoseElements, connections, timePos, timeStamp, motion.m_InterpolationData, stateRes);
		}

		// -----------------------------------------------------------------------------------------------------------------------------------------------------------------

		SC LinearInterpolation(const SC& fromSC, const SC& toSC, MA_REAL pos)
		{
			assert(pos >= 0 && pos <= 1);

			return SC(quaternionInterpolation(fromSC, toSC, pos));
		}

		QuaternionCS LinearInterpolation(const QuaternionCS& fromSC, const QuaternionCS& toSC, MA_REAL pos)
		{
			assert(pos >= 0 && pos <= 1);
			return quaternionInterpolation(fromSC, toSC, pos);
		}

		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------

		void GetLocalTransform(const SC& fromSC, const SC& toSC, OUT SC& localSC)
		{
			localSC.SetSC(MtrA_In_MtrB(toSC.Tr_Matrix(), fromSC.Tr_Matrix()));
		}

		void GetLocalTransform(const SC& fromSC, const SC& toSC, OUT EulerAnglesCS& localSC)
		{
			const SC& localMatr = MtrA_In_MtrB(toSC.Tr_Matrix(), fromSC.Tr_Matrix());
			YawPitchRollFromMatrix(localMatr.Tr_Matrix(), localSC.m_Angles);
			localSC.m_Pos = localMatr.Pos();
		}

		void GetLocalTransform(const SC& fromSC, const SC& toSC, OUT QuaternionCS& localSC)
		{
			const SC& localMatr = MtrA_In_MtrB(toSC.Tr_Matrix(), fromSC.Tr_Matrix());
			MatrixToQuaternion(localMatr.Tr_Matrix(), localSC.m_Quaternion);
			localSC.m_Pos = localMatr.Pos();
		}

		void GetLocalTransform(const QuaternionCS& fromSC, const QuaternionCS& toSC, OUT QuaternionCS& localSC)
		{
// 			localSC.m_Pos = toSC.m_Pos - fromSC.m_Pos;
// 			localSC.m_Quaternion = fromSC.m_Quaternion.GetConjugate() * toSC.m_Quaternion;
// 			localSC.m_Quaternion.Normalize();

			localSC = fromSC.Inverse() * toSC;
		}

		// ------------------------------------------------------------------------------------------------------------------------------------------------------

		void ReTransform(const SC& wcs, const SC& lcs, OUT SC& resWcs)
		{
			resWcs = lcs.Tr_Matrix() * wcs.Tr_Matrix();
		}

		void ReTransform(const SC& wcs, const EulerAnglesCS& lcs, OUT SC& resWcs)
		{
			const SC lcsMtr = (SC)lcs;
			resWcs = lcsMtr.Tr_Matrix() * wcs.Tr_Matrix();
		}

		void ReTransform(const SC& wcs, const QuaternionCS& lcs, OUT SC& resWcs)
		{
			const SC lcsMtr = (SC)lcs;
			resWcs = lcsMtr.Tr_Matrix() * wcs.Tr_Matrix();
		}

		void ReTransform(const QuaternionCS& wcs, const QuaternionCS& lcs, OUT QuaternionCS& resWcs)
		{
			resWcs.m_Quaternion = wcs.m_Quaternion * lcs.m_Quaternion;
			resWcs.m_Quaternion.Normalize();

			Vector newPos = lcs.m_Pos;
			RotateByQtNorm(wcs.m_Quaternion, newPos);
			resWcs.m_Pos = wcs.m_Pos + newPos;
		}


		void ReTransformRevers(const SC& wcs, const EulerAnglesCS& lcs, OUT SC& resWcs)
		{
			const SC lcsMtr = (SC)lcs;
			const SC& inverseLCS = InverseDirectionCosMatrix(lcsMtr.Tr_Matrix());

			resWcs = inverseLCS.Tr_Matrix() * wcs.Tr_Matrix();
		}

		void ReTransformRevers(const SC& wcs, const QuaternionCS& lcs, OUT SC& resWcs)
		{
			const SC lcsMtr = (SC)lcs;
			const SC& inverseLCS = InverseDirectionCosMatrix(lcsMtr.Tr_Matrix());

			resWcs = inverseLCS.Tr_Matrix() * wcs.Tr_Matrix();
		}

		void ReTransformRevers(const QuaternionCS& wcs, const QuaternionCS& lcs, OUT QuaternionCS& resWcs)
		{
			resWcs.m_Quaternion = wcs.m_Quaternion * lcs.m_Quaternion.GetConjugate();
			resWcs.m_Quaternion.Normalize();

			Vector newLocalPos;
			RotateByQtNorm(resWcs.m_Quaternion, newLocalPos);

			resWcs.m_Pos = wcs.m_Pos - newLocalPos;
		}

		// -----------------------------------------------------------------------------------------------------------------------------------

		SC calculateNewBasicLCS(const SC& wcs, const SC& parentWcs, const SC& lcs)
		{
			return InverseDirectionCosMatrix(lcs.Tr_Matrix()) * wcs.Tr_Matrix() * InverseDirectionCosMatrix(parentWcs.Tr_Matrix());
		}

		QuaternionCS calculateNewBasicLCS(const QuaternionCS& wcs, const QuaternionCS& parentWcs, const QuaternionCS& lcs)
		{
			return parentWcs.Inverse() * wcs * lcs.Inverse();
		}

		// -----------------------------------------------------------------------------------------------------------------------------------

		void updateSceletonDownFromConnection(Connection* pUpperConnect)
		{
			if (!pUpperConnect)
				return;

			// recalc all new wcs
			std::queue<Element*> parent_Childs;
			parent_Childs.push(pUpperConnect->GetElementB());

			while (!parent_Childs.empty())
			{
				Element* pElement = parent_Childs.front();
				parent_Childs.pop();

				if (!pElement)
					continue;

				const auto& childConnects = pElement->GetChildConnects();

				for (Connection* pChConn : childConnects)
				{
					assert(pChConn);

					pChConn->UpdateWCSFromElementA();
					parent_Childs.push(pChConn->GetElementB());
				}
			}
		}


		void UpdateSceletonDownWCS(Connection* pUpperConnect, const SceletonData::TypeGlobalCS& updateWCS)
		{
			if (!pUpperConnect)
				return;

			// set new wcs
			pUpperConnect->setWCS(updateWCS);

			// recalc basic and local transform	
			if (Element* pUppEl = pUpperConnect->GetElementA())
			{
				SceletonData::TypeGlobalCS lcsWorldCoord;
				FromCS_ToCS(pUpperConnect->GetLCS(), lcsWorldCoord);

				pUpperConnect->setBasicLCS(calculateNewBasicLCS(updateWCS, pUppEl->GetWCS(), lcsWorldCoord));
			}
			else
			{
				pUpperConnect->setBasicLCS(SceletonData::TypeGlobalCS::Default());
				pUpperConnect->setLCS_WCS(updateWCS);
			}

			// update sceleton down
			updateSceletonDownFromConnection(pUpperConnect);
		}


		void UpdateSceletonDownLCS(Connection* pUpperConnect, const SceletonData::TypeLocalCS& updateLCS)
		{
			if (!pUpperConnect)
				return;

			// set new lcs
			pUpperConnect->setLCS(updateLCS);
			
			// set new wcs
			pUpperConnect->UpdateWCSFromElementA();

			// update sceleton down
			updateSceletonDownFromConnection(pUpperConnect);
		}


		void UpdateSceletonFromConnection(Connection* pConnect, bool onlyUpDir)
		{
			// load parent - childs
			struct Parent_Child
			{
				bool                        m_DownDirection;
				Element*                    m_pChild_Element;
				Connection*                 m_pFrom_Connect;
			};

			std::queue<Parent_Child> parent_childs;
			parent_childs.push({ false, pConnect->GetElementA(), pConnect });
			
			if (!onlyUpDir)
				parent_childs.push({ true, pConnect->GetElementB(), pConnect });

			// recalc new wcs
			while (!parent_childs.empty())
			{
				Parent_Child parCh = parent_childs.front();
				parent_childs.pop();

				Element* pElement = parCh.m_pChild_Element;
				if (!pElement)
					continue;

				if (parCh.m_DownDirection)
				{
					const auto& childConnects = pElement->GetChildConnects();

					for (Connection* pChConn : childConnects)
					{
						assert(pChConn);

						pChConn->UpdateWCSFromElementA();
						parent_childs.push({ true, pChConn->GetElementB(), pChConn });
					}
				}
				else
				{
					// up 
					Connection* pUpperConnect = pElement->GetParentConnect();
					assert(pUpperConnect);
					assert(parCh.m_pFrom_Connect);

					const SceletonData::TypeGlobalCS& newUpperConnWCS = parCh.m_pFrom_Connect->CalculateParentWCS();
					pUpperConnect->setWCS(newUpperConnWCS);

					if (!pUpperConnect->GetElementA())
					{
						pUpperConnect->setLCS_WCS(newUpperConnWCS);
						pUpperConnect->setBasicLCS(SceletonData::TypeGlobalCS::Default());
					}
					else
						parent_childs.push({ false, pUpperConnect->GetElementA(), pUpperConnect });

					// down
					const auto& childConnects = pElement->GetChildConnects();

					for (Connection* pChConn : childConnects)
					{
						assert(pChConn);

						if (pChConn == parCh.m_pFrom_Connect)
							continue;

						pChConn->UpdateWCSFromElementA();
						parent_childs.push({ true, pChConn->GetElementB(), pChConn });
					}
				}
			}

		}


		void UpdateSceletonUpLCS(Connection* pUpperConnect, const SceletonData::TypeLocalCS& updateLCS)
		{
			if (!pUpperConnect)
				return;

			// set new lcs
			pUpperConnect->setLCS(updateLCS);
			
			if (!pUpperConnect->GetElementA())
			{
				pUpperConnect->setBasicLCS(SceletonData::TypeGlobalCS::Default());
				
				SceletonData::TypeGlobalCS wcs;
				FromCS_ToCS(updateLCS, wcs);
				pUpperConnect->setWCS(wcs);

				// update sceleton tree
				UpdateSceletonFromConnection(pUpperConnect);
				return;
			}

			// update sceleton tree
			UpdateSceletonFromConnection(pUpperConnect, true);
		}


		void UpdateSceleton(Connection* pBeginConnect, const SceletonData::TypeGlobalCS& updateWCS)
		{
			if (!pBeginConnect)
				return;

			// set new wcs
			pBeginConnect->setWCS(updateWCS);

			if (!pBeginConnect->GetElementA())
			{
				pBeginConnect->setLCS_WCS(updateWCS);
				pBeginConnect->setBasicLCS(SceletonData::TypeGlobalCS::Default());
			}

			// update sceleton tree
			UpdateSceletonFromConnection(pBeginConnect);
		}


		void UpdateSceletonDownBasicLCS(Connection* pUpperConnect, const SceletonData::TypeGlobalCS& updateBasicLCS)
		{
			if (!pUpperConnect || !pUpperConnect->GetElementA())
				return;

			// set basic LCS
			pUpperConnect->setBasicLCS(updateBasicLCS);

			// set new wcs
			pUpperConnect->UpdateWCSFromElementA();

			// update sceleton down
			updateSceletonDownFromConnection(pUpperConnect);
		}

		// ------------------------------------------------------------------------------------------------------------------------------

		void LoadParts(IN std::unordered_map<ULONG, ElementData>& elements, IN std::unordered_map<ULONG, SceletonData::ConnectionData>& connections,
			IN StateData& state, OUT std::unordered_map<ULONG, std::unique_ptr<IPartSceleton>>& parts)
		{
			for (auto itCon = connections.begin(); itCon != connections.end(); itCon++)
			{
				auto itWCS = state.m_GlobalPoseElements.find(itCon->first);
				if (itWCS == state.m_GlobalPoseElements.end())
					itWCS = state.m_GlobalPoseElements.emplace(itCon->first, SceletonData::TypeGlobalCS::Default()).first;

				auto itLCS = state.m_LocalPoseElements.find(itCon->first);
				if (itLCS == state.m_LocalPoseElements.end())
					itLCS = state.m_LocalPoseElements.emplace(itCon->first, SceletonData::TypeLocalCS::Default()).first;

				parts.emplace(itCon->first, std::unique_ptr<IPartSceleton>(new Connection(itCon, NULL, NULL, itWCS, itLCS)));
			}

			for (auto itElem = elements.begin(); itElem != elements.end(); itElem++)
			{
				auto itPart = parts.find(itElem->second.m_ParentConnectId);
				assert(itPart != parts.end());

				Connection* pParentConnect = dynamic_cast<Connection*>(itPart->second.get());
				assert(pParentConnect);

				Element* pElement = new Element(itElem, pParentConnect);
				parts.emplace(itElem->first, std::unique_ptr<IPartSceleton>(pElement));

				for (ULONG idConnect : itElem->second.m_ConnectIDs)
				{
					itPart = parts.find(idConnect);
					assert(itPart != parts.end());

					Connection* pConnect = dynamic_cast<Connection*>(itPart->second.get());
					assert(pConnect);

					pElement->m_psConnects.insert(pConnect);
				}
			}

			for (const auto& itConn : connections)
			{
				auto itPart = parts.find(itConn.first);
				assert(itPart != parts.end());

				Connection* pConnect = dynamic_cast<Connection*>(itPart->second.get());
				assert(pConnect);

				if (itConn.second.m_ElementA)
				{
					itPart = parts.find(itConn.second.m_ElementA);
					assert(itPart != parts.end());

					Element* pElement = dynamic_cast<Element*>(itPart->second.get());
					assert(pElement);

					pConnect->m_pElementA = pElement;
				}

				if (itConn.second.m_ElementB)
				{
					itPart = parts.find(itConn.second.m_ElementB);
					assert(itPart != parts.end());

					Element* pElement = dynamic_cast<Element*>(itPart->second.get());
					assert(pElement);

					pConnect->m_pElementB = pElement;
				}
			}
		}

		// -----------------------------------------------------------------------------------------------------------------------------------

		template<class T_LCS, InterpolationType IT>
		void removeInterpolationData(ULONG idConn, InterpolationData<T_LCS, IT>& interpolationData)
		{ }

		template<>
		void removeInterpolationData<EulerAnglesCS, InterpolationType::IT_Cubical_Spline_Euler>(ULONG idConn
			, InterpolationData<EulerAnglesCS, InterpolationType::IT_Cubical_Spline_Euler>& interpolationData)
		{
			MapConnectionCubSplIntrps& splines = interpolationData.m_Splines;
			
			for (auto& itSpl : splines)
			{
				MapEulersCubSplIntrps& eulInterpols = itSpl.second;
				eulInterpols.erase(idConn);
			}
		}

		void RemoveInterpolationData(ULONG idConn, InterpolationData<SceletonData::TypeLocalCS, SceletonData::sInterpolationType>& interpolationData)
		{
			removeInterpolationData(idConn, interpolationData);
		}


		// ----------------------------------------------------------------------------------------------------------------------------------------


		void UpdateLocalPositions(const LocalPositions& newPositions, const std::pair<ULONG, SceletonData::TypeGlobalCS>& wcsBaseConnection, IN std::unordered_map<ULONG, std::unique_ptr<IPartSceleton>>& connections)
		{
			for (const auto& localPosePair : newPositions)
			{
				auto itrtConnection = connections.find(localPosePair.first);
				assert(itrtConnection != connections.end());

				Connection* pConnection = dynamic_cast<Connection*>(itrtConnection->second.get());
				assert(pConnection);

				pConnection->setLCS(localPosePair.second);
			}

			auto itrtConnection = connections.find(wcsBaseConnection.first);
			assert(itrtConnection != connections.end());

			Connection* pConnection = dynamic_cast<Connection*>(itrtConnection->second.get());
			assert(pConnection);

			pConnection->Retransform(wcsBaseConnection.second);
		}


		// ----------------------------------------------------------------------------------------------------------------------------------------

		EulerAngles maxAbs(const EulerAngles& eulA, const EulerAngles& eulB)
		{
			EulerAngles resEulAngs;
			resEulAngs.m_pitch = max(fabs(eulA.m_pitch), fabs(eulB.m_pitch));
			resEulAngs.m_roll = max(fabs(eulA.m_roll), fabs(eulB.m_roll));
			resEulAngs.m_yaw = max(fabs(eulA.m_yaw), fabs(eulB.m_yaw));

			return resEulAngs;
		}

		Quaternion maxAbs(const Quaternion& qA, const Quaternion& qB)
		{
			Quaternion resQ;
			resQ.m_W = 0;

			resQ.m_X = max(fabs(qA.m_X), fabs(qB.m_X));
			resQ.m_Y = max(fabs(qA.m_Y), fabs(qB.m_Y));
			resQ.m_Z = max(fabs(qA.m_Z), fabs(qB.m_Z));

			return resQ;
		}


		std::unordered_map<ULONG, EulerAngles> calculateLimitKinematicCharaters(const _MotionData<EulerAnglesCS, SC, IT_Linear_Euler>& motionData, MA_REAL timeRelation)
		{
			std::unordered_map<ULONG, EulerAngles> resKinematicChars;

			auto itrtStates = motionData.m_States.begin();
			if (itrtStates == motionData.m_States.end())
				return resKinematicChars;

			auto itrtStatesNext = itrtStates;

			while (++itrtStatesNext != motionData.m_States.end())
			{
				MA_REAL dT = (itrtStatesNext->first - itrtStates->first) * timeRelation;

				const _StateData<EulerAnglesCS, SC>& preState = itrtStates->second;
				const _StateData<EulerAnglesCS, SC>& nextState = itrtStatesNext->second;

				const std::unordered_map<ULONG, EulerAnglesCS>& preLocalStates = preState.m_LocalPoseElements;
				const std::unordered_map<ULONG, EulerAnglesCS>& nextLocalStates = nextState.m_LocalPoseElements;

				for (const std::pair<ULONG, EulerAnglesCS>& preLocalSC : preLocalStates)
				{
					auto itrtNextLocalSC = nextLocalStates.find(preLocalSC.first);
					if (itrtNextLocalSC == nextLocalStates.end())
						throw std::runtime_error("Invalid SceletonData !!!");

					EulerAngles angSpeeds;
					angSpeeds.m_pitch = (itrtNextLocalSC->second.m_Angles.m_pitch - preLocalSC.second.m_Angles.m_pitch) / dT; // error !!!
					angSpeeds.m_roll = (itrtNextLocalSC->second.m_Angles.m_roll - preLocalSC.second.m_Angles.m_roll) / dT;
					angSpeeds.m_yaw = (itrtNextLocalSC->second.m_Angles.m_yaw - preLocalSC.second.m_Angles.m_yaw) / dT;

					EulerAngles olderAngSpeed = resKinematicChars[preLocalSC.first];
					resKinematicChars[preLocalSC.first] = maxAbs(olderAngSpeed, angSpeeds);
				}

				itrtStates = itrtStatesNext;
			}

			return resKinematicChars;
		}

		std::unordered_map<ULONG, EulerAngles> calculateLimitKinematicCharaters(const _MotionData<EulerAnglesCS, SC, IT_Cubical_Spline_Euler>& motionData, MA_REAL timeRelation)
		{
			std::unordered_map<ULONG, EulerAngles> resKinematicChars;
			const MapConnectionCubSplIntrps& splines = motionData.m_InterpolationData.m_Splines;

			for (const std::pair<MA_REAL, MapEulersCubSplIntrps>& itrtInterpMap : splines)
			{
				const MA_REAL x1 = itrtInterpMap.first;

				for (const std::pair<ULONG, EulerAnglesSplineInterpolation>& splinesPair : itrtInterpMap.second)
				{
					const EulerAngles oldCharacters = resKinematicChars[splinesPair.first];
					const MA_REAL x2 = splinesPair.second.m_pitchIntrps.m_Xk;

					MA_REAL extremeAngSpeed = splinesPair.second.m_pitchIntrps.MaxDerivative(x1, x2).second / timeRelation ;
					resKinematicChars[splinesPair.first].m_pitch = max(fabs(extremeAngSpeed), oldCharacters.m_pitch);

					extremeAngSpeed = splinesPair.second.m_rollIntrps.MaxDerivative(x1, x2).second / timeRelation;
					resKinematicChars[splinesPair.first].m_roll = max(fabs(extremeAngSpeed), oldCharacters.m_roll);

					extremeAngSpeed = splinesPair.second.m_yawIntrps.MaxDerivative(x1, x2).second / timeRelation;
					resKinematicChars[splinesPair.first].m_yaw = max(fabs(extremeAngSpeed), oldCharacters.m_yaw);
				}
			}

			return resKinematicChars;
		}

		Quaternion calculAngSpeed(const Quaternion& posA, const Quaternion& posB, MA_REAL dT)
		{
			Quaternion angSpeed;

			Quaternion localRotate = posB * posA.GetConjugate();
			localRotate.Normalize();

			MA_REAL angle; Vector axisRotate;
			localRotate.GetAxisAngl(angle, axisRotate);

			const MA_REAL angSpeedScolar = angle / dT;

			angSpeed.m_X = axisRotate.X * angSpeedScolar;
			angSpeed.m_Y = axisRotate.Y * angSpeedScolar;
			angSpeed.m_Z = axisRotate.Z * angSpeedScolar;
			return angSpeed;
		}

		std::unordered_map<ULONG, Quaternion> calculateLimitKinematicCharaters(const _MotionData<QuaternionCS, QuaternionCS, IT_Linear_Quaternion>& motionData, MA_REAL timeRelation)
		{
			std::unordered_map<ULONG, Quaternion> resKinematicChars;

			auto itrtStates = motionData.m_States.begin();
			if (itrtStates == motionData.m_States.end())
				return resKinematicChars;

			auto itrtStatesNext = itrtStates;

			while (++itrtStatesNext != motionData.m_States.end())
			{
				MA_REAL dT = (itrtStatesNext->first - itrtStates->first) * timeRelation;

				const _StateData<QuaternionCS, QuaternionCS>& preState = itrtStates->second;
				const _StateData<QuaternionCS, QuaternionCS>& nextState = itrtStatesNext->second;

				const std::unordered_map<ULONG, QuaternionCS>& preLocalStates = preState.m_LocalPoseElements;
				const std::unordered_map<ULONG, QuaternionCS>& nextLocalStates = nextState.m_LocalPoseElements;

				for (const std::pair<ULONG, QuaternionCS>& preLocalSC : preLocalStates)
				{
					auto itrtNextLocalSC = nextLocalStates.find(preLocalSC.first);
					if (itrtNextLocalSC == nextLocalStates.end())
						throw std::runtime_error("Invalid SceletonData !!!");

					Quaternion olderAngSpeed = resKinematicChars[preLocalSC.first];
					resKinematicChars[preLocalSC.first] = maxAbs(olderAngSpeed, calculAngSpeed(preLocalSC.second.m_Quaternion, itrtNextLocalSC->second.m_Quaternion, dT));
				}

				itrtStates = itrtStatesNext;
			}


			return resKinematicChars;
		}

		std::unordered_map<ULONG, SceletonData::TypeLocalCS::RotatorType> CalculateLimitKinematicCharaters(const SceletonData& sceletonData, const std::unordered_map<ULONG, MA_REAL>& motionTimeRelationCoeffs)
		{
			std::unordered_map<ULONG, SceletonData::TypeLocalCS::RotatorType> resCharacters;

			for (const std::pair<ULONG, SceletonData::MotionData>& motData : sceletonData.m_Motions)
			{
				auto itrtMotRelCoeff = motionTimeRelationCoeffs.find(motData.first);
				if (itrtMotRelCoeff == motionTimeRelationCoeffs.end())
					continue;

				const std::unordered_map<ULONG, SceletonData::TypeLocalCS::RotatorType>& resKinChars = calculateLimitKinematicCharaters(motData.second, itrtMotRelCoeff->second);

				for (const std::pair<ULONG, SceletonData::TypeLocalCS::RotatorType>& rotaryChars : resKinChars)
				{
					const ULONG idConnection = rotaryChars.first;
					resCharacters[idConnection] = maxAbs(resCharacters[idConnection], rotaryChars.second);
				}
			}

			return resCharacters;
		}


		// --------------------------------------------------------------------------------------------------------------------------------------------------------

		CubicSplineFunction DifferenceInHeightOfConnections(const SceletonData::MotionData& motionData, ULONG idConnA, ULONG idConnB)
		{
			std::map<MA_REAL, MA_REAL> diffs;

			for (const auto& statePair : motionData.m_States)
			{
				const auto& localPoses = statePair.second.m_LocalPoseElements;
				
				auto itrtConnA = localPoses.find(idConnA);
				if (itrtConnA == localPoses.end())
					throw std::runtime_error("Invalid motionData");

				auto itrtConnB = localPoses.find(idConnB);
				if (itrtConnB == localPoses.end())
					throw std::runtime_error("Invalid motionData");

				diffs.emplace(statePair.first, itrtConnA->second.m_Pos.Z - itrtConnB->second.m_Pos.Z);
			}
			
			return CubicSplineFunction(diffs);
		}

		// ------------------------------------------------------------------------------------------------------------------------------------------------

		QuaternionCS intermediateCS(const QuaternionCS& csA, const QuaternionCS& csB, MA_REAL relationAtoB)
		{
			return LinearInterpolation(csA, csB, relationAtoB);
		}

		EulerAnglesCS intermediateCS(const EulerAnglesCS& csA, const EulerAnglesCS& csB, MA_REAL relationAtoB)
		{
			// 			const EulerAngles& eulA = csA.m_Angles.Normal();
			// 			const EulerAngles& eulB = csB.m_Angles.Normal();
			// 
			// 			const EulerAngles& resEulers = eulA + (eulB - eulA).Normal() * relationAtoB;
			// 			const Vector& resPos = csA.m_Pos + (csB.m_Pos - csA.m_Pos) * relationAtoB;
			// 
			// 			return EulerAnglesCS(resEulers, resPos);

			QuaternionCS qtCsA;
			QuaternionCS qtCsB;

			FromCS_ToCS(csA, qtCsA);
			FromCS_ToCS(csB, qtCsB);

			QuaternionCS res = intermediateCS(qtCsA, qtCsB, relationAtoB);

			EulerAnglesCS eRes;
			FromCS_ToCS(res, eRes);

			return eRes;
		}


		LocalPositions IntermediateState(const LocalPositions& stateA, const LocalPositions& stateB, MA_REAL relationAtoB)
		{
			if (relationAtoB < 0 || relationAtoB > 1)
				throw std::runtime_error("Invalid relation coefficient !!!");

			LocalPositions res;

			for (const auto& pairLocal : stateA)
			{
				auto itrtStateB = stateB.find(pairLocal.first);
				if (itrtStateB == stateB.end())
					throw std::runtime_error("Invalid state B !!!");

				res.emplace(pairLocal.first, intermediateCS(pairLocal.second, itrtStateB->second, relationAtoB));
			}

			return res;
		}


		// ----------------------------------------------------------------------------------------------------------------------------------------------

		EulerAngles diffRotatorStates(const EulerAngles& fromPos, const EulerAngles& toPos)
		{
			const EulerAngles& fromNorm = fromPos.Normal();
			const EulerAngles& toNorm = toPos.Normal();

			return (toNorm - fromNorm).Normal();
		}

		Quaternion diffRotatorStates(const Quaternion& fromPos, const Quaternion& toPos)
		{
			Quaternion localRotate = toPos * fromPos.GetConjugate();
			localRotate.Normalize();

			return localRotate;
		}


		SceletonData::TypeLocalCS::RotatorType DiffRotatorStates(const SceletonData::TypeLocalCS::RotatorType& fromState, const SceletonData::TypeLocalCS::RotatorType& toState)
		{
			return diffRotatorStates(fromState, toState);
		}


		QuaternionCS diffStates(const QuaternionCS& fromPos, const QuaternionCS& toPos)
		{
			Quaternion localRotate = toPos.m_Quaternion * fromPos.m_Quaternion.GetConjugate();
			localRotate.Normalize();

			const Vector& resPos = toPos.m_Pos - fromPos.m_Pos;
			return QuaternionCS(localRotate, resPos);
		}

		EulerAnglesCS diffStates(const EulerAnglesCS& fromPos, const EulerAnglesCS& toPos)
		{
			// 			const EulerAngles& fromNorm = fromPos.m_Angles.Normal();
			// 			const EulerAngles& toNorm = toPos.m_Angles.Normal();
			// 
			// 			const EulerAngles& resEulers = (toNorm - fromNorm).Normal();
			// 			const Vector& resPos = fromPos.m_Pos - toPos.m_Pos;
			// 
			// 			return EulerAnglesCS(resEulers, resPos);

			QuaternionCS frQt;
			FromCS_ToCS(fromPos, frQt);

			QuaternionCS toQt;
			FromCS_ToCS(toPos, toQt);

			QuaternionCS res = diffStates(frQt, toQt);

			EulerAnglesCS resEul;
			FromCS_ToCS(res, resEul);

			return resEul;
		}


		LocalPositions DiffStates(const LocalPositions& fromState, const LocalPositions& toState)
		{
			LocalPositions resPoses;

			for (const auto& pairFromPos : fromState)
			{
				const ULONG indexPos = pairFromPos.first;

				LocalPositions::const_iterator itrtToState = toState.find(indexPos);
				if (itrtToState == toState.end())
					throw std::runtime_error("Poses container fail");

				resPoses.emplace(indexPos, diffStates(pairFromPos.second, itrtToState->second));
			}

			return resPoses;
		}

	}
}